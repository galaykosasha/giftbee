jQuery(document).ready(function() {
// SHOW HIDE SIDEBAR
$('.gb-hum-cont label').on('click', function() {
	$('.sidebar').toggleClass('gb-hide');
	$('.sidebar_switch').toggleClass('gb-sidebar-switch');
	$('.menu-open-hide').toggleClass('menu-open-show');
});

// MASK
$(".phone-field").mask("+38(099) 999-99-99");
$(".date-field").mask("99.99.9999");

// MODAL CALL BACK
$('.modal-1').on('hidden.bs.modal', function (e) {
	console.log('Хуй там плавав');
});

// CHECKBOX VALIDATION
$('#user-form').change(function() {
	setTimeout(function() {
		var hasFormError = $('.input-group, .for-checkbox-validate').hasClass('error');
		if(hasFormError != false) {
			$('#Users_active').prop( "checked", false );
		}
	}, 300);
});

// HIDE MENU SWITCH
$(window).scroll(function() {
	var animOutEffect = 'zoomOutLeft';
	var animInEffect = 'zoomIn';
	var scrollValue = $(window).scrollTop();
	if(scrollValue > $(window).height()) {
		$('.sidebar_switch .gb-hum-cont').addClass(animOutEffect);
		$('.footer .go-top').css("display","block").addClass(animInEffect).removeClass(animOutEffect);
		$('.sidebar_switch').css('height','0');
	} 

	if (scrollValue < $(window).height() && $('.sidebar_switch .gb-hum-cont').hasClass(animOutEffect) ) {
		$('.sidebar_switch .gb-hum-cont').css('display', 'block').removeClass(animOutEffect).addClass(animInEffect);
		$('.footer .go-top').addClass(animOutEffect);
	}

	if($('.sidebar').hasClass('gb-hide')) {
		$('.sidebar_switch .gb-hum-cont').css('display', 'block').removeClass('animated');
		$('.sidebar_switch').addClass('gb-sidebar-switch');

	} else {

		$('.sidebar_switch .gb-hum-cont').addClass('animated');
	}
})

// ALERT CLOSE ANIMATION
$('.alert .close').on('click', function() {
	$('.notify-message').addClass('bounceOutRight')
})

// SELECT
$(".selectpicker").chosen();

// SLICK SLIDER
  $(".home-gb-grid").slick({
      infinite: true,
      slidesToScroll: 1,
      slidesToShow: 5,
      autoplay: false,
      autoplaySpeed: 3000,
      prevArrow: false,
      nextArrow: false,
      settings: "unslick",
      focusOnSelect: false,
      centerMode: false,
      responsive: [

      {
        breakpoint: 1920,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          centerMode: false
        }
      },

      {
        breakpoint: 1500,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          centerMode: false
        }
      },
          {
            breakpoint: 1280,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: true,
              dots: true,
              centerMode: false
            }
          },
          {
            breakpoint: 980,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              dots: true,
              centerMode: false
            }
          },

          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: true,
              autoplay: false
            }
          }
        ]
  });

// WINNERS BOARD CENTERED
function getCountWinner() {
  var itemElem = $("#comp-winners-tab .comp-user-board-item"),
      elemLength = itemElem.length;
  if (elemLength < 5) {
    itemElem.addClass("elem-centered");
  }
}

getCountWinner();




});










