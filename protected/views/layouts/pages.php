<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<!-- GRID GIFTS -->
<div class="container-fluid pages pages-top-mrg">
<div class="row"> 
<?php if(Yii::app()->controller->action->id != 'faq'): ?>
<!-- COMP LOGO -->
    <div class="col-md-12 top_bar-logo">
      <a href="/" ><img src="<?= Yii::app()->createUrl('/images'); ?>/logo-top_bar.png" alt=""></a>
    </div>
<!-- END COMP LOGO -->
<?php endif; ?>

    <?= $content; ?>
    <!-- END GRID GIFTS -->
</div>
</div>
<?php $this->endContent(); ?>