<?php
/* @var $this Controller */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $this->pageTitle; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="google-site-verification" content="jWD9sq-HiNSbVdbSlgdaxb_YJjY_DKzxpHmFx8YP4N8" />
    <meta name='yandex-verification' content='6ad3b1b4bf8f5532' />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="data:image/x-icon;base64,AAABAAEAEBAAAAEAGABoAwAAFgAAACgAAAAQAAAAIAAAAAEAGAAAAAAAAAMAAAAAAAAAAAAAAAAAAAAAAAACTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTuf9//////8CTucCTucCTucCTucCTucCTucCTuf///8CTucCTucCTucCTucAT+b///8CTuf///////8CTucCTucCTucCTucCTuf///////8CTuf///8CT+UCTuf///8CTucCTucCTuf///////8CTucCTucCTucCTucCTucCTuf///////8CTucCTuf///8CTucCTucCTucCTuf///8CTucCTucCTucCTucCTuf///////8CTuf///////8CTucCTucCTucCTuf///8CTucCTucCTucCTucCTucCTucCTucCTuf///8CTuf///4CTuf///////////8CT+UCTucCTucCTucCTucCTucCTucCTucDTef///7//////////v////8CTucCTucCTucCTucCTucCTucCTucCTucDTef////9//8CTucCTuf///8CTuf///8CTucCTucCTucCTucCTucCTucCTucCTuf///8CTucCTucCTuf//////////////////////v8CTucCTucCTucCTucCTuf///8CTucCTucCTucCTuf///8CTucCTuf///8CTucCTucCTucCTucCTucCTucCTuf///////8CTucCTucCTuf///8CTucCTuf///4CTucCTucCTucCTucCTucCTucCTucCTuf///////8CTuf///8ATucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTuf///////8CTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucCTucAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPCEtLQokKCdib2R5JykuZXEoMCkuY3NzKCd3aWR0aCcpCi0tPgo=" rel="shortcut icon" type="image/x-icon" />
  <?php /*  <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/vendor/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/vendor/slick.css">
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/vendor/slick-theme.css">
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/fonts/fontello.css">
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/fonts/fontello.css">
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/vendor/animate.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.3.0/animate.min.css">
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/vendor/chosen.css">*/ ?>
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/vendor/vendor.min.css">
    <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/style.css">
    <script type="text/javascript" src="http://vk.com/js/api/share.js?91" charset="windows-1251"></script>
    <script>
    function ptn(id, a, b, c)
    {
        var x = i = 0;
        var y = 1000/1000;
        var j = parseInt(b/y);
        y = b/j;
        var k = a/j;

        var int1 = setInterval(function()
        {
            if(i<j+1)
            {
                x =  k * i;
                document.getElementById(id).innerHTML = (x.toFixed(c));
                i++;
            }else
            {
                window.clearInterval(int1);
            }
        },y);
    }
    </script>

<!-- ======= stringConcat ======= -->
<script>
    function stringConcat(elemClass, countSumb) {
        var bcString = document.getElementsByClassName(elemClass)[0];
        var textBcString = bcString.textContent;
        var bcStringLenth = textBcString.length;
        if(bcStringLenth > countSumb) {
            var bcStringConcat = textBcString.slice(0, countSumb) + "...";
            bcString.textContent = bcStringConcat;
        }
    }
</script>
<!-- ======= end_stringConcat ======= -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65408063-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>

<div id="top-here" class="container-wrp h100">
    <!-- SIDEBAR -->
    <?php $this->widget('application.widgets.MenuSidebarWidget'); ?>
    <!-- ENDSIDEBAR -->

    <!-- CONTENT -->
    <main class="content">
        <div class="top_bar navbar-fixed-top">
            <div class="sidebar_switch text-center">
                <div class="gb-hum-cont animated">
                  <input id="click" name="exit" type="checkbox" />
                  <label for="click"><span class="burger"></span></label>       
                </div>  
            </div>
        </div>
        <?= $content; ?>
        <!-- FOOTER -->
        <div class="container-fluid footer clearfix">
          <div class="row">
            <div class="col-md-12">
              <a href="#top-here" class="go-top animated"><i class="fa fa-chevron-up"></i></a>
              <span class="copyright"><b>Copyright</b> © 2015 giftbee.ua <span class="all-right-reserved">Все права защищены</span></span>
              <a href="https://www.facebook.com/giftbeeua" target="_blank"><i class="fa fa-facebook"></i></a>
              <a href="https://vk.com/giftbeeua" target="_blank"><i class="fa fa-vk"></i></a>
              <a href="https://plus.google.com/u/0/101220237034563164885/posts" target="_blank"><i class="fa fa-google-plus"></i></a>
              <a href="https://www.youtube.com/channel/UCbnq9QoccDj_qnBpueXhWdw?ab_channel=GiftBeeuainsta" target="_blank"><i class="fa fa-youtube"></i></a>
              <a href="https://instagram.com/giftbeeua/" target="_blank"><i class="fa fa-instagram"></i></a>
            </div>
          </div>
        </div>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
        <!-- END FOOTER -->
        <div class="clearfix"></div>
    </main>
    <!-- ENDCONTENT -->
</div>

<?php if(Yii::app()->user->isGuest): ?>
    <?php $this->widget('application.widgets.AuthModalWidget'); ?>
<?php endif; ?>

    <!-- JAVASCRIPT -->
    <?php $cs = Yii::app()->clientScript
        ->registerCoreScript('jquery')
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/masonry.pkgd.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/jquery.imageloader.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/jquery.backgroundSize.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request.'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.1.8/imagesloaded.pkgd.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/jquery-imagefill.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/slick.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/share.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/jquery.simple.timer.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/bootstrap.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/jquery.maskedinput.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/chosen.jquery.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        // ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/jquery.animateNumber.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)

        ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/vendor.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/vendor/salvattore.min.js', false, -1, YII_DEBUG), CClientScript::POS_END)
        ->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->request->baseUrl.'js/main.js', false, -1, YII_DEBUG), CClientScript::POS_END);
    ?>

<!-- NOTIFY MESSAGE -->
    <?php if(Yii::app()->user->hasFlash('error')): ?>

    <div class="notify-message animated bounceInRight">
        <div class="alert alert-error alert-dismissable flex clearfix">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="col-md-2 text-center p0">
                <i class="fa fa-exclamation"></i>
            </div>
            <div class="col-md-10 p-r">
                <?= Yii::app()->user->getFlash('error'); ?>
            </div>
        </div>
    </div>
    
    <?php elseif (Yii::app()->user->hasFlash('success')): ?>

    <div class="notify-message animated bounceInRight">
        <div class="alert alert-success alert-dismissable flex clearfix">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="col-md-2 text-center p0">
                <i class="icon fa fa-check"></i>
            </div>
            <div class="col-md-10 p-r">
                <?= Yii::app()->user->getFlash('success'); ?>
            </div>
        </div>
    </div>

    <?php elseif (Yii::app()->user->hasFlash('info')): ?>

    <div class="notify-message animated bounceInRight">  
        <div class="alert alert-info alert-dismissable flex clearfix">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="col-md-2 text-center p0">
                <img class="alert-user-photo" src="<?= Users::model()->findByPk(Yii::app()->user->id)->image; ?>" alt=""/>
            </div>
            <div class="col-md-10 p-r">
                <?= Yii::app()->user->getFlash('info'); ?>
            </div>
        </div>
    </div>


    <?php elseif (Yii::app()->user->hasFlash('facebook')): ?>
        
    <div class="notify-message animated bounceInRight">  
        <div class="alert alert-facebook alert-dismissable flex clearfix">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
           
            <div class="col-md-2 text-center p0">
               <i class="icon fa fa-facebook"></i>
            </div>    
            <div class="col-md-10 p-r">
                <?= Yii::app()->user->getFlash('facebook'); ?>
            </div>
        </div>
    </div>

    <?php elseif (Yii::app()->user->hasFlash('vkontakte')): ?>
        
    <div class="notify-message animated bounceInRight">  
        <div class="alert alert-vkontakte alert-dismissable flex clearfix">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="col-md-2 text-center p0">
                <i class="icon fa fa-vk"></i>
            </div>
            <div class="col-md-10 p-r">
                <?= Yii::app()->user->getFlash('vkontakte'); ?>
            </div>
        </div>
    </div>

        <?php elseif (Yii::app()->user->hasFlash('congratulations')): ?>

    <div class="notify-message animated bounceInRight">  
        <div class="alert alert-congratulations alert-dismissable flex clearfix">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="col-md-2 text-center p0">
                <i class="icon fa fa-gift"></i>
            </div>
            <div class="col-md-10 p-r">
                <?= Yii::app()->user->getFlash('congratulations'); ?>
            </div>
        </div>
    </div>

    <?php endif; ?>

<div class="menu-open-hide">
    
</div>

<?php 
    Yii::app()->clientScript->registerScript('notify-message-out',' 
        $(".site-hint").tooltip();

        setTimeout(function() {
            jQuery(".notify-message").addClass("bounceOutRight");
        }, 4000);


        // TIMER
          jQuery(".timer").startTimer();
        ',

       CClientScript::POS_READY
    );
?>



<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31559818 = new Ya.Metrika({
                    id:31559818,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31559818" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>