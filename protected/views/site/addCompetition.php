<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 03.08.2015
 * Time: 20:50
 * @var $model AddCompetition
 * @var $form CActiveForm
 */
?>

<div class="container-fluid m-t profile">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center setting-page-dsc p0">
            <h1>Разместите свой конкурс на Giftbee!</h1>
            <span>Отправьте нам заявку, укажите сылки на свои группы и какой приз хотели бы разыграть. Мы обязательно изучим Ваше предложение и свяжемся с Вами. Это абсолютно бесплатно!</span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
            <div class="user-info-board clearfix">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'add-form',
                    'enableAjaxValidation'=>false,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit'=>true,
                    ),
                    'htmlOptions'=>array('class'=>'form-inline form-1'),
                )); ?>

                <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                            <?= $form->textField($model,'name', array('class'=>'form-control first-name', 'placeholder'=>$model->attributeLabels()['name'])); ?>
                            <?= $form->error($model,'name', array('class'=>'danger-message')); ?>
                            <!-- <div class="message"><?= $model->attributeLabels()['name']; ?></div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                            <?= $form->textField($model,'telephone', array('class'=>'form-control first-name', 'placeholder'=>$model->attributeLabels()['telephone'])); ?>
                            <?= $form->error($model,'telephone', array('class'=>'danger-message')); ?>
                            <!-- <div class="message"><?= $model->attributeLabels()['telephone']; ?></div> -->
                        </div>
                    </div>
                </div>

                <!-- email -->
                <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                            <?= $form->textField($model,'email', array('class'=>'form-control first-name', 'placeholder'=>'Почта для обратной связи')); ?>
                            <?= $form->error($model,'email', array('class'=>'danger-message')); ?>
                        </div>
                    </div>
                </div>
                <!-- end_email -->

                <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-facebook-square"></i></div>
                            <?= $form->textField($model,'facebook_url', array('class'=>'form-control first-name', 'placeholder'=>$model->attributeLabels()['facebook_url'])); ?>
                            <?= $form->error($model,'facebook_url', array('class'=>'danger-message')); ?>
                            <!-- <div class="message"><?= $model->attributeLabels()['facebook_url']; ?></div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-vk"></i></div>
                            <?= $form->textField($model,'vk_url', array('class'=>'form-control first-name', 'placeholder'=>$model->attributeLabels()['vk_url'])); ?>
                            <?= $form->error($model,'vk_url', array('class'=>'danger-message')); ?>
                            <!-- <div class="message"><?= $model->attributeLabels()['vk_url']; ?></div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                            <?= $form->textArea($model,'comments', array('class'=>'form-control first-name', 'placeholder'=>$model->attributeLabels()['comments'])); ?>
                            <?= $form->error($model,'comments', array('class'=>'danger-message')); ?>
                            <!-- <div class="message"><?= $model->attributeLabels()['comments']; ?></div> -->
                        </div>
                    </div>
                </div>
                <!-- END WIDGET -->

                <div class="col-xs-12 col-md-12">
                    <?= CHtml::submitButton('Отправить', array('class'=>'btn btn-green')); ?>
                </div>
                <?php $this->endWidget(); ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
