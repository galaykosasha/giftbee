<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 29.07.2015
 * Time: 14:17
 */
$this->pageTitle = "Что такое Giftbee?";
?>
<style>
	.content {
		background: #cc3a18;
	}

	.copyright {
		color: #fff;
	}

	h1 {
		color: #fff;
		font-weight: 900;
		font-size: 2em;
	}
</style>
<!-- COMP LOGO -->
    <div class="col-md-12 top_bar-logo">
      <a href="/" ><img src="<?= Yii::app()->createUrl('/images'); ?>/logo-top_bar1.png" alt=""></a>
    </div>
<!-- END COMP LOGO -->
<h1 class="text-center">Что такое Giftbee?</h1>
<div class="pages-content text-center clearfix">

<div class="row">
	<div class="col-md-12">
		<div class="col-md-12">
			<p>Ежедневно в социальных сетях проводится сотни конкурсов. Этот проект возник как необходимость объединить в одном месте много конкурсов и подарков. Мы гарантируем вручение приза! <b>С чего начать?</b></p> 
		</div>
	</div>
</div>


<div class="row faq-steps">
	<figure class="col-sm-4 col-md-4">
		<!-- <img src="images/w1.png" alt=""> -->
		<i class="fa fa-user animated"></i>
		<figcaption>
			<b><?php if(Yii::app()->user->isGuest): ?><a href="" data-toggle="modal" data-target=".modal-1">Зарегистрируйтесь</a><?php endif; ?><?php if(!Yii::app()->user->isGuest): ?>Зарегистрируйтесь<?php endif; ?></b> на сайте и выберите конкурс
		</figcaption>
	</figure>
	<figure class="col-sm-4 col-md-4">
		<!-- <img src="images/w2.png" alt=""> -->
		<i class="fa fa-check-square-o animated"></i>
		<figcaption>
			Выполните условия конкурса и получите номер участника
		</figcaption>
	</figure>
	<figure class="col-sm-4 col-md-4">
		<!-- <img src="images/w3.png" alt=""> -->
		<i class="fa fa-gift animated"></i>
		<figcaption>
			Мы выберем случайный номер и вручим приз победителю!
		</figcaption>
	</figure>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="col-md-12">
			<iframe class="video-size" width="640" height="360" src="https://www.youtube.com/embed/qZt3qpox5ws?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe><br><br>
			<p>У нас все честно и просто! Убедитесь в этом сами...</p>
		</div>
	</div>
</div>
</div>

<div class="text-center">
	<div class="col-md-12">		
		<a style="margin-top: 40px; color: #cc3a18; font-weight: 600;" href="<?= Yii::app()->createUrl('/site/competitions'); ?>" class="btn btn-yellow">Начать сейчас</a>
		<p style="color: #831d03; font-size: 1.2em;">Остались вопросы или нужна помощь?</p>
		<p style="color: #faaa98; font-size: 1.25em;">Посетить раздел <a  style="color: #faaa98;" href="<?= Yii::app()->createUrl('/site/instruction'); ?>">Инструкции</a></p>
	</div>
</div>
