<?php
/**
 * @var $data Competitions
 */
?>

<div class="item">
    <figure>
        <a href="<?= Yii::app()->createUrl('/site/competition', array('id'=>$data->id)); ?>">
            <?= CHtml::image(Yii::app()->request->baseUrl.'/uploads/competitions/xs-images/'.$data->min_image, ''); ?>
        </a>
        <figcaption>
            <h2><a href="<?= Yii::app()->createUrl('/site/competition', array('id'=>$data->id)); ?>" ><?= $data->company_id; ?></a></h2>
            <p><?= $data->title_ru; ?></p>
        </figcaption>
        <div class="col-xs-6 col-sm-6 col-md-6 info-board border-right">
            <div class="info-board_line"><i class="fa fa-clock-o"></i><span> 23:59:59</span></div>
            <div><i class="fa fa-user"></i><span> 2398</span></div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 info-board">
            <div class="info-board_line"><i class="fa fa-gift"></i><span> <?= $data->presents_count; ?></span></div>
            <div>
                <i class="fa fa-map-marker"></i>
                <span>
                    <?= $data->country->name; ?>
                    <?= !empty($data->region_id) ? ','.$data->region->name : ''; ?>
                    <?= !empty($data->city_id) ? ','.$data->city->name : ''; ?>
                </span>
            </div>
        </div>
        <div class="clearfix"></div>
    </figure>
</div>