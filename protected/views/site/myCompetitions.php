<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 30.07.2015
 * Time: 15:02
 */
?>
    <!-- GRID GIFTS -->
    <div class="container pages-top-mrg">
        <div class="row">
            <!-- COMP LOGO -->
            <div class="col-md-12 top_bar-logo">
              <a href="/" ><img src="<?= Yii::app()->createUrl('/images'); ?>/logo-top_bar.png" alt=""></a>
            </div>
            <!-- END COMP LOGO -->
            <h1 class="text-center">Мои конкурсы</h1>
            <div id="giftbee-start" class="gb-grid">
                <?php $this->widget('application.widgets.ItemsWidget', array('model'=>$competitions)); ?>
            </div>
        </div>
    </div>
    <!-- END GRID GIFTS -->

    <!-- MORE -->
    <!--<div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="more-btn btn btn-green" id="more-competitions">Еще</span>
            </div>
        </div>
    </div>-->
    <!-- END MORE -->

<?php Yii::app()->clientScript->registerScript('more-competitions', '
var ua = navigator.userAgent.toLowerCase(), isOpera = (ua.indexOf("opera")  > -1), isIE = (!isOpera && ua.indexOf("msie") > -1), stop = true;

function getDocumentHeight(){ // высота всей страницы вместе с невидимой частью
    return Math.max(document.compatMode!="CSS1Compat"?document.body.scrollHeight:document.documentElement.scrollHeight,getViewportHeight());
}

function getViewportHeight(){ // высота браузера
    return ((document.compatMode||isIE)&&!isOpera)?(document.compatMode=="CSS1Compat")?document.documentElement.clientHeight:document.body.clientHeight:(document.parentWindow||document.defaultView).innerHeight;
}

jQuery(window).scroll(function (event) {
    event.preventDefault();
    var s = getDocumentHeight() - getViewportHeight();
    var ls =  s - $(this).scrollTop();


    if(ls <= 300 && stop == true) {
        stop = false;
        var itemCount = jQuery(".item").length;
        $.ajax({
             url: "'.Yii::app()->createUrl('/ajax/moreMyCompetitions').'",
             global: false,
             type: "POST",
             data: ({count : itemCount}),
             success: function (html) {
                jQuery(".gb-grid").append(html);
             }
         });
    }
});

', CClientScript::POS_END); ?>