<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 30.07.2015
 * Time: 15:02
 */
?>
    <!-- GRID GIFTS -->
    <div class="container pages-top-mrg">
        <div class="row">
            <!-- COMP LOGO -->
            <div class="col-md-12 top_bar-logo">
                <a href="/" ><img src="<?= Yii::app()->createUrl('/images'); ?>/logo-top_bar.png" alt=""></a>
            </div>
            <!-- END COMP LOGO -->


            <h1 class="text-center">Мои Скидки</h1>
            <div class="sale-item_wrp col-md-12">
            <?php foreach($competitions as $competition): ?>
                <div class="user-info-board sale-item">
                    <div class="sale-item-block col-xs-12 col-md-2 first-sale-block">
                        <?= CHtml::image(Yii::app()->baseUrl.'/uploads/companies/logos/'.$competition->company->image, $competition->company->name_ru); ?>
                        <p><?= $competition->company->name_ru; ?></p>
                    </div>
                    <div class="sale-item-block col-xs-12 col-md-2"><a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$competition->company->url, 'competition_url'=>$competition->url)); ?>"><?= $this->getShortDescription($competition->title_ru, 50); ?></a></div>
                    <div class="sale-item-block col-xs-6 col-sm-6 col-md-1 p0">
                        <?php if($competition->vkUserIsParticipant): ?>
                            <?php $this->renderPartial('_users', array('data'=>UserCompetitions::model()->find('competition_id = :c_id AND social_id = 1 AND user_id = :u_id', array(
                                ':u_id'=>Yii::app()->user->id,
                                ':c_id'=>$competition->id,
                            )))); ?>
                        <?php endif; ?>
                    </div>

                    <div class="sale-item-block col-xs-6 col-sm-6 col-md-1 p0">
                        <?php if($competition->fbUserIsParticipant): ?>
                            <?php $this->renderPartial('_users', array('data'=>UserCompetitions::model()->find('competition_id = :c_id AND social_id = 2 AND user_id = :u_id', array(
                                ':u_id'=>Yii::app()->user->id,
                                ':c_id'=>$competition->id,
                            )))); ?>
                        <?php endif; ?>
                    </div>
                    <div class="sale-item-block col-xs-12 col-md-4"><?= $this->getShortDescription($competition->sale_description, 150); ?></div>

                    <div class="sale-item-block last-sale-block col-xs-12 col-md-2 text-center" style="background: <?= $competition->sale_type == 1 ? '#faaa18' : '#94c451'; ?>;"><?= $competition->sale; ?></div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- END GRID GIFTS -->

<?php Yii::app()->clientScript->registerScript('more-sales', '
var ua = navigator.userAgent.toLowerCase(), isOpera = (ua.indexOf("opera")  > -1), isIE = (!isOpera && ua.indexOf("msie") > -1), stop = true;

function getDocumentHeight(){ // высота всей страницы вместе с невидимой частью
    return Math.max(document.compatMode!="CSS1Compat"?document.body.scrollHeight:document.documentElement.scrollHeight,getViewportHeight());
}

function getViewportHeight(){ // высота браузера
    return ((document.compatMode||isIE)&&!isOpera)?(document.compatMode=="CSS1Compat")?document.documentElement.clientHeight:document.body.clientHeight:(document.parentWindow||document.defaultView).innerHeight;
}

jQuery(window).scroll(function (event) {
    event.preventDefault();
    var s = getDocumentHeight() - getViewportHeight();
    var ls =  s - $(this).scrollTop();


    if(ls <= 300 && stop == true) {
        stop = false;
        var itemCount = jQuery(".item").length;
        $.ajax({
             url: "'.Yii::app()->createUrl('/ajax/moreMySales').'",
             global: false,
             type: "POST",
             data: ({count : itemCount}),
             success: function (html) {
                jQuery(".gb-grid").append(html);
             }
         });
    }
});

', CClientScript::POS_END); ?>