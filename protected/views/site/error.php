<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
Yii::app()->ClientScript->registerCss('style404', '
	.gb-page-error .bee {
	  display: inline-block;
	  position: relative;
	  width: 130px;
	  height: 90px;
	  z-index: 100;
	  background-color: #61555a;
	  border-radius: 100% 98% 98% 100%;
	  box-shadow: inset 0px -10px 0px -2px rgba(0, 0, 0, 0.15), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	  -moz-transform: rotate(2deg);
	  -ms-transform: rotate(2deg);
	  -webkit-transform: rotate(2deg);
	  transform: rotate(2deg);
	  -moz-animation: float 3s infinite;
	  -webkit-animation: float 3s infinite;
	  animation: float 3s infinite;
	}
	.gb-page-error .bee:before, .bee:after {
	  display: block;
	  content: "";
	  position: absolute;
	}
	.gb-page-error .bee:before {
	  right: 24px;
	  bottom: 57%;
	  width: 8px;
	  height: 15px;
	  border-radius: 100% 100% 88% 88%;
	  background-color: #fff;
	  box-shadow: 10px 0px 0px -2px #fff;
	  -moz-animation: blink 7s 0 infinite;
	  -webkit-animation: blink 7s 0 infinite;
	  animation: blink 7s 0 infinite;
	}
	.gb-page-error .bee:after {
	  left: -7px;
	  top: 50%;
	  width: 20px;
	  height: 10px;
	  border-radius: 100% 100% 0 0%;
	  border-top: 5px solid #61555a;
	}
	.gb-page-error .bee .antennae {
	  position: relative;
	}
	.gb-page-error .bee .antennae:before, .bee .antennae:after {
	  display: block;
	  content: "";
	  position: absolute;
	}
	.gb-page-error .bee .antennae:before {
	  top: 4px;
	  right: 2px;
	  height: 32px;
	  width: 18px;
	  z-index: -1;
	  border-left: 4px solid #61555a;
	  border-radius: 100% 0 0 100%;
	  -moz-transform: rotate(30deg);
	  -ms-transform: rotate(30deg);
	  -webkit-transform: rotate(30deg);
	  transform: rotate(30deg);
	  -moz-animation: antenna 5s infinite;
	  -webkit-animation: antenna 5s infinite;
	  animation: antenna 5s infinite;
	}
	.gb-page-error .bee .antennae:after {
	  top: -4px;
	  right: 20px;
	  height: 32px;
	  width: 6px;
	  z-index: -1;
	  border-left: 4px solid #61555a;
	  border-radius: 100% 0 0 100%;
	  -moz-transform: rotate(14deg);
	  -ms-transform: rotate(14deg);
	  -webkit-transform: rotate(14deg);
	  transform: rotate(14deg);
	  -moz-animation: antenna 10s 0 infinite;
	  -webkit-animation: antenna 10s 0 infinite;
	  animation: antenna 10s 0 infinite;
	}
	.gb-page-error .bee .wing {
	  position: absolute;
	  background-color: rgba(0, 0, 0, 0.2);
	  border-radius: 100% 100% 100% 100%;
	}
	.gb-page-error .bee .wing.one {
	  width: 34px;
	  height: 52px;
	  top: -44px;
	  left: 27px;
	  box-shadow: inset 6px 1px 0px -4px rgba(0, 0, 0, 0.3);
	  -moz-transform-origin: 50% 100%;
	  -ms-transform-origin: 50% 100%;
	  -webkit-transform-origin: 50% 100%;
	  transform-origin: 50% 100%;
	  -moz-transform: rotate(-30deg);
	  -ms-transform: rotate(-30deg);
	  -webkit-transform: rotate(-30deg);
	  transform: rotate(-30deg);
	  -moz-animation: wings 0.01s 0 infinite;
	  -webkit-animation: wings 0.01s 0 infinite;
	  animation: wings 0.01s 0 infinite;
	}
	.gb-page-error .bee .wing.two {
	  width: 32px;
	  height: 44px;
	  top: -48px;
	  left: 32px;
	  box-shadow: 3px 1px 0px -1px rgba(0, 0, 0, 0.4);
	  -moz-transform-origin: 50% 100%;
	  -ms-transform-origin: 50% 100%;
	  -webkit-transform-origin: 50% 100%;
	  transform-origin: 50% 100%;
	  -moz-transform: rotate(50deg);
	  -ms-transform: rotate(50deg);
	  -webkit-transform: rotate(50deg);
	  transform: rotate(50deg);
	  -moz-animation: wingstwo 0.01s 0 infinite;
	  -webkit-animation: wingstwo 0.01s 0 infinite;
	  animation: wingstwo 0.01s 0 infinite;
	}

	.gb-page-error .shadow {
	  position: absolute;
	  width: 90px;
	  height: 30px;
	  z-index: 10;
	  top: 62%;
	  left: 50%;
	  margin-left: -45px;
	  border-radius: 100%;
	  background-color: rgba(0, 0, 0, 0.2);
	  -moz-animation: shadow 3s infinite;
	  -webkit-animation: shadow 3s infinite;
	  animation: shadow 3s infinite;
	}

	@-webkit-keyframes float {
	  0% {
	    box-shadow: inset 0px -11px 0px -2px rgba(0, 0, 0, 0.2), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 7px);
	    -ms-transform: translate(0, 7px);
	    -webkit-transform: translate(0, 7px);
	    transform: translate(0, 7px);
	  }
	  50% {
	    box-shadow: inset 0px -10px 0px -2px rgba(0, 0, 0, 0.15), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 0);
	    -ms-transform: translate(0, 0);
	    -webkit-transform: translate(0, 0);
	    transform: translate(0, 0);
	  }
	  100% {
	    box-shadow: inset 0px -11px 0px -2px rgba(0, 0, 0, 0.2), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 7px);
	    -ms-transform: translate(0, 7px);
	    -webkit-transform: translate(0, 7px);
	    transform: translate(0, 7px);
	  }
	}
	@-moz-keyframes float {
	  0% {
	    box-shadow: inset 0px -11px 0px -2px rgba(0, 0, 0, 0.2), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 7px);
	    -ms-transform: translate(0, 7px);
	    -webkit-transform: translate(0, 7px);
	    transform: translate(0, 7px);
	  }
	  50% {
	    box-shadow: inset 0px -10px 0px -2px rgba(0, 0, 0, 0.15), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 0);
	    -ms-transform: translate(0, 0);
	    -webkit-transform: translate(0, 0);
	    transform: translate(0, 0);
	  }
	  100% {
	    box-shadow: inset 0px -11px 0px -2px rgba(0, 0, 0, 0.2), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 7px);
	    -ms-transform: translate(0, 7px);
	    -webkit-transform: translate(0, 7px);
	    transform: translate(0, 7px);
	  }
	}
	@-ms-keyframes float {
	  0% {
	    box-shadow: inset 0px -11px 0px -2px rgba(0, 0, 0, 0.2), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 7px);
	    -ms-transform: translate(0, 7px);
	    -webkit-transform: translate(0, 7px);
	    transform: translate(0, 7px);
	  }
	  50% {
	    box-shadow: inset 0px -10px 0px -2px rgba(0, 0, 0, 0.15), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 0);
	    -ms-transform: translate(0, 0);
	    -webkit-transform: translate(0, 0);
	    transform: translate(0, 0);
	  }
	  100% {
	    box-shadow: inset 0px -11px 0px -2px rgba(0, 0, 0, 0.2), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 7px);
	    -ms-transform: translate(0, 7px);
	    -webkit-transform: translate(0, 7px);
	    transform: translate(0, 7px);
	  }
	}
	@keyframes float {
	  0% {
	    box-shadow: inset 0px -11px 0px -2px rgba(0, 0, 0, 0.2), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 7px);
	    -ms-transform: translate(0, 7px);
	    -webkit-transform: translate(0, 7px);
	    transform: translate(0, 7px);
	  }
	  50% {
	    box-shadow: inset 0px -10px 0px -2px rgba(0, 0, 0, 0.15), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 0);
	    -ms-transform: translate(0, 0);
	    -webkit-transform: translate(0, 0);
	    transform: translate(0, 0);
	  }
	  100% {
	    box-shadow: inset 0px -11px 0px -2px rgba(0, 0, 0, 0.2), inset 32px 0px 0px -20px #61555a, inset 48px 0px 0px -20px #fcd658, inset 58px 0px 0px -20px #61555a, inset 88px 0px 0px -28px #fcd658;
	    -moz-transform: translate(0, 7px);
	    -ms-transform: translate(0, 7px);
	    -webkit-transform: translate(0, 7px);
	    transform: translate(0, 7px);
	  }
	}
	@-webkit-keyframes shadow {
	  0% {
	    margin-left: -49px;
	    width: 98px;
	  }
	  50% {
	    margin-left: -45px;
	    width: 90px;
	  }
	  100% {
	    margin-left: -49px;
	    width: 98px;
	  }
	}
	@-moz-keyframes shadow {
	  0% {
	    margin-left: -49px;
	    width: 98px;
	  }
	  50% {
	    margin-left: -45px;
	    width: 90px;
	  }
	  100% {
	    margin-left: -49px;
	    width: 98px;
	  }
	}
	@-ms-keyframes shadow {
	  0% {
	    margin-left: -49px;
	    width: 98px;
	  }
	  50% {
	    margin-left: -45px;
	    width: 90px;
	  }
	  100% {
	    margin-left: -49px;
	    width: 98px;
	  }
	}
	@keyframes shadow {
	  0% {
	    margin-left: -49px;
	    width: 98px;
	  }
	  50% {
	    margin-left: -45px;
	    width: 90px;
	  }
	  100% {
	    margin-left: -49px;
	    width: 98px;
	  }
	}
	@-webkit-keyframes antenna {
	  0% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  30% {
	    -moz-transform: rotate(-8deg);
	    -ms-transform: rotate(-8deg);
	    -webkit-transform: rotate(-8deg);
	    transform: rotate(-8deg);
	  }
	  79% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  80% {
	    -moz-transform: rotate(10deg);
	    -ms-transform: rotate(10deg);
	    -webkit-transform: rotate(10deg);
	    transform: rotate(10deg);
	  }
	  81% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  100% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	}
	@-moz-keyframes antenna {
	  0% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  30% {
	    -moz-transform: rotate(-8deg);
	    -ms-transform: rotate(-8deg);
	    -webkit-transform: rotate(-8deg);
	    transform: rotate(-8deg);
	  }
	  79% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  80% {
	    -moz-transform: rotate(10deg);
	    -ms-transform: rotate(10deg);
	    -webkit-transform: rotate(10deg);
	    transform: rotate(10deg);
	  }
	  81% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  100% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	}
	@-ms-keyframes antenna {
	  0% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  30% {
	    -moz-transform: rotate(-8deg);
	    -ms-transform: rotate(-8deg);
	    -webkit-transform: rotate(-8deg);
	    transform: rotate(-8deg);
	  }
	  79% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  80% {
	    -moz-transform: rotate(10deg);
	    -ms-transform: rotate(10deg);
	    -webkit-transform: rotate(10deg);
	    transform: rotate(10deg);
	  }
	  81% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  100% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	}
	@keyframes antenna {
	  0% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  30% {
	    -moz-transform: rotate(-8deg);
	    -ms-transform: rotate(-8deg);
	    -webkit-transform: rotate(-8deg);
	    transform: rotate(-8deg);
	  }
	  79% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  80% {
	    -moz-transform: rotate(10deg);
	    -ms-transform: rotate(10deg);
	    -webkit-transform: rotate(10deg);
	    transform: rotate(10deg);
	  }
	  81% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	  100% {
	    -moz-transform: rotate(0);
	    -ms-transform: rotate(0);
	    -webkit-transform: rotate(0);
	    transform: rotate(0);
	  }
	}
	@-webkit-keyframes blink {
	  0% {
	    height: 14px;
	  }
	  30% {
	    height: 14px;
	  }
	  31% {
	    height: 1px;
	  }
	  32% {
	    height: 14px;
	  }
	  88% {
	    height: 14px;
	  }
	  89% {
	    height: 1px;
	  }
	  90% {
	    height: 14px;
	  }
	  100% {
	    height: 14px;
	  }
	}
	@-moz-keyframes blink {
	  0% {
	    height: 14px;
	  }
	  30% {
	    height: 14px;
	  }
	  31% {
	    height: 1px;
	  }
	  32% {
	    height: 14px;
	  }
	  88% {
	    height: 14px;
	  }
	  89% {
	    height: 1px;
	  }
	  90% {
	    height: 14px;
	  }
	  100% {
	    height: 14px;
	  }
	}
	@-ms-keyframes blink {
	  0% {
	    height: 14px;
	  }
	  30% {
	    height: 14px;
	  }
	  31% {
	    height: 1px;
	  }
	  32% {
	    height: 14px;
	  }
	  88% {
	    height: 14px;
	  }
	  89% {
	    height: 1px;
	  }
	  90% {
	    height: 14px;
	  }
	  100% {
	    height: 14px;
	  }
	}
	@keyframes blink {
	  0% {
	    height: 14px;
	  }
	  30% {
	    height: 14px;
	  }
	  31% {
	    height: 1px;
	  }
	  32% {
	    height: 14px;
	  }
	  88% {
	    height: 14px;
	  }
	  89% {
	    height: 1px;
	  }
	  90% {
	    height: 14px;
	  }
	  100% {
	    height: 14px;
	  }
	}
	@-webkit-keyframes wings {
	  0% {
	    -moz-transform: rotate(-36deg);
	    -ms-transform: rotate(-36deg);
	    -webkit-transform: rotate(-36deg);
	    transform: rotate(-36deg);
	  }
	  50% {
	    -moz-transform: rotate(-40deg);
	    -ms-transform: rotate(-40deg);
	    -webkit-transform: rotate(-40deg);
	    transform: rotate(-40deg);
	  }
	  100% {
	    -moz-transform: rotate(-36deg);
	    -ms-transform: rotate(-36deg);
	    -webkit-transform: rotate(-36deg);
	    transform: rotate(-36deg);
	  }
	}
	@-moz-keyframes wings {
	  0% {
	    -moz-transform: rotate(-36deg);
	    -ms-transform: rotate(-36deg);
	    -webkit-transform: rotate(-36deg);
	    transform: rotate(-36deg);
	  }
	  50% {
	    -moz-transform: rotate(-40deg);
	    -ms-transform: rotate(-40deg);
	    -webkit-transform: rotate(-40deg);
	    transform: rotate(-40deg);
	  }
	  100% {
	    -moz-transform: rotate(-36deg);
	    -ms-transform: rotate(-36deg);
	    -webkit-transform: rotate(-36deg);
	    transform: rotate(-36deg);
	  }
	}
	@-ms-keyframes wings {
	  0% {
	    -moz-transform: rotate(-36deg);
	    -ms-transform: rotate(-36deg);
	    -webkit-transform: rotate(-36deg);
	    transform: rotate(-36deg);
	  }
	  50% {
	    -moz-transform: rotate(-40deg);
	    -ms-transform: rotate(-40deg);
	    -webkit-transform: rotate(-40deg);
	    transform: rotate(-40deg);
	  }
	  100% {
	    -moz-transform: rotate(-36deg);
	    -ms-transform: rotate(-36deg);
	    -webkit-transform: rotate(-36deg);
	    transform: rotate(-36deg);
	  }
	}
	@keyframes wings {
	  0% {
	    -moz-transform: rotate(-36deg);
	    -ms-transform: rotate(-36deg);
	    -webkit-transform: rotate(-36deg);
	    transform: rotate(-36deg);
	  }
	  50% {
	    -moz-transform: rotate(-40deg);
	    -ms-transform: rotate(-40deg);
	    -webkit-transform: rotate(-40deg);
	    transform: rotate(-40deg);
	  }
	  100% {
	    -moz-transform: rotate(-36deg);
	    -ms-transform: rotate(-36deg);
	    -webkit-transform: rotate(-36deg);
	    transform: rotate(-36deg);
	  }
	}
	@-webkit-keyframes wingstwo {
	  0% {
	    -moz-transform: rotate(50deg);
	    -ms-transform: rotate(50deg);
	    -webkit-transform: rotate(50deg);
	    transform: rotate(50deg);
	  }
	  50% {
	    -moz-transform: rotate(54deg);
	    -ms-transform: rotate(54deg);
	    -webkit-transform: rotate(54deg);
	    transform: rotate(54deg);
	  }
	  100% {
	    -moz-transform: rotate(50deg);
	    -ms-transform: rotate(50deg);
	    -webkit-transform: rotate(50deg);
	    transform: rotate(50deg);
	  }
	}
	@-moz-keyframes wingstwo {
	  0% {
	    -moz-transform: rotate(50deg);
	    -ms-transform: rotate(50deg);
	    -webkit-transform: rotate(50deg);
	    transform: rotate(50deg);
	  }
	  50% {
	    -moz-transform: rotate(54deg);
	    -ms-transform: rotate(54deg);
	    -webkit-transform: rotate(54deg);
	    transform: rotate(54deg);
	  }
	  100% {
	    -moz-transform: rotate(50deg);
	    -ms-transform: rotate(50deg);
	    -webkit-transform: rotate(50deg);
	    transform: rotate(50deg);
	  }
	}
	@-ms-keyframes wingstwo {
	  0% {
	    -moz-transform: rotate(50deg);
	    -ms-transform: rotate(50deg);
	    -webkit-transform: rotate(50deg);
	    transform: rotate(50deg);
	  }
	  50% {
	    -moz-transform: rotate(54deg);
	    -ms-transform: rotate(54deg);
	    -webkit-transform: rotate(54deg);
	    transform: rotate(54deg);
	  }
	  100% {
	    -moz-transform: rotate(50deg);
	    -ms-transform: rotate(50deg);
	    -webkit-transform: rotate(50deg);
	    transform: rotate(50deg);
	  }
	}
	@keyframes wingstwo {
	  0% {
	    -moz-transform: rotate(50deg);
	    -ms-transform: rotate(50deg);
	    -webkit-transform: rotate(50deg);
	    transform: rotate(50deg);
	  }
	  50% {
	    -moz-transform: rotate(54deg);
	    -ms-transform: rotate(54deg);
	    -webkit-transform: rotate(54deg);
	    transform: rotate(54deg);
	  }
	  100% {
	    -moz-transform: rotate(50deg);
	    -ms-transform: rotate(50deg);
	    -webkit-transform: rotate(50deg);
	    transform: rotate(50deg);
	  }
	}
	.gb-page-error html, .gb-page-error body {
	  height: 100%;
	  background-color: #fcfade;
	  font-size: 62.5%;
	  line-height: 3rem;
	}

	.gb-page-error .v-t {
	  display: table;
	  vertical-align: middle;
	  text-align: center;
	  height: 100%;
	  width: 100%;
	}
	.gb-page-error .v-t .v-tc {
	  display: table-cell;
	  vertical-align: middle;
	}

	.gb-page-error .background-gradient {
	  position: absolute;
	  top: 0;
	  left: 0;
	  width: 100%;
	  height: 100%;
	  z-index: 1;
	  pointer-events: none;
	  background: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PHJhZGlhbEdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIyMCUiIHN0b3AtY29sb3I9IiNmZWZkZjEiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmYWY2YzciIHN0b3Atb3BhY2l0eT0iMC4xIi8+PC9yYWRpYWxHcmFkaWVudD48L2RlZnM+PHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgZmlsbD0idXJsKCNncmFkKSIgLz48L3N2Zz4g");
	  background: -moz-radial-gradient(#fefdf1 20%, rgba(250, 246, 199, 0.1) 100%);
	  background: -webkit-radial-gradient(#fefdf1 20%, rgba(250, 246, 199, 0.1) 100%);
	  background: radial-gradient(#fefdf1 20%, rgba(250, 246, 199, 0.1) 100%);
	}

	.gb-page-error h1, .gb-page-error h2 {
	  font-family: "Montserrat", sans-serif;
	  font-weight: 600;
	  text-align: center;
	  text-transform: uppercase;
	  letter-spacing: 0.25em;
	  position: absolute;
	  width: 100%;
	  z-index: 10;
	}

	.gb-page-error h1 {
	  font-size: 18px;
	  font-size: 1.2em;
	  text-transform: none;
	  color: rgba(0, 0, 0, 0.5);
	  top: 16%;
	}

	.gb-page-error h2 {
	  bottom: 12%;
	  font-size: 10px;
	  font-size: 1.0rem;
	  color: rgba(0, 0, 0, 0.35);
	}

	.gb-page-error span {
	  font-size: 92px;
	  font-size: 9.2rem;
	  line-height: 98px;
	  z-index: 100;
	  text-shadow: 0.04em 0.04em 0 #fcfade;
	}
	.gb-page-error span, .gb-page-error span:after {
	  font-weight: 600;
	  color: #58c4b1;
	  white-space: nowrap;
	  display: inline-block;
	  position: relative;
	  letter-spacing: 0.16em;
	}
	.gb-page-error span:after {
	  content: "'. $code . '";
	  color: rgba(0, 0, 0, 0.25);
	  text-shadow: none;
	  position: absolute;
	  left: .08em;
	  top: .08em;
	  z-index: -1;
	  -webkit-mask-image: url(http://jonambas.com/valentines/public/images/mask.png);
	}
');
?>

 <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

<div class="container-fluid gb-page-error h100">
	<div class="row h100">
		<div class="v-t">
			<div class="v-tc">

				<h1><span><?php echo $code; ?></span><br><br><?php echo CHtml::encode($message); ?></h1>

				<div class="bee">
					<div class="antennae"></div>  
					<div class="wing one"></div>
					<div class="wing two"></div>
				</div>
				<div class="shadow"></div>

		     <h2><a href="/">Giftbee</a></h2>
			</div>
		</div>
		<div class="background-gradient"></div>
	</div>
</div>


