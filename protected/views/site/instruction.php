<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 05.08.2015
 * Time: 15:30
 */

$this->pageTitle = "Giftbee - Инструкция";
?>
<style>
	h1 {
		font-size: 2.5em;
		font-weight: 700;
	}

	h2 {
		max-width: 960px;
		margin: 40px auto 20px;
		font-weight: 600;
		padding: 20px 0;
	}

	.instruction img {
		max-width: 100%;
		padding: 0 20px;
	}

	.instruction-row figure {
		margin: 10px 0 60px;
	}


	.instruction-row figcaption {
		margin-top: 25px;
		text-align: left;
	}

	.instruction-nav {
		max-width: 960px;
		margin: 30px auto 0;
		list-style-type: none;
	}

	.instruction-nav li {
		display: inline-block;
		list-style-type: none;
	}

	.instruction-nav li a {
		padding: 5px 10px;
		text-transform: uppercase;
		text-decoration: none;
		color: #888;
		font-weight: 600;
	}

	.instruction-nav li.active a {
		color: #faaa18;
	}

/*	.instruction-row {
		margin: 30px 0;
	}*/
</style>
<h1 class="text-center">Инструкция</h1>

	<ul class="hidden-xs nav instruction-nav text-center">
		<li><a href="#step1">Регистрация</a></li>
		<li><a href="#step2">Конкурсы</a></li>
		<li><a href="#step3">Условия</a></li>
		<li><a href="#step4">Номер участника</a></li>
		<li><a href="#step5">Розыгрыш</a></li>
	</ul>



<h2 id="step1" >1. Регистрация</h2>
<div class="instruction">
	<div class="pages-content text-center clearfix">
		<div class="row instruction-row">
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_1.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Что бы участвовать в
				конкурсах Вам необходимо
				зарегистрироваться на giftbee.
				com.ua. Нажмите “Авторизация”
				в боковом меню или на странице
				конкурса. Выберите любой из
				социальных профилей
				</figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_3.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Введите адрес электронной почты.
				Мы отправим Вам уникальную
				ссылку для подтверждения,
				перейдите по ней. Если Вам не
				пришло письмо, проверьте СПАМ.
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_2.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Поздравляем, теперь Вы зарегистрированный пользователь! Вперед к подаркам.
				</figcaption>
			</figure>
		</div>

<!-- 		<div class="row instruction-row">
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_4.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Заполните недостающие поля в
				разделе Настройки. Если вдруг
				Вашего города не оказалось в
				списке выберите ближайший.
				Мы не передаем Ваши данные
				3-тим лицам. Они нужны нам чтоб
				подобрать для Вас интересные
				конкурсы.
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_5.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Подпишитесь на новости
				от Giftbee в виджете той
				социальной сети через которую
				Вы регистрировались. Это
				обязательное условия для участия
				в конкурсах. Если у Вас есть
				профиль в других социальных
				сетях – подтяните их и увеличьте
				свой шанс на получения приза.
				Просто нажмите на «+» и
				подпишитесь на новости от Giftbee
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_6.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Нажмите кнопку “Сохранить”.
				Если Вы все правильно сделали
				то увидете соответствующие
				сообщение.
				</figcaption>
			</figure>
		</div> -->
	</div>

	<h2 id="step2">2. Конкурсы</h2>
	<div class="pages-content text-center clearfix">
		<div class="row instruction-row">
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_7.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Откройте раздел «Конкурсы» в
				боковом меню. Вы увидите все
				активные конкурсы. Выбирайте
				любой из них.
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_8.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				На странице конкурса Вы увидите
				таймер, количество участников
				и призов, условия и участники.
				Все участники реальные люди
				с активными ссылками на свои
				профили.
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_9.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				По окончанию таймера Конкурс
				автоматически попадает в раздел
				«Архив» и ждет розыгрыша.
				После определения победителя
				в архивном конкурсе появляются
				победители.
				</figcaption>
			</figure>
		</div>
	</div>

	<h2 id="step3">3. Условия</h2>
	<div class="pages-content text-center clearfix">
		<div class="row instruction-row">
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_10.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Что бы стать участником конкурса
				и получить шанс на выигрыш Вы
				должны выполнить несколько
				простых условий. Обратите
				внимание что в каждом конкурсе
				условия могут быть разными.
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_11.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Например: Подписаться на
				страницы спонсора, поделиться
				страницей конкурса
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_12.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Или: Перейти на сайт спонсора,
				поделиться страницей сайта
				спонсора и поделиться страницей
				конкурса
				</figcaption>
			</figure>
		</div>

		<div class="row instruction-row">
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_13.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				После нажатия на кнопку
				«Участвую» Вы получаете
				порядковый номер. Вы можете
				увеличить свои шансы получив
				два порядковых номера. Для
				этого выполните условия в двух
				социальных сетях.
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_14.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Если у Вас подтянут всего один
				профиль, подтяните другой прямо
				на странице конкурса. Нажмите
				кнопку «Добавить»
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_15.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Обратите внимание что после
				получения номера Вы не сможете
				выполнять условия. Проверьте все
				перед тем как нажимать кнопку
				участвую. Если Вы случайно не
				выполните хоть одно условие то в
				случаи выигрыша мы не сможем
				подарить Вам приз(
				</figcaption>
			</figure>
		</div>
	</div>

	<h2 id="step4">4. Номер участника</h2>
	<div class="pages-content text-center clearfix">
		<div class="row instruction-row">
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_16.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				После нажатия на кнопку
				«Участвую» Вы получите
				порядковый номер. Он
				закрепляется за Вами. Вы так же
				можете видеть участников которые
				участвовали перед Вами.
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_17.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				Вы можете в любое время
				проверить себя в списке
				участников. Просто введите
				полученный номер в поиск
				</figcaption>
			</figure>
		</div>
	</div>

	<h2 id="step5">5. Розыгрыш и получение приза</h2>
	<div class="pages-content text-center clearfix">
		<div class="row instruction-row">
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_18.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				После завершения конкурса мы
				случайным образом выбираем
				один или несколько номером
				среди всех участников. Конкурс
				переносится в архив а вместо
				условий появляются победители и
				видео с розыгрыша.
				</figcaption>
			</figure>
			<figure class="col-md-4 clearfix">
				<img src="<?= Yii::app()->createUrl('/images'); ?>/giftbee_19.jpg" alt="">
				<figcaption class="col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0">
				В случаи выигрыша мы свяжемся с
				Вами по электронной почте и/или
				сообщение в социальных сетях.
				Договоримся о передаче призов.
				</figcaption>
			</figure>
		</div>
	</div>
</div>
