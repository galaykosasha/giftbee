<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 08.07.2015
 * Time: 11:37
 * @var $this SiteController
 * @var $competition Competitions
 * @var $users UserCompetitions
 */
$this->pageTitle = 'GiftBee: '.$competition->title_ru;
Yii::app()->clientScript->registerMetaTag(Yii::app()->baseUrl.'/uploads/competitions/md-images/'.$competition->md_image, 'og:image');
Yii::app()->clientScript->registerMetaTag($this->pageTitle, 'og:title');
Yii::app()->clientScript->registerMetaTag('website', 'og:type');
Yii::app()->clientScript->registerMetaTag(strip_tags($competition->description_ru), 'og:description');
?>
    <!-- COMPETITION -->
    <div class="container-fluid competition">
        <div class="row">
            <!-- COMP LOGO -->
            <div class="col-md-12 top_bar-logo">
                <a href="/" ><img src="<?= Yii::app()->createUrl('/images'); ?>/logo-top_bar.png" alt=""></a>
            </div>
            <!-- END COMP LOGO -->
            <!-- COMP-CONTENT -->
            <div class="col-md-12 comp-content-wrp">
                <!-- BREDACRUMBS -->
                <ol class="comp-content breadcrumbs breadcrumbs--comp" vocab="http://schema.org/" typeof="BreadcrumbList">
                    <li class="breadcrumbs__item" property="itemListElement" typeof="ListItem">
                        <a class="breadcrumbs__link" property="item" typeof="WebPage" href="<?= Yii::app()->createUrl('/site/index'); ?>"><span property="name">Главная</span></a><span class="breadcrumbs__devider"><i class="fa fa-arrow-circle-left"></i></span>
                        <meta property="position" content="1">
                    </li>
                    <li class="breadcrumbs__item" property="itemListElement" typeof="ListItem">
                        <a class="breadcrumbs__link" property="item" typeof="WebPage" href="<?= Yii::app()->createUrl('/site/archive'); ?>"><span property="name">Архив</span></a><span class="breadcrumbs__devider"><i class="fa fa-arrow-circle-left"></i></span>
                        <meta property="position" content="2">
                    </li>
                    <li class="breadcrumbs__item breadcrumbs__item--current" property="itemListElement" typeof="ListItem">
                        <span property="name" class="breadcrumbs__last-item"><?= $competition->title_ru; ?></span>
                        <meta property="position" content="3">
                    </li>
                </ol>
                <!-- END_BREDACRUMBS -->
                <div class="comp-content gb-clearfix">
                    <!-- COMP-HEADER -->
                    <div class="comp-header">
                        <div class="comp-logo">
                            <?= CHtml::image(Yii::app()->baseUrl.'/uploads/companies/logos/'.$competition->company->image, $competition->company->name_ru); ?>
                        </div>
                        <?= CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/md-images/'.$competition->md_image, $competition->company->name_ru, array('class'=>'comp-banner')); ?>
                        
                        <?php if(!empty($competition->images)): ?>
                            <a href="" class="comp-header__btn btn" data-toggle="modal" data-target=".modal-gallery"><i class="fa fa-camera"></i> <span>+ <?= sizeof($competition->images); ?></span></a>
                        <?php endif; ?>
                    </div>
                    <!-- COMP-INFO -->
                    <div class="comp-info gb-clearfix">
                        <div class="col-md-12 p0">
                            <!-- COMP-TITLE -->
                            <h1 class="comp-title"><?= $competition->title_ru; ?></h1>
                            <h3 class="comp-company-name">
                                <b><?= $competition->company->name_ru; ?></b>
                                <a href="<?= $competition->site_url; ?>" class="comp-outlink" target="_blank"><i class="fa fa-external-link"></i></a>
                            </h3>
                            <div class="comp-description">
                                <?= $competition->description_ru; ?>
                            </div>
                            <!-- COMP-OUTLINK -->
                        </div>
                        <div class="col-xs-7 col-sm-6 col-md-6 p0">
                            <!-- ARCHIVE -->
                            <div class="comp-archive">
                                <h3><i class="fa fa-archive"></i> Этот конкурс уже закончился</h3>
                            </div>
                        </div>

                        <!-- USERS AND GIFTS COUNT -->
                        <div class="col-xs-5 col-sm-6 col-md-6 p0">
                            <div class="comp-informer-wrp">
                                <div class="comp-informer">
                                    <a href=""><i class="fa fa-user"></i></a>
                                    <div>
                                        <div class="comp-informer-num">
                                            <?= (int)$competition->usersCount; ?>
                                            <div class="timer-info ">участников</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="comp-informer">
                                    <a href=""><i class="fa fa-gift"></i></a>
                                    <div class="comp-informer-border">
                                        <div class="comp-informer-num">
                                            <?= $competition->presents_count; ?>
                                            <div class="timer-info ">призов</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if(!(time() >= strtotime($competition->date_end) || ((int)$competition->max_users_count > 0 && (int)$competition->max_users_count <= (int)$competition->usersCount))): ?>

                            <?php if(!Yii::app()->user->isGuest): ?>
                                <div class="clearfix">
                                    <div class="col-md-12 text-center comp-helper-title p0">
                                        <h3>Для того чтобы стать участником конкурса выполните несколько простых условий:</h3>
                                    </div>
                                </div>
                            <?php endif; ?>

                        <?php endif; ?>
                    </div>

                    <!-- WINNERS  -->
                    <?php if($competition->winners): ?>
                        <div class="comp-winners">
                            <!-- WINNERS TITLE -->
                            <ul class="nav nav-tabs text-center" role="tablist">
                                <li role="presentation" class="active comp-fb"><a href="#comp-winners-tab" aria-controls="comp-winners-tab" role="tab" data-toggle="tab"><i class="fa fa-trophy"></i> Победители</a></li>
                            </ul>
                            <!-- END WINNERS TITLE -->

                            <!-- WINNERS CONTENT -->
                            <div class="tab-content text-center">
                                <div role="tabpanel" class="tab-pane active" id="comp-winners-tab">
                                    <?php $this->widget('application.widgets.WinnersWidget', array('competition_id'=>$competition->id, 'winners'=>$competition->winners, 'video'=>$competition->video)); ?>
                                </div>
                            </div>
                            <!-- END WINNERS CONTENT -->
                        </div>

                    <?php else: ?>
                        <div class="comp-winners">
                            <ul class="nav nav-tabs text-center" role="tablist">
                                <li role="presentation" class="active comp-fb"><a href="#comp-winners-tab" aria-controls="comp-winners-tab" role="tab" data-toggle="tab"><i class="fa fa-trophy"></i> Победители</a></li>
                            </ul>
                            <div class="tab-content text-center">
                                <div role="tabpanel" class="tab-pane active" id="comp-winners-tab">
                                    <div class="winner-message">
                                        <span><i class="fa fa-forumbee"></i> Мы выбираем победителя</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- USER-BOARD -->
                    <div class="comp-user-board clearfix">
                        <h2 class="comp-user-board__title">Все участники конкурса:</h2>
                        <?php if($users->itemCount > 0): ?>
                            <div class="form-group comp-user-board__search">
                                <div class="input-group comp-find-user">
                                    <?= CHtml::textField('find_number', '', array('id'=>'find-user', 'placeholder'=>'Поиск по номеру участника')); ?>
                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!-- <img class="bb-user-preloader" src="http://photojournals.ru/mozaika/images/preloader2.gif" alt=""> -->
                        <div class="bb-user-preloader">
                            <i class="fa fa-gift"></i>
                        </div>
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider'=>$users,
                            'ajaxUpdate'=>true,
                            'itemView'=>'_users',
                            'template'=>'{items}<div class="clearfix"></div>{pager}',
                            'cssFile'=>false,
                            'viewData'=>array('count'=>$users->totalItemCount),
                            'pager'=>array(
                                'maxButtonCount' => 5,
                                'lastPageLabel'=> false,
                                'nextPageLabel'=>'>',
                                'prevPageLabel'=>'<',
                                'firstPageLabel'=> false,
                                'class'=>'CLinkPager',
                                'header'=>false,
                                'cssFile'=>null, // устанавливаем свой .css файл
                                'htmlOptions'=>array('class'=>'sad'),
                            ),
                            'pagerCssClass'=>'pager',
                            'sortableAttributes'=>array(
                                'rating',
                                'create_time',
                            ),
                            'emptyText'=>'Стань первым',
                            'itemsCssClass'=>'competition-users',
                            'htmlOptions'=>array('id'=>'users', 'class'=>'col-md-12')
                        ));
                        ?>
                    </div>
                    <!-- END USER-BOARD -->
                    <?php if(Yii::app()->user->role == 'admin'): ?>
                        <div class="comp-message-conainer">
                            <button id="add-winner" class="btn btn-yellow">Определить победителя</button>
                        </div>
                    <?php endif; ?>

                    <?php if(Yii::app()->user->isGuest): ?>
                        <div class="comp-message-conainer">
                            <div class="col-md-12">
                                <h2>Этот конкурс уже закончился. Но Вы можете испытать свои шансыв других конкурсах, для этого авторизируйтесь.</h2>
                            </div>
                            
                            <p>Выберите одну из социальных сетей:</p>
                            <div class="login-group_button">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if(Yii::app()->session['social_id'] != 2): ?>
                                            <?= CHtml::link('<i class="pull-left text-center fa fa-facebook"></i><span>Facebook</span>', array('/site/loginFb', 'url'=>true), array('class'=>'login-item btn-group fb-color')); ?>
                                        <?php endif; ?>

                                        <?php if(Yii::app()->session['social_id'] != 1): ?>
                                            <?= CHtml::link('<i class="pull-left text-center fa fa-vk"></i><span>Вконтакте</span>', array('/site/loginVk', 'url'=>true), array('class'=>'login-item btn-group vk-color')); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>

    <?php if(!empty($competition->images)): ?>
    <!-- MODAL GALLERY -->
    <div class="modal modal-gallery" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg modal-full-screen">
                <div class="modal-content text-center">
                    <div class="comp-gallery">
                        <div class="slider-nav hidden-xs">
                            <?php foreach($competition->images as $images): ?>
                                <div><img class="slider-nav__img" src="<?= Yii::app()->baseUrl; ?>/uploads/competitions/extra/<?= $images->image; ?>" alt=""></div>
                            <?php endforeach; ?>
                        </div>
                        <div class="slider-for">
                            <?php foreach($competition->images as $images): ?>
                                <div><img class="slider-for__img" src="<?= Yii::app()->baseUrl; ?>/uploads/competitions/extra/<?= $images->image; ?>" alt=""></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <i class="fa fa-times" data-dismiss="modal"></i>
                </div>
        </div>
    </div>
    <?php endif; ?>
    <!-- <script type="text/javascript" src="//vk.com/js/api/openapi.js?113"></script> -->
    <!-- END MODAL GALLERY -->

    <!-- COMP-PAGE-SCRIPT-->
<?php Yii::app()->clientScript->registerScript('find-user', '
    jQuery(function(){

    // SLICK GALLERY
    $(".modal-gallery").on("shown.bs.modal", function (e) {
        $(".slider-for").slick({
         slidesToShow: 1,
         slidesToScroll: 1,
         arrows: false,
         fade: false,
         asNavFor: ".slider-nav",
         draggable: true,
         touchMove: true,
         adaptiveHeight: true,
       });

       $(".slider-nav").slick({
         slidesToShow: 5,
         slidesToScroll: 1,
         asNavFor: ".slider-for",
         dots: false,
         centerMode: false,
         focusOnSelect: true,
         draggable: true,
         touchMove: true,
         adaptiveHeight: false,
         variableWidth: true
       });
       
       // $(".comp-gallery").addClass("zoomIn").css("opacity", "1");
    });
});


   jQuery("#find-user").on("keyup", function(){
        if(jQuery("#find-user").val() === "") {
            jQuery(".pager").show();
            $.fn.yiiListView.update("users");
        } else {
            $.ajax({
                url: "'.Yii::app()->createUrl('/ajax/findUser').'",
                global: false,
                type: "POST",
                data: ({competition_id : '.$competition->id.', number: jQuery("#find-user").val()}),
                success: function (html) {
                    jQuery(".pager").hide();
                    jQuery("#users .competition-users").html(html);
                }
            });
        }
   });
', CClientScript::POS_END); ?>


<?php if(Yii::app()->user->role == 'admin'): ?>
    <?php Yii::app()->clientScript->registerScript('mix-user', '

jQuery(document).on("click", "#add-winner", function() {
         $.ajax({
             url: "'.Yii::app()->createUrl('/ajax/mixUsers').'",
             global: false,
             type: "POST",
             data: ({presents_count: '.$competition->presents_count.', competition_id : '.$competition->id.'}),
             success: function (html) {
                 console.log("sdfsdf");
                 jQuery(".competition-users .comp-user-board-item").hide();
                 jQuery(".bb-user-preloader").show();
                 jQuery(".pager, .comp-user-board__title, .comp-user-board__search").hide();

                 setTimeout(function() {
                     jQuery(".bb-user-preloader").hide();
                     jQuery("#users .competition-users").html(html);
                 }, 1500);
             }
         });
    });

    jQuery(document).on("click", ".add-winner-to-competition", function(e){
        var userId = jQuery(this).data(userId);
        var number = jQuery(this).data(number);
        var parentDiv = jQuery(this).parent();
        $.ajax({
            url: "'.Yii::app()->createUrl('/ajax/addWinner').'",
            type: "POST",
            data: ({userId: userId.userid, competition_id : '.$competition->id.', number: number.number}),
            success: function (html) {
                console.log("sdfsdf");
                parentDiv.addClass("animated fadeOutUp");
                setTimeout(function(){
                    jQuery("#comp-winners-tab").prepend(html);
                    parentDiv.remove();
                }, 1000);
            }
        });
       });

    ', CClientScript::POS_END); ?>
<?php endif; ?>
