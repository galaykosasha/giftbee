<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 10.07.2015
 * Time: 11:43
 * @var $data UserCompetitions
 */
?>
<div itemscope itemtype="http://schema.org/Person" class="comp-user-board-item">
    <a href="<?= $data->social_id == 1 ? 'http://vk.com/id' : 'https://www.facebook.com/'; ?><?= $data->user_social_id; ?>" target="_blank"><img itemprop="image" src="<?= $data->user->image; ?>" alt="<?= $data->user->first_name.' '.$data->user->last_name; ?>">
        <p itemprop="name" class="comp-user-name"><?= $data->user->first_name.'<br>'.$data->user->last_name; ?></p>
        <b>#<?= $data->number; ?></b>
        <i class="fa <?= $data->social_id == 1 ? 'fa-vk soc-icon soc-vkontakte' : 'fa-facebook soc-icon soc-facebook' ; ?>"></i>
    </a>
    <span class="add-winner-to-competition" data-userId="<?= $data->user->id; ?>" data-number="<?= $data->number; ?>"><span class="fa fa-gift"></span></span>

    <?php if(Yii::app()->user->role == 'admin'): ?>
        <div class="date-for-admin"><?= date('d-m-Y H:i', strtotime($data->participation_date)); ?></div>
    <?php endif; ?>
</div>