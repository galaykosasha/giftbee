<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 08.07.2015
 * Time: 11:37
 * @var $this SiteController
 * @var $competition Competitions
 * @var $users UserCompetitions
 */


$this->pageTitle = $competition->company->name_ru.' - '.$competition->title_ru;
Yii::app()->clientScript->registerMetaTag(Yii::app()->baseUrl.'/uploads/competitions/md-images/'.$competition->md_image, 'og:image');
Yii::app()->clientScript->registerMetaTag($this->pageTitle, 'og:title');
Yii::app()->clientScript->registerMetaTag('website', 'og:type');
Yii::app()->clientScript->registerMetaTag(strip_tags($competition->description_ru), 'og:description');
?>

<!-- COMPETITION CONTAINER-->
<div class="container-fluid competition">

    <!-- COMP ROW -->
    <div class="row">
        <!-- COMP LOGO -->
        <div class="col-md-12 top_bar-logo">
            <a href="<?= Yii::app()->createUrl('/site/index'); ?>">
                <img src="<?= Yii::app()->baseUrl.'/images/logo-top_bar.png'; ?>" alt="">
            </a>
        </div>
        <!-- END COMP LOGO -->



        <!-- COMP-CONTENT -->
        <div class="col-md-12 comp-content-wrp">
            <!-- BREDACRUMBS -->
            <!-- <a href="http://vk.com/share.php?url=https://vk.com/wall44223049_1206" target="_blank">Поделиться ВКонтакте</a> -->
            <ol class="comp-content breadcrumbs breadcrumbs--comp" vocab="http://schema.org/" typeof="BreadcrumbList">
                <li class="breadcrumbs__item" property="itemListElement" typeof="ListItem">
                    <a class="breadcrumbs__link" property="item" typeof="WebPage" href="<?= Yii::app()->createUrl('/site/index'); ?>"><span property="name">Главная</span></a><span class="breadcrumbs__devider"><i class="fa fa-arrow-circle-left"></i></span>
                    <meta property="position" content="1">
                </li>
                <li class="breadcrumbs__item" property="itemListElement" typeof="ListItem">
                    <a class="breadcrumbs__link" property="item" typeof="WebPage" href="<?= Yii::app()->createUrl('/site/competitions'); ?>"><span property="name">Конкурсы</span></a><span class="breadcrumbs__devider"><i class="fa fa-arrow-circle-left"></i></span>
                    <meta property="position" content="2">
                </li>
                <li class="breadcrumbs__item breadcrumbs__item--current" property="itemListElement" typeof="ListItem">
                    <span property="name" class="breadcrumbs__last-item"><?= $competition->title_ru; ?></span>
                    <meta property="position" content="3">
                </li>
            </ol>
            <!-- END_BREDACRUMBS -->
            <script>
                stringConcat("breadcrumbs__last-item", 70);
            </script>
            <div class="comp-content gb-clearfix">
                <!-- COMP-HEADER -->
                <div class="comp-header">
                    <i class="item-star <?= $competition->userPlay ? '' : 'hidden'; ?>"></i>
                    <div class="comp-logo">
                        <?= CHtml::image(Yii::app()->baseUrl.'/uploads/companies/logos/'.$competition->company->image, $competition->company->name_ru); ?>
                        <?/*= CHtml::link(CHtml::image(Yii::app()->baseUrl.'/uploads/companies/logos/'.$competition->company->image, $competition->company->name_ru), array('/site/company', 'url'=>$competition->company->url)); */?>
                    </div>
                    <?= CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/md-images/'.$competition->md_image, $competition->company->name_ru, array('class'=>'comp-banner')); ?>
                    
                    <?php if(!empty($competition->images)): ?>
                        <a href="" class="comp-header__btn btn" data-toggle="modal" data-target=".modal-gallery"><i class="fa fa-camera"></i> <span>+ <?= sizeof($competition->images); ?></span></a>
                    <?php endif; ?>
                </div>
                <!-- END COMP-HEADER -->

                <!-- COMP-INFO -->
                <div class="comp-info gb-clearfix">
                    <div class="col-md-12 p0">

                        <h1 class="comp-title"><?= $competition->title_ru; ?></h1>
                        <h3 class="comp-company-name">
                            <b><?= $competition->company->name_ru; ?></b>
                            <a href="<?= $competition->site_url; ?>" class="comp-outlink" target="_blank"><i class="fa fa-external-link"></i></a>
                        </h3>
                        <div class="comp-description clearfix">
                            <?= $competition->description_ru; ?>
                        </div>

                    </div>

                    <div class="col-xs-7 col-sm-6 col-md-6 p0">
                        <?php if($competition->max_users_count): ?>

                            <div class="bb-comp-user-stat">
                            <div class="comp-informer-wrp">
                                <div class="comp-informer">
                                    <a href=""><i class="fa fa-user"></i></a>
                                    <div>
                                        <div class="comp-informer-num">
                                            <?= (int)$competition->usersCount; ?>
                                            <div class="timer-info ">участвуют</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="comp-informer">
                                    <a href=""><i class="fa fa-user-plus"></i></a>
                                    <div>
                                        <div class="comp-informer-num com-informer-num--center">
                                            <?= (int)$competition->max_users_count - (int)$competition->usersCount; ?>
                                            <div class="timer-info ">осталось</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="comp-informer">
                                    <a href=""><i class="fa fa-users"></i></a>
                                    <div>
                                        <div class="comp-informer-num">
                                            <?= (int)$competition->max_users_count; ?>
                                            <div class="timer-info ">необходимо</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            <div class="clearfix"></div>
                            <div class="bb-progresbar">
                                <div class="bb-sidebar__count-line" style="width: <?= (int)$competition->usersCount * 100 / $competition->max_users_count; ?>%;"></div>
                            </div>
                            </div>
                        <?php else: ?>
                            <!-- TIMER -->
                            <div class="comp-timer-wrp">
                                <h4 class="comp-h4"><i class="fa fa-clock-o"></i> Конкурс заканчивается через:</h4>
                                <div class="comp-timer" data-seconds-left="<?= strtotime($competition->date_end) - time(); ?>">
                                    <div class="days"><?= $competition->days; ?></div>
                                </div>
                            </div>
                            <!-- END TIMER -->
                        <?php endif; ?>
                    </div>


                    <!-- USERS/GIFTS COUNT -->
                    <div class="col-xs-5 col-sm-6 col-md-6 p0">
                        <div class="comp-informer-wrp">
                            <?php if(!$competition->max_users_count): ?>
                                <div class="comp-informer">
                                    <a href=""><i class="fa fa-user"></i></a>
                                    <div class="comp-informer-border">
                                        <div class="comp-informer-num">
                                            <?= (int)$competition->usersCount; ?>
                                            <div class="timer-info ">участников</div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="comp-informer">
                                <a href=""><i class="fa fa-gift"></i></a>
                                <div >
                                    <div class="comp-informer-num">
                                        <?= $competition->presents_count; ?>
                                        <div class="timer-info ">призов</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END USERS/GIFTS COUNT -->

                    <?php /*if(!Yii::app()->user->isGuest): ?>
                        <div class="clearfix">
                            <div class="col-md-12 text-center comp-helper-title p0">
                                <h3>Для того чтобы стать участником конкурса выполните несколько простых условий:</h3>
                            </div>
                        </div>
                    <?php endif; */?>
                </div>
                <!-- END COMP-INFO -->

                <!-- USER IS GUEST -->
                <?php if(Yii::app()->user->isGuest): ?>
                    <div class="comp-message-conainer">
                        <div class="login-group_button">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Для участия в конкурсе необходима авторизация</span></h2>
                                    <p>Выберите одну из социальных сетей:</p>
                                    <?php if(Yii::app()->session['social_id'] != 2): ?>
                                        <?= CHtml::link('<i class="pull-left text-center fa fa-facebook"></i><span>Facebook</span>', array('/site/loginFb', 'url'=>true), array('class'=>'login-item btn btn-group fb-color')); ?>
                                    <?php endif; ?>
                                    <?php if(Yii::app()->session['social_id'] != 1): ?>
                                        <?= CHtml::link('<i class="pull-left text-center fa fa-vk"></i><span>Вконтакте</span>', array('/site/loginVk', 'url'=>true), array('class'=>'login-item btn btn-group vk-color')); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                <!-- END USER IS GUEST -->


                <!-- TABS -->
                <div class="comp-tabs">

                <!-- TAB INSERTS -->
                    <ul class="nav nav-tabs text-center" role="tablist">
                        <?php if($competition->group_vk || $competition->competition_url): ?>
                            <li role="presentation" class="<?= Users::isVkUser() ? 'active' : ''; ?> comp-vk"><a href="#comp-vkontakte" aria-controls="comp-vkontakte" role="tab" data-toggle="tab"><i class="fa fa-check <?= !$competition->vkUserIsParticipant ? 'hidden' : ''; ?>"></i> Вконтакте</a></li>
                        <?php endif; ?>
                        <?php if($competition->group_fb || $competition->competition_url): ?>
                            <li role="presentation" class="<?= (!$competition->group_vk && !$competition->competition_url) || !Users::isVkUser() ? 'active' : ''; ?> comp-fb"><a href="#comp-facebook" aria-controls="comp-facebook" role="tab" data-toggle="tab"><i class="fa fa-check <?= !$competition->fbUserIsParticipant ? 'hidden' : ''; ?>"></i> Facebook</a></li>
                        <?php endif; ?>
                    </ul>
                <!-- END TAB INSERTS -->


                    <div class="tab-content gb-clearfix">
                        <!-- TAB-VK -->
                        <?php if($competition->group_vk || $competition->competition_url): ?>
                            <div role="tabpanel" class="tab-pane <?= Users::isVkUser() ? 'active' : ''; ?> clearfix" id="comp-vkontakte">
                                <?php if(Users::isVkUser()): ?>

                                            <?php if($competition->vkUserIsParticipant): ?>
                                                <!-- COMP-PARTICIPANT -->
                                                <div class="clearfix"></div>
                                                <div class="comp-partcipant gb-clearfix text-center">
                                                    <?php $this->renderPartial('_users', array('data'=>UserCompetitions::model()->find('competition_id = :c_id AND social_id = 1 AND user_id = :u_id', array(
                                                        ':u_id'=>Yii::app()->user->id,
                                                        ':c_id'=>$competition->id,
                                                    )))); ?>
                                                    <h3>Спасибо за участие. <br>Желаем удачи!</h3>
                                                </div>
                                                <!-- END COMP-PARTICIPANT -->
                                            <?php else: ?>
                                        <!-- COMP-STEP1 -->
                                        <div class="col-md-12 comp-user-step-wrp">
                                            <div class="comp-user-step">
                                                <div class="step-progres">
                                                    <div class="sptep-progres__container">
                                                        <div class="sptep-progres__1--active sptep-progres__1 step-progres__steps">1</div>
                                                        <div class="sptep-progres__2 step-progres__steps">2</div>
                                                        <div class="sptep-progres__3 step-progres__steps">3</div>
                                                    </div>
                                                </div>
                                                <?php if($competition->competition_url): ?>
                                                    <h3 class="comp-step-titleh3">Поделитесь сайтом спонсора</h3>
                                                    <!-- <h4 class="comp-step-titleh4 comp-step-next">Я уже поделился</h4> -->

                                                    <div class="share-btn" data-url="<?= $competition->competition_url; ?>">
                                                        <a class="btn btn-green comp-finish-btn--site" data-id="vk">Поделиться</a>
                                                    </div>
                                                    <div class="message-for-sharing">Виджет «поделиться» может открываться в новом окне.<br>
Вернитесь  обратно чтоб закончить участие в конкурсе</div>
                                                <?php else: ?>
                                                    <h3 class="comp-step-titleh3">Подпишитесь</h3>
                                                    
                                                    <div class="comp-widget col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                                                        <!-- WIDGET SPONSOR GROUP VK -->
                                                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                                                        <div id="vk_groups"></div>
                                                        <script type="text/javascript">
                                                            VK.Widgets.Group("vk_groups", {mode: 1, width: "auto", height: "150", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, <?= $competition->group_vk; ?>);
                                                        </script>
                                                    </div>
                                                    <h4 class="comp-step-titleh4 comp-step-next">Я уже подписан <i class="fa fa-arrow-right"></i></h4>
                                                <?php endif; ?>
                                                <!-- END WIDGET SPONSOR GROUP VK-->
                                                <!--                                             <div class="col-md-12 comp-step-container">
                                                                                                <span class="comp-step-next">Вперед <i class="fa fa-arrow-right"></i></span>
                                                                                            </div> -->
                                            </div>
                                        </div>
                                        <!-- COMP-STEP2 -->
                                        <div class="col-md-12 comp-user-step-wrp hidden">
                                            <div class="comp-user-step">
                                                <div class="step-progres">
                                                    <div class="sptep-progres__container">
                                                        <div class="sptep-progres__1--check sptep-progres__1 step-progres__steps">1</div>
                                                        <div class="sptep-progres__1--active sptep-progres__2 step-progres__steps">2</div>
                                                        <div class="sptep-progres__3 step-progres__steps">3</div>
                                                    </div>
                                                </div>
                                                <h3 class="comp-step-titleh3">Поделитесь страницей конкурса</h3>
                                                <!-- <h4 class="comp-step-titleh4 comp-step-next hidden">Я уже поделился <i class="fa fa-arrow-right"></i></h4> -->
                                                <div class="comp-widget">
                                                    <?php if(empty($competition->post_url_vk)): ?>
                                                        <div class="share-btn" data-url="<?= Yii::app()->createAbsoluteUrl('/site/competition', array('company'=>$competition->company->url, 'competition_url'=>$competition->url)); ?>" data-title="<?= $this->pageTitle; ?>" data-desc="<?= strip_tags($this->getShortDescription($competition->description_ru, 100)); ?>">
                                                            <a class="btn btn-green comp-finish-btn" data-id="vk">Поделиться</a>
                                                        </div>
                                                    <?php else: ?>

                                                        <div class="share-btn" data-url="<?= $competition->post_url_vk; ?>">
                                                            <a class="btn btn-green comp-finish-btn" data-id="vk">Поделиться</a>
                                                        </div>
                                                    <?php endif; ?>
                                                    <div class="message-for-sharing">Виджет «поделиться» может открываться в новом окне. Вернитесь обратно чтоб закончить участие в конкурсе</div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- COMP-STEP3 -->
                                        <div class="col-md-12 comp-user-step-wrp hidden">
                                            <div class="comp-user-step">
                                                <div class="step-progres">
                                                    <div class="sptep-progres__container">
                                                        <div class="sptep-progres__1--check sptep-progres__1 step-progres__steps">1</div>
                                                        <div class="sptep-progres__1--check sptep-progres__2 step-progres__steps">2</div>
                                                        <div class="sptep-progres__1--active sptep-progres__3 step-progres__steps">3</div>
                                                    </div>
                                                </div>
                                                <h3 class="comp-step-titleh3">Я выполнил условия</h3>
                                                
                                                <div class="comp-widget">
                                                    <!-- FINISH BTN VK -->
                                                    <?= CHtml::ajaxLink('Стать участником','',
                                                        array(
                                                            'method' => 'POST',
                                                            'data'=>array('soc_id'=>1),
                                                            'success'=>'js:function(){
                                                                location.reload();
                                                            }'
                                                        ),
                                                        array('class'=>'comp-finish-btn btn-green btn')); ?>
                                                    <!-- END FINISH BTN VK -->
                                                </div>
                                                <h4 class="comp-step-titleh4 comp-step-start"><i class="fa fa-arrow-left"></i> Проверить выполнение</h4> <!-- Повернути на перший крок -->
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                <?php else: ?>
                                    <div class="comp-account-out">
                                        <p>Вы можете увеличить свои шансы получив дополнительный номер участника</p>
                                        <span>Хотите добавить аккаунт <b>Вконтакте?</b></span>
                                        <a href="<?= Yii::app()->createUrl('/site/loginVk', array('url'=>123)); ?>" class="btn btn-green">Добавить</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                        <?php if($competition->group_fb || $competition->competition_url): ?>
                            <!-- TAB FACEBOOK -->
                            <div role="tabpanel" class="text-center tab-pane clearfix <?= (!$competition->group_vk && !$competition->competition_url) || !Users::isVkUser() ? 'active' : ''; ?>" id="comp-facebook">
                                <?php if(Users::isFbUser()): ?>
                                            <?php if($competition->fbUserIsParticipant): ?>
                                                <!-- COMP-PARTICIPANT -->
                                                <div class="clearfix"></div>
                                                <div class="comp-partcipant gb-clearfix text-center">
                                                    <?php $this->renderPartial('_users', array('data'=>UserCompetitions::model()->find('competition_id = :c_id AND social_id = 2 AND user_id = :u_id', array(
                                                        ':u_id'=>Yii::app()->user->id,
                                                        ':c_id'=>$competition->id,
                                                    )))); ?>
                                                    <h3>Спасибо за участие. <br>Желаем удачи!</h3>
                                                </div>
                                                <!-- END COMP-PARTICIPANT -->
                                            <?php else: ?>
                                                <!-- COMP-STEP1 -->
                                                <div class="col-md-12 comp-user-step-wrp">
                                                    <div class="comp-user-step">
                                                        <div class="step-progres">
                                                            <div class="sptep-progres__container">
                                                                <div class="sptep-progres__1--active sptep-progres__1 step-progres__steps">1</div>
                                                                <div class="sptep-progres__2 step-progres__steps">2</div>
                                                                <div class="sptep-progres__3 step-progres__steps">3</div>
                                                            </div>
                                                        </div>
                                                        <?php if($competition->competition_url): ?>
                                                            <h3 class="comp-step-titleh3">Поделитесь сайтом спонсора</h3>
                                                            <h4 class="comp-step-titleh4 comp-step-next">Я уже поделился</h4>
                                                            <div class="share-btn" data-url="<?= $competition->competition_url; ?>">
                                                                <a class="btn btn-green comp-finish-btn--site" data-id="fb">Поделиться</a>
                                                            </div>
                                                            <div class="message-for-sharing">Виджет «поделиться» может открываться в новом окне. 
<br>Вернитесь  обратно чтоб закончить участие в конкурсе</div>
                                                        <?php else: ?>
                                                            <h3 class="comp-step-titleh3">Подпишитесь</h3>
                                                            
                                                            <div class="comp-widget col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                                                                <!-- WIDGET SPONSOR GROUP FB -->
                                                                <div class="fb-page" data-href="https://www.facebook.com/<?= $competition->group_fb; ?>" data-small-header="true"
                                                                     data-adapt-container-width="true" data-hide-cover="true"
                                                                     data-show-facepile="true" data-show-posts="false" data-width="250">
                                                                    <div class="fb-xfbml-parse-ignore">
                                                                        <blockquote cite="https://www.facebook.com/<?= $competition->group_fb; ?>"><a href="https://www.facebook.com/<?= $competition->group_fb; ?>"></a></blockquote>
                                                                    </div>
                                                                </div>
                                                                <!-- WIDGET SPONSOR GROUP FB -->
                                                            </div>
                                                            <h4 class="comp-step-titleh4 comp-step-next">Мне уже нравится <i class="fa fa-arrow-right"></i></h4>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <!-- COMP-STEP2 -->
                                                <div class="col-md-12 comp-user-step-wrp hidden">
                                                    <div class="comp-user-step">
                                                        <div class="step-progres">
                                                            <div class="sptep-progres__container">
                                                                <div class="sptep-progres__1--check sptep-progres__1 step-progres__steps">1</div>
                                                                <div class="sptep-progres__1--active sptep-progres__2 step-progres__steps">2</div>
                                                                <div class="sptep-progres__3 step-progres__steps">3</div>
                                                            </div>
                                                        </div>
                                                        <h3 class="comp-step-titleh3">Поделитесь страницей конкурса</h3>
                                                        <!-- <h4 class="comp-step-titleh4 comp-step-next hidden">Я уже поделился <i class="fa fa-arrow-right"></i></h4> -->
                                                        <div class="comp-widget">
                                                            <?php if(empty($competition->post_url_fb)): ?>
                                                                <div class="share-btn" data-url="<?= Yii::app()->createAbsoluteUrl('/site/competition', array('company'=>$competition->company->url, 'competition_url'=>$competition->url)); ?>" data-title="<?= $this->pageTitle; ?>" data-desc="<?= strip_tags($this->getShortDescription($competition->description_ru, 100)); ?>">
                                                                    <a class="btn btn-green comp-finish-btn" data-id="fb">Поделиться</a>
                                                                </div>
                                                            <?php else: ?>
                                                                <div class="share-btn" data-url="<?= $competition->post_url_fb; ?>">
                                                                    <a class="btn btn-green comp-finish-btn" data-id="fb">Поделиться</a>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="message-for-sharing">Виджет «поделиться» может открываться в новом окне. Вернитесь обратно чтоб закончить участие в конкурсе</div>
                                                    </div>
                                                </div>

                                        <!-- COMP-STEP3 -->
                                        <div class="col-md-12 comp-user-step-wrp hidden">
                                            <div class="comp-user-step">
                                                <div class="step-progres">
                                                    <div class="sptep-progres__container">
                                                        <div class="sptep-progres__1--check sptep-progres__1 step-progres__steps">1</div>
                                                        <div class="sptep-progres__1--check sptep-progres__2 step-progres__steps">2</div>
                                                        <div class="sptep-progres__1--active sptep-progres__3 step-progres__steps">3</div>
                                                    </div>
                                                </div>

                                                <h3 class="comp-step-titleh3">Я выполнил условия</h3>
                                                
                                                <div class="comp-widget">
                                                    <!-- widget here -->
                                                    <!-- FINISH BTN FB -->
                                                    <?= CHtml::ajaxLink('Стать участником','',
                                                        array(
                                                            'method' => 'POST',
                                                            'data'=>array('soc_id'=>2),
                                                            'success'=>'js:function(){
                                                                location.reload();;
                                                            }'
                                                        ),
                                                        array('class'=>'comp-finish-btn btn-green btn')); ?>
                                                    <!-- END FINISH BTN FB -->
                                                </div>
                                                <h4 class="comp-step-titleh4 comp-step-start"><i class="fa fa-arrow-left"></i> Проверить выполнение</h4> <!-- Повернути на перший крок -->
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php else: ?>


                                    <div class="comp-account-out">
                                        <p>Вы можете увеличить свои шансы получив дополнительный номер участника</p>
                                        <span>Хотите добавить аккаунт <b>Facebook?</b></span>
                                        <a href="<?= Yii::app()->createUrl('/site/loginFb', array('url'=>123)); ?>" class="btn">Добавить</a>
                                    </div>

                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>
                <!-- END TABS -->
                <?php endif; ?>


                <!-- USER-BOARD -->
                <div class="comp-user-board clearfix">
                <!-- ======= ADSENSE BANNER ======= -->
                <div class="adseanse-banner-1">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- adseanse-banner-1 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-8343973588647668"
                         data-ad-slot="5811928036"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <!-- ======= END ADSENSE BANNER ======= -->

                    <h2>Все участники конкурса:</h2>
                    <?php if($users->itemCount > 0): ?>
                        <div class="form-group">
                            <div class="input-group comp-find-user">
                                <?= CHtml::textField('find_number', '', array('id'=>'find-user', 'placeholder'=>'Поиск по номеру участника')); ?>
                                <div class="input-group-addon"><i class="fa fa-search"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$users,
                        'ajaxUpdate'=>true,
                        'itemView'=>'_users',
                        'template'=>'{items}<div class="clearfix"></div>{pager}',
                        'cssFile'=>false,
                        'viewData'=>array('count'=>$users->totalItemCount),
                        'pager'=>array(
                            'maxButtonCount' => 5,
                            'lastPageLabel'=> false,
                            'nextPageLabel'=>'>',
                            'prevPageLabel'=>'<',
                            'firstPageLabel'=> false,
                            'class'=>'CLinkPager',
                            'header'=>false,
                            'cssFile'=>null, // устанавливаем свой .css файл
                            'htmlOptions'=>array('class'=>'sad'),
                        ),
                        'pagerCssClass'=>'pager',
                        'sortableAttributes'=>array(
                            'rating',
                            'create_time',
                        ),
                        'emptyText'=>'Стань первым',
                        'itemsCssClass'=>'competition-users',
                        'htmlOptions'=>array('id'=>'users', 'class'=>'col-md-12')
                    ));
                    ?>
<!--                 <div class="gift-mistify">
                    <button class="btn btn-yellow btn-mistify">Определить победителя</button>
                </div> -->
                </div>
                <!-- END USER-BOARD -->

                <!-- ======== FACEBOOK COMMENTS PLUGIN ======= -->
<!--                 <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

                <div class="fb-comments" data-href="http://dev.giftbee.com.ua/giftbee/Macbook-pro-retina" data-width="100%" data-numposts="5"></div> -->
                <!-- ======== END FACEBOOK COMMENTS PLUGIN ======= -->
            </div>

        </div>
        <!-- END COMP-CONTENT -->
        


        <!-- SIDEBAR -->
        <div class="col-lg-3 comp-side-overlay">
            <div class="comp-side col-md-12 clearfix">
                <?php $this->widget('application.widgets.RelatedCompetitionsWidget', array('excludedIds'=>$competition->id)); ?>
                <div class="row comp-side__row side-soc-widget">
                    <!-- FACEBOOK -->
                    <div class="col-sm-6 col-md-4 col-lg-12 col-sm-offset-0 col-md-offset-2 col-lg-offset-0 widget_body">
                    <h2>Подпишитесь на наши страницы:</h2>
                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-page" data-href="https://www.facebook.com/giftbeeua" data-small-header="true" data-adapt-container-width="true" data-width="500" data-height="200" data-hide-cover="true" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/giftbeeua"><a href="https://www.facebook.com/giftbeeua">giftbee.ua</a></blockquote></div></div>
                    </div>
                    <!-- VKONTAKTE -->
                    <div class="col-sm-6 col-md-4 col-lg-12 widget_body">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                        <!-- VK Widget -->
                        <div id="vk_groups1"></div>
                        <script type="text/javascript">
                        VK.Widgets.Group("vk_groups1", {mode: 0, width: "auto", height: "200", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 96481626);
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- COMP ROW -->
    
</div>
<!-- END COMPETITION CONTAINER-->

<?php if(!empty($competition->images)): ?>
<!-- MODAL GALLERY -->
<div class="modal modal-gallery" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg modal-full-screen">
            <div class="modal-content text-center">
                <div class="comp-gallery">
                    <div class="slider-nav hidden-xs">
                        <?php foreach($competition->images as $images): ?>
                            <div><img class="slider-nav__img" src="<?= Yii::app()->baseUrl; ?>/uploads/competitions/extra/<?= $images->image; ?>" alt=""></div>
                        <?php endforeach; ?>
                    </div>
                    <div class="slider-for">
                        <?php foreach($competition->images as $images): ?>
                            <div><img class="slider-for__img" src="<?= Yii::app()->baseUrl; ?>/uploads/competitions/extra/<?= $images->image; ?>" alt=""></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <i class="fa fa-times" data-dismiss="modal"></i>
            </div>
    </div>
</div>
<?php endif; ?>
<!-- <script type="text/javascript" src="//vk.com/js/api/openapi.js?113"></script> -->
<!-- END MODAL GALLERY -->
<!-- COMP-PAGE-SCRIPT-->
<?php if($needRemind): ?>
<?php Yii::app()->clientScript->registerScript('remind', '
jQuery(function() {
    // MODAL fb-vk
    jQuery(".modal-fb-vk").modal("show");
});
', CClientScript::POS_END); ?>
<?php endif; ?>

<?php Yii::app()->clientScript->registerScript('comp-countdown', '
    jQuery(function(){
    
    // STEPS
    $(".comp-finish-btn").on("click", function() {
        $(this).parents(":eq(3)").addClass("hidden");
        $(this).parents(":eq(3)").next(".comp-user-step-wrp").removeClass("hidden");
        $(".comp-step-next").removeClass("hidden");
    })

    $(".comp-finish-btn--site").on("click", function() {
        $(this).parents(":eq(1)").addClass("hidden");
        $(this).parents(":eq(2)").next(".comp-user-step-wrp").removeClass("hidden");
        $(".comp-step-next").removeClass("hidden");
    })

   $(".comp-step-next").on("click", function() {
        $(this).parents(":eq(1)").addClass("hidden");
        $(this).parents(":eq(1)").next(".comp-user-step-wrp").removeClass("hidden");
     });

    $(".comp-step-start").on("click", function() {
        location.reload();
    });

    $(".comp-partcipant").prev().prev().remove();

    // SHARING BTN
     cubeShare.init();

    // COMPETITION WIDGET FACEBOOK API
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, "script", "facebook-jssdk"));
    
    // COMPETITION-TIMER
          jQuery(".comp-timer").startTimer({
            onComplete: function(element){
                element.addClass("is-complete");
            }
    });

// SLICK GALLERY
$(".modal-gallery").on("shown.bs.modal", function (e) {
    $(".slider-for").slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: false,
     fade: false,
     asNavFor: ".slider-nav",
     draggable: true,
     touchMove: true,
     adaptiveHeight: true,
   });

   $(".slider-nav").slick({
     slidesToShow: 5,
     slidesToScroll: 1,
     asNavFor: ".slider-for",
     dots: false,
     centerMode: false,
     focusOnSelect: true,
     draggable: true,
     touchMove: true,
     adaptiveHeight: false,
     variableWidth: true
   });
   
   // $(".comp-gallery").addClass("zoomIn").css("opacity", "1");
});
    
    // COMPETITION-TIMER days/hours/minutes/seconds
    jQuery(".days").append("<div class=\"timer-info\">дней</div>");
    
    });
    ', CClientScript::POS_END) ?>
<?php Yii::app()->clientScript->registerScript('find-user', '
    jQuery("#find-user").on("keyup", function(){
         if(jQuery("#find-user").val() === "") {
             jQuery(".pager").show();
             $.fn.yiiListView.update("users");
         } else {
             $.ajax({
                 url: "'.Yii::app()->createUrl('/ajax/findUser').'",
                 global: false,
                 type: "POST",
                 data: ({competition_id : '.$competition->id.', number: jQuery("#find-user").val()}),
                 success: function (html) {
                     jQuery(".pager").hide();
                     jQuery("#users .competition-users").html(html);
                 }
             });
         }
    });
    ', CClientScript::POS_END); ?>