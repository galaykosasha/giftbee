<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 30.07.2015
 * Time: 15:02
 */
?>
    <!-- GRID GIFTS -->
    <div class="container-fluid pages-top-mrg">
    <div class="row">        
    <!-- COMP LOGO -->
        <div class="col-md-12 top_bar-logo">
          <a href="/" ><img src="<?= Yii::app()->createUrl('/images'); ?>/logo-top_bar.png" alt=""></a>
        </div>
        <!-- END COMP LOGO -->
        <!-- BREDACRUMBS -->
        <ol class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">
            <li class="breadcrumbs__item" property="itemListElement" typeof="ListItem">
                <a class="breadcrumbs__link" property="item" typeof="WebPage" href="<?= Yii::app()->createUrl('/site/index'); ?>"><span property="name">Главная</span></a><span class="breadcrumbs__devider"><i class="fa fa-arrow-circle-left"></i></span>
                <meta property="position" content="1">
            </li>
            <li class="breadcrumbs__item breadcrumbs__item--current" property="itemListElement" typeof="ListItem">
                <span property="name">Архив</span>
                <meta property="position" content="2">
            </li>
        </ol>
        <!-- END_BREDACRUMBS -->

        <h1 class="mgl10">Архив конкурсов</h1>
        <div id="giftbee-start" class="gb-grid archive">
            <?php if(empty($competitions)): ?>
                <div class="archive-empty">
                    <p class="text-center"><i class="fa fa-star"></i> Сейчас все конкурсы активные.</p>
                </div>
            <?php endif; ?>
            <?php $this->widget('application.widgets.ItemsWidget', array('model'=>$competitions)); ?>
        </div>
        <!-- END GRID GIFTS -->
    </div>
    </div>
    <!-- MORE -->
    <!--<div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="more-btn btn btn-green" id="more-competitions">Еще</span>
            </div>
        </div>
    </div>-->
    <!-- END MORE -->

<?php Yii::app()->clientScript->registerScript('more-competitions', '
var ua = navigator.userAgent.toLowerCase(), isOpera = (ua.indexOf("opera")  > -1), isIE = (!isOpera && ua.indexOf("msie") > -1), stop = true;

function getDocumentHeight(){ // высота всей страницы вместе с невидимой частью
    return Math.max(document.compatMode!="CSS1Compat"?document.body.scrollHeight:document.documentElement.scrollHeight,getViewportHeight());
}

function getViewportHeight(){ // высота браузера
    return ((document.compatMode||isIE)&&!isOpera)?(document.compatMode=="CSS1Compat")?document.documentElement.clientHeight:document.body.clientHeight:(document.parentWindow||document.defaultView).innerHeight;
}

jQuery(window).scroll(function (event) {
    event.preventDefault();
    var s = getDocumentHeight() - getViewportHeight();
    var ls =  s - $(this).scrollTop();


    if(ls <= 300 && stop == true) {
        stop = false;
        var itemCount = jQuery(".item").length;
        $.ajax({
             url: "'.Yii::app()->createUrl('/ajax/moreArchiveCompetitions').'",
             global: false,
             type: "POST",
             data: ({count : itemCount}),
             success: function (html) {
                jQuery(".gb-grid").append(html);
             }
         });
    }
});

', CClientScript::POS_END); ?>