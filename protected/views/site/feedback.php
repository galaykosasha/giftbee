<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 29.10.2015
 * Time: 13:54
 */
$user = Users::model()->findByPk(Yii::app()->user->id);
?>

<div class="container-fluid m-t profile">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center setting-page-dsc">
            <h1>Обратная связь</h1>
            <span>Отправьте нам сообщение если Вы нашли ошибку, <br>у Вас есть предложения или просто Вам скучно</span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
            <div class="user-info-board clearfix">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'add-form',
                    'enableAjaxValidation'=>false,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit'=>true,
                    ),
                    'htmlOptions'=>array('class'=>'form-inline form-1'),
                )); ?>

                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        <div class="input-group">
                            <!-- <div class="input-group-addon"><i class="fa fa-envelope"></i></div> -->
                            <?= $form->textField($model,'email', array('class'=>'form-control first-name', 'placeholder'=>'E-mail', 'value'=>!empty($user) ? $user->email : '')); ?>
                            <?= $form->error($model,'email', array('class'=>'danger-message')); ?>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        <div class="input-group">
                            <!-- <div class="input-group-addon"><i class="fa fa-comment"></i></div> -->
                            <?= $form->textArea($model,'comment', array('class'=>'form-control first-name bb-textarea', 'placeholder'=>'Введите текст')); ?>
                            <?= $form->error($model,'comment', array('class'=>'danger-message')); ?>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET -->

                <div class="col-md-6 col-md-offset-3">
                <div class="form-group">
                    <div class="input-group">
                        <?= CHtml::submitButton('Отправить', array('class'=>'btn btn-green')); ?>
                    </div>
                </div>
                    
                </div>
                <?php $this->endWidget(); ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
