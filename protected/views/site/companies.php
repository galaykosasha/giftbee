<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 30.10.2015
 * Time: 17:46
 * @var $this SiteController
 * @var $companies Companies
 */
?>
<div class="container-fluid pages-top-mrg">
    <div class="row">
    <!-- COMP LOGO -->
    <div class="col-md-12 top_bar-logo">
      <a href="/" ><img src="<?= Yii::app()->baseUrl.'/logo-top_bar.png'; ?>" alt=""></a>
    </div>
    <!-- END COMP LOGO -->
    <!-- BREDACRUMBS -->
    <ol class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">
        <li class="breadcrumbs__item" property="itemListElement" typeof="ListItem">
            <a class="breadcrumbs__link" property="item" typeof="WebPage" href="<?= Yii::app()->createUrl('/site/index'); ?>"><span property="name">Главная</span></a><span class="breadcrumbs__devider"><i class="fa fa-arrow-circle-left"></i></span>
            <meta property="position" content="1">
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current" property="itemListElement" typeof="ListItem">
            <span property="name">Компании</span>
            <meta property="position" content="2">
        </li>
    </ol>
    <!-- END_BREDACRUMBS -->
        <h1 class="mgl10">Компании</h1>
        <!-- GRID GIFTS -->
        <div id="giftbee-start" class="gb-grid gb-grid--companies">
            <?php foreach($companies as $company): ?>
                <div class="item">
                    <figure>
                        <i class="item-star hidden"></i>
                        <div class="item-img-wrp">
                            <a href="<?= Yii::app()->createUrl('/site/company', array('url'=>$company->url)); ?>">
                                <img class="item__img" src="<?= Yii::app()->baseUrl.'/uploads/companies/logos/'.$company->image; ?>" alt="">
                            </a>
                        </div>
                        <figcaption>
                            <h2>
                                <?= CHtml::link($company->name_ru, array('/site/company', 'url'=>$company->url)); ?>
                            </h2>
                        </figcaption>
                        <div class="clearfix"></div>
                    </figure>
                </div>
            <?php endforeach; ?>
        </div>
        <!-- END GRID GIFTS --> 
    </div>
</div>