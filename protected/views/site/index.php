<?php
/* @var $this SiteController */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name;

Yii::app()->clientScript->registerMetaTag(Yii::app()->baseUrl.'/images/content/giftbee-banner-min.jpg', 'og:image');
Yii::app()->clientScript->registerMetaTag('GiftBee – уникальная платформа для проведения акций и конкурсов.', 'og:title');
Yii::app()->clientScript->registerMetaTag('website', 'og:type');
Yii::app()->clientScript->registerMetaTag('Надеемся Вы любите получать подарки? Ведь мы любим их дарить, но еще больше любим дарить их много! Наша пчела собрала все акции в одном месте! Теперь подарки живут у нас и с нетерпением ждут своих владельцев!', 'og:description');

?>
<?php if(Yii::app()->controller->action->id == 'index'): ?>

<!-- BANNER -->
<div class="container-fluid">
    <div class="row banner flex">
      <div class="container banner-overlay">
        <figure class="col-xs-12 col-sm-7 col-md-5 text-center banner-description">
        <div class="col-md-12">
          <img src="<?= Yii::app()->request->baseUrl; ?>/images/logo-1w.png" alt="">
        </div>
            <figcaption class="col-xs-12">
                <h1>Здесь живут подарки!</h1>
                <div class="row">
                  <div class="col-xs-4 col-sm-4 col-md-4 p0">
                    <div id="user_count" class="gb-count img-circle">
                    <script>
                      ptn("user_count", <?= UserCompetitions::countMembers(); ?>, 300, 0);
                    </script>
                    </div>
                    <p class="gb-count_dsc cc1">участников</p>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4 p0">
                    <div id="winners_count" class="gb-count img-circle">
                    <script>
                      ptn("winners_count", <?= Competitions::countWinners(); ?>, 300, 0);
                    </script>
                    </div>
                    <p class="gb-count_dsc cc2">победителей</p>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4 p0">
                    <div id="gifts_count" class="gb-count img-circle">
                    <script>
                      ptn("gifts_count",  <?= Competitions::countActivePresents(); ?>, 300, 0);
                    </script>
                    </div>
                    <p class="gb-count_dsc cc3">подарков<br></p>
                  </div>
                </div>
                <a href="<?= Yii::app()->createUrl('/site/competitions'); ?>" class="btn btn-description">Начать сейчас</a>
            </figcaption>
        </figure>
      </div>
    </div>
</div>
<!-- END BANNER -->
<?php endif; ?>

<!-- GRID GIFTS -->
<div id="title-new-competitions"></div>
<h2 class="text-center">Новые конкурсы:</h2>
<div class="home-gb-grid gb-grid container">
    <?php $this->widget('application.widgets.ItemsWidget', array('model'=>$competitions)); ?>
</div>
<!-- MORE -->
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 text-center">
      <a href="<?= Yii::app()->createUrl('/site/competitions'); ?>" class="more-link">Смотреть все конкурсы ></a>
    </div>
  </div>
</div> 
<!-- END MORE -->
<!-- END GRID GIFTS -->


<div class="container-fluid text-center">
  <div class="how-it-works">
    <div class="row">
      <div class="col-md-6">
        <h2>Узнай как это работает</h2>
        <p class="col-md-8 col-md-offset-2 how-it-works-dsc">GiftBee – уникальная платформа для проведения акций и конкурсов. <b>Надеемся Вы любите получать подарки?</b></p>
        <iframe class="video-size" width="640" height="360" src="https://www.youtube.com/embed/qZt3qpox5ws?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
        <a href="<?= Yii::app()->createUrl('/site/faq'); ?>" class="btn btn-green">Подробнее</a>
      </div>
      <div class="col-md-6">
        <h2 class="m-top">Разместить конкурс</h2>
        <p class="col-md-8 col-md-offset-2 how-it-works-dsc">Вы можете разместить свой конкурс на Giftbee и привлечь лояльную аудиторию к своему продукту или услуге.</p>
        <div>
          <img src="images/add-gift.png" alt="add-gift">
        </div>
        <a href="<?= Yii::app()->createUrl('/site/addCompetition'); ?>" class="add-gift btn btn-green">Подробнее</a>
      </div>
    </div>
  </div>
</div>

<div class="giftbee-stat">
  <div class="container-fluid">
    <div class="row text-center">
      <h2 class="gb-h2">Наши группы в соцсетях</h2>
      <div class="text-center col-md-6 col-md-offset-3 home-soc-info">
        Подпишись на новости от Giftbee.ua<br>Узнавай первым о новых конкурсах и результатах розыгрышей.<br><b>Это обязательное условия для участия в конкурсах</b>
      </div>

        <div class="col-sm-5 col-md-4 col-lg-3 col-sm-offset-1 col-md-offset-2 col-lg-offset-3 bottom-mrg">
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

        <div id="vk_groups"></div>
        <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {wide: 0, mode: 0, width: "auto", height: "200", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 96481626);
        </script>
      </div>
      
      <div class="col-sm-5 col-md-4 col-lg-3 text-center bottom-mrg">
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-page" data-href="https://www.facebook.com/giftbeeua" data-width="500" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>
      </div>
    </div>
  </div>
</div>
