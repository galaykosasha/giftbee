<?php

class SiteController extends Controller
{

    public function actionRules()
    {
        $this->layout = '//layouts/pages';
        $this->render('rules');
    }

    public function actionPrivatePolicy()
    {
    	$this->layout = '//layouts/pages';
        $this->render('privatePolicy');
    }

    public function actionInstruction()
    {
        $this->layout = '//layouts/pages';
        $this->render('instruction');
    }

    public function actionFaq()
    {
        $this->layout = '//layouts/pages';
        $this->render('faq');
    }

    public function actionDeliveryForm()
    {
        $this->layout = '//layouts/pages';
        $this->render('deliveryForm');
    }

    public function actionReviews()
    {
        $this->layout = '//layouts/main';
        $this->render('reviews');
    }

    public function actionFeedback()
    {
        $this->layout = '//layouts/pages';
        $model = new Feedback();
        if(isset($_POST['Feedback'])) {
            $model->attributes = $_POST['Feedback'];
            if($model->validate()) {
                $subject='=?UTF-8?B?'.base64_encode('Обратная связь на Giftbee.ua').'?=';
                $headers="MIME-Version: 1.0\r\n"."Content-type: text/html; charset=utf8";
                $text = 'Email: '.strip_tags($model->email).'<br>';
                $text .= 'Комментарий: '.strip_tags($model->comment).'<br>';
                if(mail('giftbeecompany@gmail.com',$subject, $text, $headers)){
                    Yii::app()->user->setFlash('success', 'Ваше сообщение отправлено.');
                    $this->refresh();
                }
            }
        }
        $this->render('feedback', array('model'=>$model));
    }

    public function actionAddCompetition()
    {
        $this->layout = '//layouts/pages';
        $model = new AddCompetition();
        if(isset($_POST['AddCompetition'])) {
            $model->attributes = $_POST['AddCompetition'];
            if($model->validate()) {
                $subject='=?UTF-8?B?'.base64_encode('Заявка добавления конкурса на Giftbee.ua').'?=';
                $headers="MIME-Version: 1.0\r\n"."Content-type: text/html; charset=utf8";
                $text = 'Имя: '.strip_tags($model->name).'<br>';
                $text .= 'E-mail: '.CHtml::link(strip_tags($model->email), strip_tags($model->email)).'<br>';
                $text .= 'Ссылка FB: '.strip_tags($model->facebook_url).'<br>';
                $text .= 'Ссылка ВК: '.strip_tags($model->vk_url).'<br>';
                $text .= 'Номер телефона: '.strip_tags($model->telephone).'<br>';
                $text .= 'Комментарий: '.strip_tags($model->comments).'<br>';
                if(mail('giftbeecompany@gmail.com',$subject, $text, $headers)){
                    Yii::app()->user->setFlash('success', 'Ваша заявка успешно отправлена.');
                    $this->refresh();
                }
            }
        }
        $this->render('addCompetition', array('model'=>$model));
    }

    public function actionIndex()
    {
        $this->register();
        $criteria = new CDbCriteria();
        $criteria->order = 'date_start DESC';
        $criteria->condition = 'date_start < NOW() AND date_end > NOW()';
        $criteria->compare('is_overflow', 0);
        $criteria->compare('is_active', 1);
        $criteria->limit = 8;
        $competitions = Competitions::model()->findAll($criteria);

        $this->render('index', array('competitions'=>$competitions));
    }

    public function actionArchive()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date_start DESC';
        $criteria->condition = 'date_end < NOW()';
        $criteria->compare('is_overflow', 1, false, 'OR');
        $criteria->compare('is_active', 1);
        $criteria->limit = 25;

        $competitions = Competitions::model()->findAll($criteria);

        $this->render('archive', array('competitions'=>$competitions));
    }

    public function actionLoginVk()
    {
        if(!isset($_GET['code'])) {
            Yii::app()->session['url'] = $_SERVER['HTTP_REFERER'];
        }
        $vk = new Vk();
        if(isset($_GET['code'])) {
            $vk->getAccessToken($_GET['code']);
            if(isset(Yii::app()->session['url'])) {
                $url = Yii::app()->session['url'];
                unset(Yii::app()->session['url']);
                header("Location: $url");
            }
            $this->redirect(array('/site/index'));
        } else {
            $vk->getAccessCode();
        }
    }

    public function actionLoginFb()
    {
        if(!isset($_GET['code'])) {
            Yii::app()->session['url'] = $_SERVER['HTTP_REFERER'];
        }
        $fb = new Fb();
        if(isset($_GET['code'])) {
            $fb->getAccessToken($_GET['code']);
            if(isset(Yii::app()->session['url'])) {
                $url = Yii::app()->session['url'];
                unset(Yii::app()->session['url']);
                header("Location: $url");
            }
            $this->redirect(array('/site/index'));
        } else {
            $fb->getAccessCode();
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * @var $buffer UsersBuffer
     */
    private function register()
    {
        $model = UsersBuffer::model()->findByPk(Yii::app()->session['u_id']);
        $model->scenario = 'addToBuffer';
        if(isset($_POST['ajax'])) {
            if ($_POST['ajax']=='user-form') {
                echo CActiveForm::validate($model);
            }
            Yii::app()->end();
        }

        if(isset($_POST['UsersBuffer'])){
            if(Users::model()->exists('email = :email', array(':email'=>$_POST['UsersBuffer']['email']))) {
                unset(Yii::app()->session['u_id']);
                Yii::app()->session['social_id'] = $model->social_id;
                $model->delete();
                $this->refresh();
            } else {
                $model->attributes = $_POST['UsersBuffer'];
                $model->approve_code = uniqid();
                if($model->save()) {
                    $link = Yii::app()->createAbsoluteUrl('/site/checkCode', array('code'=>$model->approve_code));
                    $this->sendMail($link, $model->email);
                    Yii::app()->session['sending_message'] = true;
                    $this->refresh();
                }
            }
        }
    }

    private function sendMail($link, $to)
    {
        $subject='=?UTF-8?B?'.base64_encode('Подтверждение регистрации на Giftbee.com.ua').'?=';
        $headers="From: Giftbee <{hello@giftbee.com.ua}>\r\n".
            "Reply-To: hello@giftbee.com.ua\r\n".
            "MIME-Version: 1.0\r\n".
            "Content-type: text/html; charset=utf8";
        $email = new Email();
        mail($to,$subject,$email->buildEmailText($link),$headers);
    }

    private function sendMailAfterRegister($to)
    {
        $subject='=?UTF-8?B?'.base64_encode('Спасибо за регистрацию на Giftbee.com.ua').'?=';
        $headers="From: Giftbee <{hello@giftbee.com.ua}>\r\n".
            "Reply-To: hello@giftbee.com.ua\r\n".
            "MIME-Version: 1.0\r\n".
            "Content-type: text/html; charset=utf8";
        $email = new Email();
        mail($to,$subject,$email->afterRegisterMessage(),$headers);
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * Checking code from email. Redirected to index page.
     * @param $code
     */
    public function actionCheckCode($code)
    {
        $buffer = UsersBuffer::model()->find('approve_code = :code', array(':code'=>$code));
        if(isset($buffer)) {
            $user = Users::model()->find('email = :email', array(':email'=>$buffer->email)) ? Users::model()->find('email = :email', array(':email'=>$buffer->email)) : new Users('migrate');
            $user->telephone = $buffer->telephone;
            $user->email = $buffer->email;
            if($user->save()){
                $this->sendMailAfterRegister($user->email);
                $user->saveSocialAccount($buffer);
                $this->redirect('index');
            }
        } else {
            $this->redirect(array('/site/index'));
        }
    }

    /**
     * Cleared temporary data from session if user remember that he/she has an account
     */
    public function actionClearTemporaryData()
    {
        if( Yii::app()->request->isAjaxRequest && isset($_POST['u_id']) &&!empty($_POST['u_id'])) {
            UsersBuffer::model()->deleteByPk($_POST['u_id']);
            unset(Yii::app()->session['u_id']);
            unset(Yii::app()->session['sending_message']);
        }
    }

    /**
     * Competition page
     * @param $company
     * @param $competition_url
     * @throws CHttpException
     */
    public function actionCompetition($company, $competition_url)
    {
        $competition = Competitions::model()->find(array('condition'=>'url = :url', 'params'=>array(':url'=>$competition_url)));
        if(!$competition) {
            throw new CHttpException(404, 'Страница не найдена');
        }
        $users = new CActiveDataProvider('UserCompetitions',
            array(
                'criteria'=>array(
                    'condition'=>'competition_id = :id',
                    'params'=>array(':id'=>$competition->id),
                    'order'=>'id DESC',
                ),
                'sort'=>false,
                'pagination'=>array(
                    'pageSize'=>10
                ),
            )
        );

        /**
         * added user to competition
         * @TODO to ajax controller
         */
        if(isset($_POST['soc_id'])) {
            if ($competition->max_users_count > 1 && $competition->usersCount >= $competition->max_users_count) {
                echo 'Конкурс закончился!';
                Yii::app()->end;
            }
            if ($_POST['soc_id'] == 3) {
                $user = Users::model()->findByPk(Yii::app()->user->id);
                if($user->isFbUser()) {
                    $soc_id = 2;
                } else {
                    $soc_id = 1;
                }
                $user_social = UserSocials::model()->find('user_id = :u_id AND social_id = :s_id', array(':u_id' => $user->id, ':s_id' => $soc_id));
                $user_competition = UserCompetitions::model()->find('competition_id = :c_id AND user_social_id = :us_id', array(':c_id' => $competition->id, ':us_id' => $user_social->social_user_id));

            } else {
                $user_social = UserSocials::model()->find('user_id = :u_id AND social_id = :s_id', array(':u_id' => Yii::app()->user->id, ':s_id' => $_POST['soc_id']));
                $user_competition = UserCompetitions::model()->find('competition_id = :c_id AND user_social_id = :us_id', array(':c_id' => $competition->id, ':us_id' => $user_social->social_user_id));
                $soc_id = $_POST['soc_id'];
            }

            if (!isset($user_competition)) {
                $newUser = new UserCompetitions();
                $newUser->user_social_id = $user_social->social_user_id;
                $newUser->competition_id = $competition->id;
                $newUser->user_id = Yii::app()->user->id;
                $newUser->social_id = $soc_id;
                if ($newUser->save()) {
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $mail = new Email();
                    $subject = '=?UTF-8?B?' . base64_encode('Команда Giftbee.com.ua') . '?=';
                    $headers = "From: Giftbee <{hello@giftbee.com.ua}>\r\n" .
                        "Reply-To: hello@giftbee.com.ua\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/html; charset=utf8";
                    $email = new Email();
                    mail($mail, $subject, $mail->afterParticipation($user->first_name.' '.$user->last_name, $competition->title_ru), $headers);

                    if ($competition->max_users_count > 1 && $competition->usersCount + 1 >= $competition->max_users_count) {
                        $competition->is_overflow = 1;
                        $competition->save();
                    }
                }
            } else {
                echo 'Вы уже участвуете!';
            }

            Yii::app()->end();
        }

        /**
         * reminded to user that he/she not subscribed to giftbee
         */
        if(!Yii::app()->session['remind'] && Yii::app()->user->isGuest) {
            $needRemind = true;
            Yii::app()->session['remind'] = true;
        } else {
            $needRemind = false;
        }

        /**
         * choice view
         */
        if(strtotime($competition->date_end) < time()) {
            $this->render('archive_competition', array('competition'=>$competition, 'users'=>$users, 'needRemind'=>$needRemind));
        } else {
            if($competition->youtube_url)
                $this->render('youTubeCompetition', array('competition'=>$competition, 'users'=>$users, 'needRemind'=>$needRemind));
            else
                $this->render('competition', array('competition'=>$competition, 'users'=>$users, 'needRemind'=>$needRemind));
        }
    }

    /**
     * Selected competitions for current user
     */
    public function actionMyCompetitions()
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('userCompetitions');
        $criteria->distinct = true;
        $criteria->together = false;
        $criteria->join = 'INNER JOIN `gb_user_competitions` ON `t`.`id` = `gb_user_competitions`.`competition_id`';
        $criteria->condition = 'gb_user_competitions.user_id = :u_id';
        $criteria->params = array(':u_id'=>Yii::app()->user->id);
        $criteria->compare('is_active', 1);
        $criteria->compare('type', 1);
        $criteria->limit = 25;
        $competitions = Competitions::model()->findAll($criteria);

        $this->render('myCompetitions', array('competitions'=>$competitions));
    }

    /**
     * all active competitions page
     */
    public function actionCompetitions()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date_end ASC';
        $criteria->condition = 'date_start < NOW() AND date_end > NOW()';
        $criteria->compare('is_overflow', 0);
        $criteria->compare('is_active', 1);
        $criteria->limit = 100;
        $competitions = Competitions::model()->findAll($criteria);

        $this->render('competitions', array('competitions'=>$competitions));
    }

    public function actionSendMailsForNullCompetition()
    {
        $mails = Users::model()->findAll();

        $i = 0;
        foreach ($mails as $mail) {
            if($mail->competitionsCount === 0) {
                $subject = '=?UTF-8?B?' . base64_encode('Команда Giftbee.com.ua') . '?=';
                $headers = "From: Giftbee <{hello@giftbee.com.ua}>\r\n" .
                    "Reply-To: hello@giftbee.com.ua\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/html; charset=utf8";
                $email = new Email();
                mail($mail, $subject, $email->forNullCompetitionsUsers($mail->first_name.' '.$mail->last_name), $headers);
                $i++;
            }
        }
        echo $i.' All messages are send!'; exit;
    }

    public function actionSendMailsForOneCompetition()
    {
        $mails = Users::model()->findAll();
        foreach($mails as $mail) {
            if($mail->competitionsCount === 1)
            {
                $subject='=?UTF-8?B?'.base64_encode('Команда Giftbee.com.ua').'?=';
                $headers="From: Giftbee <{hello@giftbee.com.ua}>\r\n".
                    "Reply-To: hello@giftbee.com.ua\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-type: text/html; charset=utf8";
                $email = new Email();
                $competition_id = UserCompetitions::model()->find('user_id = :id', array(':id'=>$mail->id));
                $competition = Competitions::model()->findByPk($competition_id->competition_id);

                mail($mail,$subject,$email->forOneCompetitionUsers($mail->first_name.' '.$mail->last_name, $competition->title_ru),$headers);
            }
        }
        echo 'All messages are send!'; exit;
    }

    public function actionSendMailsForNewCompetition()
    {
        $mails = Users::model()->findAll();
        foreach($mails as $mail) {
            $subject='=?UTF-8?B?'.base64_encode('Команда Giftbee.com.ua').'?=';
            $headers="From: Giftbee <{hello@giftbee.com.ua}>\r\n".
                "Reply-To: hello@giftbee.com.ua\r\n".
                "MIME-Version: 1.0\r\n".
                "Content-type: text/html; charset=utf8";
            $email = new Email();

            mail($mail->email, $subject, $email->newMails($mail->first_name.' '.$mail->last_name), $headers);
        }
        echo 'All messages are send!'; exit;
    }

    public function actionCountEmailReferral($code)
    {
        $emailReferrals = ReferralsCount::model()->findByPk(1);
        $emailReferrals->saveCounters(array('count'=>1));
        $this->redirect(array('/site/checkCode', 'code'=>$code));
    }

    public function actionRegistration()
    {
        $model = new UsersBuffer();
        $model->scenario = 'addData';
        $this->render('registration', array('model'=>$model));
    }

    public function actionMySales()
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('userCompetitions');
        $criteria->distinct = true;
        $criteria->together = false;
        $criteria->join = 'INNER JOIN `gb_user_competitions` ON `t`.`id` = `gb_user_competitions`.`competition_id`';
        //$criteria->condition = 'gb_user_competitions.user_id = :u_id';
        //$criteria->params = array(':u_id'=>Yii::app()->user->id);
        $criteria->compare('gb_user_competitions.user_id', Yii::app()->user->id);
        $criteria->compare('is_active', 1);
        $criteria->addInCondition('sale_type', array(1,2), 'AND');
        $criteria->limit = 25;
        $competitions = Competitions::model()->findAll($criteria);

        $this->render('mySales', array('competitions'=>$competitions));
    }

    public function actionWinner()
    {
    	$this->redirect('competitions');
		$this->render('winners');
    }

    public function actionSendMailsForUsersInBuffer()
    {
        $mails = UsersBuffer::model()->findAll();
        foreach($mails as $mail) {
            if(!empty($mail->email)) {
                $subject='=?UTF-8?B?'.base64_encode('Команда Giftbee.com.ua').'?=';
                $headers="From: Giftbee <{hello@giftbee.com.ua}>\r\n".
                    "Reply-To: hello@giftbee.com.ua\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-type: text/html; charset=utf8";
                $email = new Email();

                mail($mail->email, $subject, $email->forUsersInBuffer(Yii::app()->createAbsoluteUrl('/site/countEmailReferral', array('code'=>$mail->approve_code))), $headers);
            }
        }
        echo 'All messages are send!'; exit;
    }

    public function actionWinners()
    {
        $this->render('allWinners');
    }

    public function actionCompanies()
    {
        $companies = Companies::model()->findAll();
        $this->render('companies', array('companies'=>$companies));
    }

    public function actionCompany($url)
    {
        $model = Companies::model()->find('url = :url', array(':url'=>$url));
        $this->render('company', array('company'=>$model));
    }

    public function actionYoutube()
    {
        $this->render('youTubeCompetition');
    }
}