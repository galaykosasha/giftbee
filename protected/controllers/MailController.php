<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 24.04.16
 * Time: 15:58
 */
class MailController extends Controller
{
    public function sendWinnerMails($competition_id)
    {
        $this->layout = null;
        $competition = Competitions::model()->find('id = :id', array(':id' => $competition_id));

        $competition->sendEmails('winners', $this->renderPartial('layoutWinner', array('competition' => $competition), true));
    }
    public function sendLoosersMails($competition_id)
    {
        $this->layout = null;
        $competition = Competitions::model()->find('id = :id', array(':id' => $competition_id));

        $competition->sendEmails('loosers', $this->renderPartial('layoutLooser', array('competition' => $competition), true));
    }

    public function actionSendTestAfterCompetitionMails($id)
    {
        $this->layout = null;
        $competition = Competitions::model()->find('id = :id', array(':id' => $id));

        $competition->sendEmails($for = 'admins', $this->renderPartial('layoutWinner', array('competition' => $competition), true));
        $competition->sendEmails($for = 'admins', $this->renderPartial('layoutLooser', array('competition' => $competition), true));
    }

    public function actionSendMailsAfterCompetition($id)
    {
        $this->sendWinnerMails($id);
        $this->sendLoosersMails($id);
    }
}