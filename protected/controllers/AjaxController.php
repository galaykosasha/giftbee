<?php
class AjaxController extends Controller
{
    protected function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if(!Yii::app()->request->isAjaxRequest)
        {
            throw new CHttpException('404', 'Page not find');
        }
        return true;
    }

    /*public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'id DESC';
        $criteria->condition = 'date_start < NOW() AND date_end > NOW()';
        $criteria->offset = $_POST['count'];
        $criteria->limit = 10;

        $model = Competitions::model()->findAll($criteria);
        $this->widget('application.widgets.ItemsWidget', array('model'=>$model));
    }*/

    public function actionMoreMyCompetitions()
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('userCompetitions');
        $criteria->distinct = true;
        $criteria->together = false;
        $criteria->join = 'INNER JOIN `gb_user_competitions` ON `t`.`id` = `gb_user_competitions`.`competition_id`';
        $criteria->condition = 'gb_user_competitions.user_id = :u_id';
        $criteria->params = array(':u_id'=>Yii::app()->user->id);
        $criteria->offset = $_POST['count'];
        $criteria->compare('is_active', 1);
        $criteria->compare('type', 1);
        $criteria->limit = 10;

        $model = Competitions::model()->findAll($criteria);
        $this->widget('application.widgets.ItemsWidget', array('model'=>$model));
    }

    public function actionMoreCompetitions()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date_start DESC';
        $criteria->condition = 'date_start < NOW() AND date_end > NOW()';
        $criteria->compare('is_overflow', 0);
        $criteria->compare('is_active', 1);
        $criteria->limit = 10;
        $criteria->offset = $_POST['count'];

        $model = Competitions::model()->findAll($criteria);
        $this->widget('application.widgets.ItemsWidget', array('model'=>$model));
    }

    public function actionMoreArchiveCompetitions()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date_start DESC';
        $criteria->condition = 'date_end < NOW()';
        $criteria->compare('is_overflow', 1, false, 'OR');
        $criteria->compare('is_active', 1);
        $criteria->offset = $_POST['count'];
        $criteria->limit = 10;

        $model = Competitions::model()->findAll($criteria);
        $this->widget('application.widgets.ItemsWidget', array('model'=>$model));
    }

    public function actionFindUser()
    {
        if(isset($_POST['competition_id']) && isset($_POST['number']))
        {
            $criteria = new CDbCriteria();
            $criteria->compare('competition_id', $_POST['competition_id']);
            $criteria->compare('number', $_POST['number']);
            $userCompetition = UserCompetitions::model()->find($criteria);
            if(!$userCompetition) {
                echo 'В этом конкурсе нет участника с номером '.CHtml::encode(strip_tags($_POST['number'])).'!';
                Yii::app()->end();
            }
            $this->renderPartial('application.views.site._users', array('data'=>$userCompetition));
        }
    }

    public function actionMixUsers()
    {
        if(isset($_POST['competition_id']) && isset($_POST['presents_count']))
        {
            $competition = Competitions::model()->findByPk($_POST['competition_id']);
            $winnersArray = explode(',', $competition->winners);

            $count = $competition->presents_count - sizeof($winnersArray);

            $criteria = new CDbCriteria();
            $criteria->compare('competition_id', $competition->id);
            $criteria->limit = $count;
            $criteria->order = 'RAND()';
            $criteria->addNotInCondition('number', $winnersArray);

            $userCompetition = UserCompetitions::model()->findAll($criteria);
            if(!$userCompetition) {
                echo 'В этом конкурсе нет участника с номером '.CHtml::encode(strip_tags($_POST['number'])).'!';
                Yii::app()->end();
            }
            foreach($userCompetition as $user) {
                $this->renderPartial('application.views.site._candidate', array('data'=>$user));
            }
        }
    }

    public function actionAddWinner()
    {
        if(isset($_POST['competition_id']) && isset($_POST['userId']))
        {
            $competition = Competitions::model()->findByPk($_POST['competition_id']);
            if(empty($competition->winners)) {
                $competition->winners = $_POST['number'];
            }
            else {
                $competition->winners .= ','.$_POST['number'];
            }
            $competition->save();

            $user = UserCompetitions::model()->find('competition_id = :c_id AND user_id = :u_id', array(':c_id'=>$competition->id, ':u_id'=>$_POST['userId']));
            $this->renderPartial('application.views.site._users', array('data'=>$user));
        }
    }
}