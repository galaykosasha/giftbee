<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 03.11.2015
 * Time: 14:38
 */
class CompaniesSliderWidget extends CWidget
{
    public $model;
    public function init()
    {
        $this->model = Companies::model()->findAll();
    }
    public function run()
    {
        $this->render('companiesSlider', array('model'=>$this->model));
    }
}