<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 29.07.2015
 * Time: 12:17
 * @var $winners string
 * @var $competition_id int
 */
class WinnersWidget extends CWidget
{
    public $winners;
    public $competition_id;
    public $video;

    protected $model;

    public function init()
    {
        $winnersArray = explode(',', trim($this->winners, ','));
        $criteria = new CDbCriteria();
        $criteria->compare('competition_id', $this->competition_id);
        $criteria->addInCondition('number', $winnersArray);

        $this->model = UserCompetitions::model()->findAll($criteria);
    }
    public function run()
    {
        $this->render('winners', array('model'=>$this->model, 'video'=>$this->video));
    }
}