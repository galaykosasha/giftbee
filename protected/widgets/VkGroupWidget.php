<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 13.07.2015
 * Time: 14:38
 */
class VkGroupWidget extends CWidget
{
    public $group_id;

    public function init()
    {
        Yii::app()->clientScript->registerScriptFile('https://vk.com/js/api/openapi.js?116',CClientScript::POS_HEAD);
    }
    public function run()
    {
        $this->render('vkGroup', array('group_id'=>$this->group_id));
    }
}