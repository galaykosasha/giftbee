<?php
class ItemsWidget extends CWidget
{
    public $model;

    public function init(){}
    public function run()
    {
        if(is_array($this->model[0])) {
            $this->render('itemsArray', array('model'=>$this->model));
        } else {
            $this->render('items', array('model'=>$this->model));
        }
    }
}