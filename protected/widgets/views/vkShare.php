<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 13.07.2015
 * Time: 15:08
 */
?>
<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="http://vk.com/js/api/share.js?91" charset="windows-1251"></script>

<!-- Put this script tag to the place, where the Share button will be -->
<script type="text/javascript">
    document.write(VK.Share.button(
        {
            title: "<?= $this->pageTitle; ?>",
            image: "<?= Yii::app()->createAbsoluteUrl('/').'/uploads/competitions/md-images/'.$competition->image; ?>"
        }
        ,
        {
            type: "custom",
            text: "<span class=\"col-md-12 competition-share\">Поделитесь страницей</span>"
        }
    ));
</script>