<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 03.11.2015
 * Time: 15:35
 * @var $data Competitions
 */
?>
<div class="item">
    <figure>
        <i class="item-star <?= $data->userPlay ? '' : 'hidden'; ?>"></i>
        <div class="item-img-wrp">
            <a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data->company->url, 'competition_url'=>$data->url)); ?>">
                <?= CHtml::image(Yii::app()->request->baseUrl.'/uploads/competitions/xs-images/'.$data->xs_image, '', array('class'=>'item__img')); ?>
            </a>
            </div>
            <figcaption>
                <h2><a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data->company->url, 'competition_url'=>$data->url)); ?>" ><?= $data->title_ru; ?></a></h2>
                <div class="item-footer">
                    <div class="col-md-12 item__company-name p-l"><p><?= $data->company->name_ru; ?></p></div>
                    <div class="info-board info-board--static col-md-12">
                        <div class="col-xs-6 col-md-6 text-left p0 item__static"><i class="fa fa-user"></i> <?= (int)$data->usersCount; ?></div>
                        <div class="col-xs-6 col-md-6 text-right p0 item__static"><i class="fa fa-users"></i> <?= (int)$data->max_users_count; ?></div>
                        <div class="clearfix"></div>
                        <div class="bb-progresbar">
                            <div class="bb-sidebar__count-line" style="width: <?= (int)$data->usersCount * 100 / $data->max_users_count; ?>%;"></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 item__btn-overlay">
                        <a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data->company->url, 'competition_url'=>$data->url)); ?>" class="btn btn-yellow">Подробнее</a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 text-right item__btn-overlay">
                        <?php if($data->group_fb): ?>
                            <i class="fa fa-facebook soc-icon soc-facebook site-hint" data-toggle="tooltip" data-placement="top" title="Конкурс для пользователей Facebook"></i>
                        <?php endif; ?>
                        <?php if($data->group_vk): ?>
                            <i class="fa fa-vk soc-icon soc-vkontakte site-hint" data-toggle="tooltip" data-placement="top" title="Конкурс для пользователей Vkontakte"></i>
                        <?php endif; ?>
                        <?php if(!empty($data->competition_url)): ?>
                            <i class="fa fa-globe soc-icon soc-sitelink site-hint" data-toggle="tooltip" data-placement="top" title="Главное условия конкурса - поделиться контентом спонсора"></i>
                        <?php endif; ?>
                        <?php if(!empty($data->youtube_url)): ?>
                            <i class="fa fa-youtube soc-icon soc-youtube" data-toggle="tooltip" data-placement="top" title="Главное условия конкурса - подписаться на канал youtube"></i>
                        <?php endif; ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </figcaption>
        <div class="clearfix"></div>
    </figure>
</div>