<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 18.06.2015
 * Time: 17:43
 * @var $model UsersBuffer
 * @var $form CActiveForm
 */
?>
<!-- MODAL EMAIL + PHONE -->
<div class="modal fade modal-1 modal-email_phone" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content text-center">
            <div class="modal-body">
                
            <h1>Впервые на <b>GiftBee?</b></h1>
            <span>Зарегистрируйтесь пожалуйста для участия в розыгрышах</span>

            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'=>'user-form',
                'action'=>Yii::app()->createUrl('/site/index'),
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit'=>true
                ),
                'htmlOptions'=>array('class'=>'form-1 modal-form'),
            )); ?>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 input-group">
                        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                        <?php echo $form->emailField($model,'email', array('class'=>'form-control email-field', 'placeholder'=>'Введите e-mail')); ?>
                        <?= $form->error($model,'email', array('class'=>'danger-message')); ?>
                        <!-- <div class="text-left message">Укажите электронный адрес почты. Мы будем уведомлять Вас о новых конкурсах</div> -->
                    </div>

                    <?php /*<div class="col-xs-12 col-md-6 col-lg-6 input-group">
                        <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                        <?= $form->telField($model,'telephone', array('class'=>'form-control phone-field', 'placeholder'=>'+38(0__) ___-__-__')); ?>
                        <?= $form->error($model,'telephone', array('class'=>'danger-message text-left')); ?>
                        <div class="text-left message">Укажите Ваш действующий номер мобильного телеона чтоб мы смогли связаться с Вами в случаи выиграша</div>
                    </div>
                    */ ?>
                </div>

                <div class="col-md-12">
                    <?= CHtml::tag('button', array('class'=>'btn'), 'Продолжить'); ?>
                </div>
                <div class="clearfix"></div>
            <?php $this->endWidget(); ?>
            </div>

            <i class="fa fa-times" data-dismiss="modal"></i>
        </div>
    </div>
</div>
<!-- ENDMODAL EMAIL + PHONE -->
<?php Yii::app()->clientScript->registerScript('closeModalCallBack', '
// MODAL CALL BACK
jQuery(".modal-1").on("hidden.bs.modal", function (e) {
    jQuery(".user-aunt_enter a").addClass("disabled");
	jQuery.ajax({
        url: "'.Yii::app()->createAbsoluteUrl('/site/clearTemporaryData').'",
        method : "POST",
        data: {u_id: '.Yii::app()->session['u_id'].'},
        success: function(html){
            jQuery(".user-aunt_enter a").removeClass("disabled");
            location.reload();

        }
    })
});
',CClientScript::POS_END); ?>
