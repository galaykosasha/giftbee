<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 13.07.2015
 * Time: 14:41
 * @var $this VkGroupWidget
 * @var $group_id
 */?>
<!-- VK Widget -->
<div id="<?= $this->id; ?>"></div>
<script type="text/javascript">
    VK.Widgets.Group("<?= $this->id; ?>", {mode: 0, height: "200",width: "auto", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, <?= $group_id; ?>);
</script>