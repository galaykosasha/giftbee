<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 16.07.2015
 * Time: 14:56
 * @var $model Competitions
 */
?>

<?php foreach($model as $data): ?>
    <div class="item">
        <figure>
            <div class="item-img-wrp">
                <i class="fa fa-star item-star <?= Competitions::model()->findByPk($data['id'])->userPlay ? '' : 'hidden'; ?>"></i>
                <a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data['company_url'], 'competition_url'=>$data['url'])); ?>">
                    <?= CHtml::image(Yii::app()->request->baseUrl.'/uploads/competitions/xs-images/'.$data['xs_image'], ''); ?>
                </a>
                <div class="info-board">
                    <?php if(time() >= strtotime($data['date_end']) || ((int)$data['max_users_count'] > 0 && (int)$data['max_users_count'] <= (int)$data['usersCount'])): ?>
                        <div class="col-xs-4 col-md-4 p0"><i class="fa fa-archive"></i><span> Архив</span></div>
                        <div class="col-xs-4 col-md-4 p0"><i class="fa fa-user"></i><span> <?= (int)$data['usersCount']; ?></span></div>
                        <div class="col-xs-4 col-md-4 p0"><i class="fa fa-gift"></i><span> <?= $data['presents_count']; ?></span></div>
                    <?php else: ?>
                        <div class="col-xs-4 col-md-4 p0">
                            <div class="days"><i class="fa fa-clock-o"></i>&nbsp;<?= floor((strtotime($data['date_end']) - time())/86400); ?>д.</div>
                            <div class="timer" data-seconds-left="<?= strtotime($data['date_end']) - time(); ?>"></div>
                        </div>
                        <div class="col-xs-4 col-md-4 text-left">
                            <div class="days"><i class="fa fa-clock-o"></i>&nbsp;<?= floor((strtotime($data['date_end']) - time())/86400) ? floor((strtotime($data['date_end']) - time())/86400).'д.' : ''; ?> </div>
                            <div class="timer <?= floor((strtotime($data['date_end']) - time())/86400) ? 'hidden' : ''; ?>" data-seconds-left="<?= strtotime($data->date_end) - time(); ?>"></div>
                        </div>
                        <div class="col-xs-4 col-md-4 p0"><i class="fa fa-user"></i><span> <?= (int)$data['usersCount']; ?></span></div>
                        <div class="col-xs-4 col-md-4 p0"><i class="fa fa-gift"></i><span> <?= $data['presents_count']; ?></span></div>
                    <?php endif; ?>
                </div>
            </div>

            <figcaption>
                <h2><a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data['company_url'], 'competition_url'=>$data['url'])); ?>" ><?= $data['title_ru']; ?></a></h2>
                <div class="item-footer">
                    <div class="col-xs-6 col-md-7 p-l"><p><?= $data['company_name_ru']; ?></p></div>
                    <div class="col-xs-6 col-md-5 p0"><a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data['company_url'], 'competition_url'=>$data['url'])); ?>" class="btn btn-yellow">Подробнее</a></div>
                    <div class="clearfix"></div>
                </div>
            </figcaption>
            <div class="clearfix"></div>
        </figure>
    </div>
<?php endforeach; ?>