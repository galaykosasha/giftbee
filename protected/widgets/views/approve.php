<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 19.06.2015
 * Time: 16:02
 */
?>
<!-- MODAL THANKS -->
<div class="modal fade modal-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content text-center">
    		<div class="modal-body">
	            <i class="fa fa-paper-plane"></i>
	            <h1><b>Спасибо</b></h1>
	            <span class="gb-feedback">На Вашу почту <b><?= $model->email; ?></b> мы отправили письмо<br> с дальнейшими инструкциями.</span>

	            <p id="new-mail" style="text-decoration: underline; color: #337ab7; font-size: 1.1em; cursor: pointer;">Указать другую почту</p>
                <p>Если Вы подтвердили почту с другого устройства<br> пожалуйста авторизируйтесь</p>
                <div class="login-group_button">
                    <div class="row">
                        <?php if(Yii::app()->session['social_id'] != 2): ?>
                                <?= CHtml::link('<i class="pull-left text-center fa fa-facebook"></i><span>Facebook</span>', array('/site/loginFb', 'url'=>true), array('class'=>'login-item btn btn-group fb-color')); ?>
                        <?php endif; ?>

                        <?php if(Yii::app()->session['social_id'] != 1): ?>
                                <?= CHtml::link('<i class="pull-left text-center fa fa-vk"></i><span>Вконтакте</span>', array('/site/loginVk', 'url'=>true), array('class'=>'login-item btn btn-group vk-color')); ?>
                        <?php endif; ?>
                    </div>
                </div>
    		</div>
	        <i class="fa fa-times" data-dismiss="modal"></i>
    	</div>
    </div>
</div>
<!-- ENDMODAL THANKS -->
<?php Yii::app()->clientScript->registerScript('closeModalCallBack', '
// MODAL CALL BACK
jQuery("#new-mail").on("click", function (e) {
    jQuery(".user-aunt_enter a").addClass("disabled");
	jQuery.ajax({
        url: "'.Yii::app()->createAbsoluteUrl('/site/clearTemporaryData').'",
        method : "POST",
        data: {u_id: '.Yii::app()->session['u_id'].'},
        success: function(html){
            jQuery(".user-aunt_enter a").removeClass("disabled");
            location.reload();

        }
    })
});
',CClientScript::POS_END); ?>