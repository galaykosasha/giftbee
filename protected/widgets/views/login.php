<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 18.06.2015
 * Time: 17:43
 */
?>
<!-- MODAL LOGIN-->
<div class="modal fade modal-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content text-center">
    <div class="modal-body">
        
            <h1>Авторизируйтесь через социальную сеть</h1>
            <span>Если Вы ранее регистрировались войдите через прежнюю социальную сеть</span>

            <div class="login-group_button">
                <div class="row">
                    <?php if(Yii::app()->session['social_id'] != 2): ?>
                            <?= CHtml::link('<i class="pull-left text-center fa fa-facebook"></i><span>Facebook</span>', array('/site/loginFb', 'url'=>true), array('class'=>'login-item btn-group btn fb-color')); ?>
                    <?php endif; ?>

                    <?php if(Yii::app()->session['social_id'] != 1): ?>
                            <?= CHtml::link('<i class="pull-left text-center fa fa-vk"></i><span>Вконтакте</span>', array('/site/loginVk', 'url'=>true), array('class'=>'login-item btn-group btn vk-color')); ?>
                    <?php endif; ?>
                </div>
            </div>
    </div>  

            <i class="fa fa-times" data-dismiss="modal"></i>
        </div>
    </div>
</div>
<!-- ENDMODAL LOGIN -->