<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 16.07.2015
 * Time: 14:56
 * @var $this ItemsWidget
 * @var $model Competitions
 * @var $data Competitions
 */
?>
<?php foreach($model as $data): ?>
    <?php if($data->max_users_count > 1): ?>
        <?php $this->render('itemsForUsersCount', array('data'=>$data)); ?>
    <?php else: ?>
        <?php $this->render('item', array('data'=>$data)); ?>
    <?php endif; ?>
<?php endforeach; ?>