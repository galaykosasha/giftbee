<div class="item">
    <figure>
        <i class="item-star <?= $data->userPlay ? '' : 'hidden'; ?>"></i>
        <div class="item-img-wrp">
            <a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data->company->url, 'competition_url'=>$data->url)); ?>">
                <?= CHtml::image(Yii::app()->request->baseUrl.'/uploads/competitions/xs-images/'.$data->xs_image, '', array('class'=>'item__img')); ?>
            </a>

        </div>
        <figcaption>
            <h2><a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data->company->url, 'competition_url'=>$data->url)); ?>" ><?= $data->title_ru; ?></a></h2>
            <div class="item-footer">
                <div class="col-md-12 item__company-name p-l"><p><?= $data->company->name_ru; ?></p></div>
                <div class="info-board col-md-12">
                    <div class="col-xs-7 col-sm-5 col-md-5 p0 text-left">
                        <i class="fa fa-clock-o site-hint" data-toggle="tooltip" data-placement="right" title="До розыгрыша осталось"></i>
                        <div class="days">
                            <?php if(strtotime($data->date_end) < time()): ?>
                                Архив
                            <?php elseif($data->days): ?>
                                <?=  $data->days.'<span class="item-days__text">дн.</span>'; ?>
                            <?php endif; ?>
                        </div>
                        <div class="timer <?= $data->days ? 'hidden' : ''; ?>" data-seconds-left="<?= strtotime($data->date_end) - time(); ?>"></div></div>
                    <div class="hidden-xs col-sm-2 col-md-2 p0">
                        <div class="item_gift-count">
                            <i class="fa fa-gift site-hint" data-toggle="tooltip" data-placement="top" title="Количество разыгрываемых подарков"></i><span> <?= $data->presents_count; ?>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5 timer-controll">
                        <div class="item_user-count p0 text-right"><i class="fa fa-user site-hint" data-toggle="tooltip" data-placement="top" title="Количество участников"></i> <?= (int)$data->usersCount; ?><!-- <span class="item-user__text">чел.</span> --></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 item__btn-overlay">
                    <a href="<?= Yii::app()->createUrl('/site/competition', array('company'=>$data->company->url, 'competition_url'=>$data->url)); ?>" class="btn btn-yellow">Подробнее</a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right item__btn-overlay">
                    <?php if($data->group_fb): ?>
                        <i class="fa fa-facebook soc-icon soc-facebook site-hint" data-toggle="tooltip" data-placement="top" title="Конкурс для пользователей Facebook"></i>
                    <?php endif; ?>
                    <?php if($data->group_vk): ?>
                        <i class="fa fa-vk soc-icon soc-vkontakte site-hint" data-toggle="tooltip" data-placement="top" title="Конкурс для пользователей Vkontakte"></i>
                    <?php endif; ?>
                    <?php if(!empty($data->competition_url)): ?>
                        <i class="fa fa-globe soc-icon soc-sitelink site-hint" data-toggle="tooltip" data-placement="top" title="Главное условия конкурса - поделиться контентом спонсора"></i>
                    <?php endif; ?>
                    <?php if(!empty($data->youtube_url)): ?>
                        <i class="fa fa-youtube soc-icon soc-youtube site-hint" data-toggle="tooltip" data-placement="top" title="Главное условия конкурса - подписаться на канал youtube"></i>
                    <?php endif; ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </figcaption>
        <div class="clearfix"></div>
    </figure>
</div>