<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 24.06.2015
 * Time: 16:58
 */
?>
<!-- SIDEBAR -->
<aside class="sidebar h100">
    <div class="logo">
        <a href="<?= Yii::app()->createUrl('site/index'); ?>"><img class="sidebar-logo" src="<?= Yii::app()->baseUrl; ?>/images/logo.png" alt="logo"></a>
        </div>
    <div class="nav-wrp">
        <figure class="user-aunt">
            <div class="user-aunt_photo pull-left">
                <img src="<?= $user->image; ?>" alt=""/>
            </div>
            <div class="user-aunt_enter pull-left">
                <span class="first-name"><?= $user->first_name; ?></span>
                <span class="last-name"><?= $user->last_name; ?></span>
            </div>
            <div class="clearfix"></div>
        </figure>

        <nav class="h100">
            <?php
            $this->widget('zii.widgets.CMenu', array(
                'items'=>array(
                    array('label'=>'<i class="faa-slow fa fa-home faa-wrench"></i> Главная', 'url'=>array('/site/index'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    array('label'=>'<i class="faa-slow fa fa-gift"></i> Конкурсы <sub class="bb-top-sub fa fa-heart"></sub>', 'url'=>array('/site/competitions'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    // array('label'=>'<i class="faa-slow fa fa-gift"></i> Компании <sub class="bb-top-sub fa fa-heart"></sub>', 'url'=>array('/site/companies'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    // array('label'=>'<i class="faa-slow fa fa-gift"></i> Победители <sub class="bb-top-sub fa fa-heart"></sub>', 'url'=>array('/site/winners'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    //array('label'=>'<i class="faa-slow fa fa-star"></i> Мои конкурсы', 'url'=>array('/site/myCompetitions'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    array('label'=>'<i class="faa-slow fa fa-archive faa-horizontal"></i> Архив', 'url'=>array('/site/archive'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    array('label'=>'<i class="fa fa-comments"></i> Отзывы', 'url'=>array('/site/reviews')),
                ),
                'encodeLabel'=>false,
                'htmlOptions'=>array('class'=>'gb-menu nav'),
            ));
            ?>

            <?php
            $this->widget('zii.widgets.CMenu', array(
                'items'=>array(
                    array('label'=>'<i class="faa-slow fa fa-user-secret faa-wrench"></i> Админ', 'url'=>array('/control/default/index'), 'linkOptions'=>array('class'=>'faa-parent animated-hover'), 'visible'=>Yii::app()->user->role == 'admin'),
                    //array('label'=>'<i class="faa-slow fa fa-wrench faa-wrench"></i> Настройки', 'url'=>array('/profile/default/settings'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    array('label'=>'<i class="fa fa-sign-out faa-burst"></i> Выход', 'url'=>array('/site/logout'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                ),
                'encodeLabel'=>false,
                'htmlOptions'=>array('class'=>'gb-menu nav'),
            ));
            ?>

            <?php
            $this->widget('zii.widgets.CMenu', array(
                'items'=>array(
                    array('label'=>'<i class="fa fa-question-circle"></i> Что такое Giftbee?', 'url'=>array('/site/faq'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    array('label'=>'<i class="fa fa-info-circle"></i> Инструкция', 'url'=>array('/site/instruction'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    array('label'=>'<i class="fa fa-plus-circle"></i> Разместить конкурс', 'url'=>array('/site/addCompetition'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                    array('label'=>'<i class="fa fa-comment"></i> Обратная связь', 'url'=>array('/site/feedback'), 'linkOptions'=>array('class'=>'faa-parent animated-hover')),
                ),
                'encodeLabel'=>false,
                'htmlOptions'=>array('class'=>'gb-menu gb-menu-uc nav'),
            ));
            ?>
        </nav>
    </div>
</aside>
<div class="sidebar sidebar-bottom"></div>
<!-- ENDSIDEBAR -->

