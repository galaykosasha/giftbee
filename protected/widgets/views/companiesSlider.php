<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 03.11.2015
 * Time: 15:00
 * @var $this CompaniesSliderWidget
 * @var $model Companies
 */
?>
<?php foreach($model as $data): ?>
    <div class="item">
        <figure>
            <div class="item-img-wrp">
                <a href="<?= Yii::app()->createUrl('/site/company', array('url'=>$data->url)); ?>">
                    <?= CHtml::image(Yii::app()->request->baseUrl.'/uploads/companies/logos/'.$data->image, '', array('class'=>'item__img')); ?>
                </a>
            </div>
            <figcaption>
                <h2><a href="<?= Yii::app()->createUrl('/site/company', array('url'=>$data->url)); ?>" ><?= $data->name_ru; ?></a></h2>
                <div class="item-footer">
                    <div class="col-md-12 item__company-name p-l">
                        <p>
                            <?= $data->name_ru; ?>
                        </p>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 item__btn-overlay">
                        <a href="<?= Yii::app()->createUrl('/site/company', array('url'=>$data->url)); ?>" class="btn btn-yellow">Подробнее</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </figcaption>
            <div class="clearfix"></div>
        </figure>
    </div>
<?php endforeach; ?>