<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 29.07.2015
 * Time: 12:18
 * @var $model UserCompetitions
 */
?>
<?php foreach($model as $data): ?>
    <div class="comp-user-board-item">
        <a href="<?= $data->social_id == 1 ? 'http://vk.com/id' : 'https://www.facebook.com/'; ?><?= $data->user_social_id; ?>"><img src="<?= $data->user->image; ?>" alt="<?= $data->user->first_name.' '.$data->user->last_name; ?>">
            <p class="comp-user-name"><?= $data->user->first_name.'<br>'.$data->user->last_name; ?></p>
            <b>#<?= $data->number; ?></b>
            <i class="fa <?= $data->social_id == 1 ? 'fa-vk soc-icon soc-vkontakte' : 'fa-facebook soc-icon soc-facebook' ; ?>"></i>
        </a>
    </div>
<?php endforeach; ?>
<?php if(!empty($video)): ?>
    <div class="comp-video">
        <h3><i class="fa fa-video-camera"></i> Видео розыгрыша</h3>
        <iframe class="video-size" width="640" height="360" src="https://www.youtube.com/embed/<?= $video; ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
    </div>
<?php endif; ?>