<?php
class MenuSidebarWidget extends CWidget
{
    public $user = null;
    public function init()
    {
        if(!Yii::app()->user->isGuest) {
            $this->user = Users::model()->findByPk(Yii::app()->user->id);
        }
    }
    public function run()
    {
        if($this->user){
            $this->render('userSidebar', array('user'=>$this->user));
        } else {
            $this->render('sidebar');
        }
    }
}