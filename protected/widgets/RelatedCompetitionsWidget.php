<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 03.08.2015
 * Time: 17:27
 */

class RelatedCompetitionsWidget extends CWidget
{
    public $excludedIds;
    protected $model;
    public function init()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date_start DESC';
        $criteria->condition = 'date_start < NOW() AND date_end > NOW()';
        $criteria->compare('is_overflow', 0);
        $criteria->compare('is_active', 1);
        $criteria->limit = 3;
        $criteria->order = 'rand()';

        $criteria->addCondition('id <> '.$this->excludedIds);

        $this->model = Competitions::model()->findAll($criteria);
    }
    public function run()
    {
        $this->widget('application.widgets.ItemsWidget', array('model'=>$this->model));
    }
}