<?php
class AuthModalWidget extends CWidget
{
    public $model = null;

    public function init()
    {
        if(Yii::app()->session['u_id']){
            $this->model = UsersBuffer::model()->findByPk(Yii::app()->session['u_id']);
            Yii::app()->clientScript->registerScript('registerEmail', 'jQuery(".modal-1").modal("show");', CClientScript::POS_READY);
        } elseif(Yii::app()->session['social_id']){
            Yii::app()->clientScript->registerScript('registerEmail', 'jQuery(".modal-1").modal("show");', CClientScript::POS_READY);
        }
    }
    public function run()
    {
        if($this->model) {
            if(Yii::app()->session['sending_message']) {
                $this->render('approve');
            } else {
                $this->model->scenario = 'addToBuffer';
                $this->render('register', array('model'=>$this->model));
            }
        }
        else
            $this->render('login');
    }
}