<?php
/**
 * Created by PhpStorm.
 * User: alex_
 * Date: 10.11.2015
 * Time: 18:19
 */
class Mailing extends CFormModel
{
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('text, country_id, region_id, name', 'safe'),
        );
    }
}