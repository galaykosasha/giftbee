<?php

/**
 * This is the model class for table "gb_competitions".
 *
 * The followings are the available columns in table 'gb_competitions':
 * @property string $id
 * @property integer $company_id
 * @property string $xs_image
 * @property string $md_image
 * @property string $title_ru
 * @property string $short_description_ru
 * @property string $description_ru
 * @property string $date_start
 * @property string $date_end
 * @property integer $max_users_count
 * @property string $presents_count
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $group_vk
 * @property integer $group_fb
 * @property integer $site_url
 * @property integer $competition_url
 * @property integer $video
 * @property string $url
 * @property integer $is_active
 * @property string $winners
 * @property string $is_overflow
 * @property integer $sale_type
 * @property string $sale
 * @property string $sale_description
 * @property string $youtube_url
 * @property string $post_url_fb
 * @property string $post_url_vk
 * @property string $winner_form_url
 * @property string $mail_text
 */
class Competitions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gb_competitions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('company_id, md_image, xs_image, title_ru, description_ru, url, type', 'required'),
			array('url', 'unique'),
			array('id, company_id, max_users_count, country_id, region_id, city_id, is_active, is_overflow, sale_type, type, presents_count', 'numerical', 'integerOnly'=>true),
			array('xs_image, md_image, title_ru, short_description_ru, group_fb, group_vk, site_url, competition_url, video, url, winners, sale, winner_form_url', 'length', 'max'=>255),
			array('youtube_url, post_url_vk, post_url_fb', 'length', 'max'=>150),
			array('presents_count', 'length', 'max'=>11),
			array('date_start, date_end, sale_description, mail_text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('site_url, id, company_id, xs_image, md_image, title_ru, short_description_ru, description_ru, date_start, date_end, max_users_count, presents_count, country_id, winner_form_url,
			    region_id, city_id, group_fb, group_vk, competition_url, video, url, is_active, winners, is_overflow, sale_type, sale, type, youtube_url, mail_text', 'safe', 'on'=>'search'),
		);
	}

    public function getDays()
    {
        return floor((strtotime($this->date_end) - time())/86400);
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'country'=>array(self::BELONGS_TO, 'Country', 'country_id'),
            'region'=>array(self::BELONGS_TO, 'Region', 'region_id'),
            'city'=>array(self::BELONGS_TO, 'City', 'city_id'),
            'userCompetitions'=>array(self::HAS_MANY, 'UserCompetitions', 'competition_id'),

            'company'=>array(self::BELONGS_TO, 'Companies', 'company_id'),
            'vk'=>array(self::HAS_ONE, 'GroupsCompetition', 'competition_id', 'condition'=>'social_id = 1'),
            'fb'=>array(self::HAS_ONE, 'GroupsCompetition', 'competition_id', 'condition'=>'social_id = 2'),
            'usersCount'=>array(self::STAT, 'UserCompetitions', 'competition_id'),
            'presents'=>array(self::STAT, __CLASS__, 'presents_count'),
            'images'=>array(self::HAS_MANY, 'Images', 'competition_id'),
		);
	}


    public function getVkUserIsParticipant()
    {
        return UserCompetitions::model()->exists('user_id = :u_id AND social_id = 1 AND competition_id = :c_id', array(':u_id'=>Yii::app()->user->id, ':c_id'=>$this->id)) ? true : false;
    }

    public function getFbUserIsParticipant()
    {
        return UserCompetitions::model()->exists('user_id = :u_id AND social_id = 2 AND competition_id = :c_id', array(':u_id'=>Yii::app()->user->id, ':c_id'=>$this->id)) ? true : false;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'company_id' => Yii::t('main', 'Company'),
			'xs_image' => Yii::t('main', 'Min Image'),
			'md_image' => Yii::t('main', 'Max Image'),
			'title_ru' => Yii::t('main', 'Title Ru'),
			'short_description_ru' => Yii::t('main', 'Short Description Ru'),
			'description_ru' => Yii::t('main', 'Description Ru'),
			'date_start' => Yii::t('main', 'Date Start'),
			'date_end' => Yii::t('main', 'Date End'),
			'max_users_count' => Yii::t('main', 'Max Users Count'),
			'presents_count' => Yii::t('main', 'Presents Count'),
			'country_id' => Yii::t('main', 'Country'),
			'region_id' => Yii::t('main', 'Region'),
			'city_id' => Yii::t('main', 'City'),
			'group_vk' => Yii::t('main', 'Group VK'),
			'group_fb' => Yii::t('main', 'Group FB'),
			'site_url' => Yii::t('main', 'Site Url'),
			'competition_url' => Yii::t('main', 'Competition Url'),
			'video' => Yii::t('main', 'Video'),
			'url' => Yii::t('main', 'URL'),
			'is_active' => Yii::t('main', 'Is active'),
			'winners' => Yii::t('main', 'Winners'),
			'is_overflow' => Yii::t('main', 'Full users'),
			'sale_type' => Yii::t('main', 'Sale Type'),
			'sale' => Yii::t('main', 'Sale'),
			'sale_description' => Yii::t('main', 'Sale Description'),
			'type' => Yii::t('main', 'Type'),
			'youtube_url' => Yii::t('main', 'Youtube url'),
			'post_url_vk' => Yii::t('main', 'Post Url vk'),
			'post_url_fb' => Yii::t('main', 'Post Url fb'),
			'winner_form_url' => Yii::t('main', 'Ссылка на гугл форму'),
			'mail_text' => Yii::t('main', 'Письмо неудачникам'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($archive = false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        if($archive) {
            $criteria->condition = 'NOW() > date_end';
        } else {
            $criteria->condition = 'NOW() < date_end';
        }
        $criteria->order = 'date_end ASC';
		$criteria->compare('id',$this->id,true);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('xs_image',$this->xs_image,true);
		$criteria->compare('md_image',$this->md_image,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('short_description_ru',$this->short_description_ru,true);
		$criteria->compare('description_ru',$this->description_ru,true);
		$criteria->compare('date_start',$this->date_start,true);
		//$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('max_users_count',$this->max_users_count);
		$criteria->compare('presents_count',$this->presents_count,true);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('region_id',$this->region_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('group_vk',$this->group_vk);
		$criteria->compare('group_fb',$this->group_fb);
		$criteria->compare('site_url',$this->site_url);
		$criteria->compare('competition_url',$this->competition_url);
		$criteria->compare('video',$this->video);
		$criteria->compare('url',$this->url);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('winners',$this->winners);
		$criteria->compare('is_overflow',$this->is_overflow);
		$criteria->compare('sale_type',$this->sale_type);
		$criteria->compare('sale',$this->sale);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Competitions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getIsArchive()
    {
        return time() >= strtotime($this->date_end) || ((int)$this->max_users_count > 0 && (int)$this->max_users_count <= (int)$this->usersCount);
    }

    public function getArchiveModels()
    {
        return $this->findAllBySql('SELECT *, (SELECT COUNT(u_c.id) FROM gb_user_competitions u_c WHERE u_c.competition_id = c.id) AS usersCount FROM gb_competitions c
          WHERE (c.max_users_count > 1 AND c.max_users_count <= (SELECT COUNT(u_c.id) FROM gb_user_competitions u_c WHERE u_c.competition_id = c.id)) OR date_end < NOW() ORDER BY date_end ASC LIMIT 0,25');
    }

    public function getUserPlay()
    {
        return UserCompetitions::model()->exists('user_id = :u_id AND competition_id = :c_id', array(':u_id'=>Yii::app()->user->id, ':c_id'=>$this->id));
    }

    public static function countActivePresents()
    {
        $presents = Yii::app()->db->createCommand()
            ->select('SUM(presents_count) as presents')
            ->from('gb_competitions')
            ->where('is_active = 1 AND date_end > :time AND is_overflow = 0', array(':time'=>date('Y-m-d H:i:s')))
            ->queryRow();
        return $presents['presents'];
    }

    public static function countWinners()
    {
        $winners = Yii::app()->db->createCommand()
            ->select('SUM(presents_count) as winners')
            ->from('gb_competitions')
            ->where('is_active = 1 AND date_end < :time OR is_overflow = 1', array(':time'=>date('Y-m-d H:i:s')))
            ->queryRow();
        return $winners['winners'];
    }

	/**
	 * @param $for
	 * @return string
	 */
	public function getEmails($for)
	{
		$winnersArray = explode(',', trim($this->winners, ','));
		$criteria = new CDbCriteria();
		$criteria->compare('competition_id', $this->id);
		if($for == 'winners')
			$criteria->addInCondition('number', $winnersArray);
		elseif($for == 'loosers')
			$criteria->addNotInCondition('number', $winnersArray);

		$usersCompetition = implode(',', CHtml::listData(UserCompetitions::model()->findAll($criteria), 'id', 'user_id'));
		$criteriaUsers = new CDbCriteria();
		$criteriaUsers->addInCondition('id', $usersCompetition);
		return implode(',', CHtml::listData($this->findAll($criteriaUsers), 'id', 'email'));
	}

	/**
	 * @param string $for winners|loosers|admins
	 * @param string $text
	 */
	public function sendEmails($for = 'winners', $text = '')
	{
		if($for === 'winners') {
			$emails = explode(',', trim($this->getEmails('winners'), ','));
			$fromEmail = 'winner@giftbee.ua';
		}
		elseif($for === 'loosers') {
			$emails = explode(',', trim($this->getEmails('loosers'), ','));
			$fromEmail = 'promo@giftbee.ua';
		}
		else {
			$emails = array('giftbeeua@gmail.com');
			$fromEmail = 'test@giftbee.ua';
		}

		foreach($emails as $key => $email) {
			$subject='=?UTF-8?B?'.base64_encode('Команда Giftbee.ua').'?=';
			$headers="From: Giftbee <{".$fromEmail."}>\r\n".
				"Reply-To: ".$fromEmail."\r\n".
				"MIME-Version: 1.0\r\n".
				"Content-type: text/html; charset=utf8";

			if(mail($email, $subject, $text, $headers))
				echo "message for ".$email." (".$key.") sended! \r\n";
			else
				echo "message for ".$email." (".$key.") not sended! \r\n";
		}
	}
}
