<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 04.08.2015
 * Time: 18:06
 */
class Feedback extends CFormModel
{
    public $email;
    public $comment;

    public function rules()
    {
        return array(
            array('email, comment', 'required'),
            array('email', 'match', 'pattern'=>'/^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/', 'message'=>'Введите корректный {attribute}'),
            array('email', 'length', 'max'=>200),
            array('comment', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('email, comment', 'safe', 'on'=>'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'email' => Yii::t('main', 'Имя'),
            'comment' => Yii::t('main', 'Ваш коментарий'),
        );
    }
}