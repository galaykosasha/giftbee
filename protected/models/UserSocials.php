<?php

/**
 * This is the model class for table "gb_user_socials".
 *
 * The followings are the available columns in table 'gb_user_socials':
 * @property string $id
 * @property integer $social_id
 * @property integer $user_id
 * @property string $social_user_id
 * @property string $token
 */
class UserSocials extends CActiveRecord
{
    const VK_SOCIAL = 1;
    const FB_SOCIAL = 2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gb_user_socials';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('social_id, user_id, social_user_id, token', 'required'),
			array('social_id, user_id', 'numerical', 'integerOnly'=>true),
			array('social_user_id, token', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, social_id, user_id, social_user_id, token', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'social_id' => Yii::t('main', 'Social'),
			'user_id' => Yii::t('main', 'User'),
			'social_user_id' => Yii::t('main', 'Social User'),
			'token' => Yii::t('main', 'Token'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('social_id',$this->social_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('social_user_id',$this->social_user_id,true);
		$criteria->compare('token',$this->token,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserSocials the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
