<?php

/**
 * This is the model class for table "gb_users".
 *
 * The followings are the available columns in table 'gb_users':
 * @property string $id
 * @property string $first_name
 * @property string $last_name
 * @property string $birth_date
 * @property string $email
 * @property string $telephone
 * @property string $city_id
 * @property string $country_id //DEPRECATED
 * @property string $region_id //DEPRECATED
 * @property integer $sex
 * @property integer $mailing
 * @property string $image
 * @property string $role
 * @property string $password
 * @property string $register_date
 * @property integer $active
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gb_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
            //array('telephone', 'match', 'pattern'=>'/^\+\d{2}\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/', 'message'=>'Введите корректный номер {attribute}'),
            array('email', 'match', 'pattern'=>'/^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/', 'message'=>'Введите корректный {attribute}'),
            array('birth_date', 'match', 'pattern'=>'/^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d$/', 'message'=>'Введите корректную дату'),
            array('sex, city_id, email, first_name, last_name, birth_date,', 'required', 'on'=>'settings'),
            //array('active', 'compare', 'compareValue'=>'1', 'message'=>'Вы должны прочитать условия и согласится', 'on'=>'settings'),
			array('sex, mailing, country_id, city_id, region_id', 'numerical', 'integerOnly'=>true),
            array('first_name, last_name', 'length', 'max'=>100),
			array('birth_date', 'length', 'max'=>10, 'min'=>10),
			array('email, image, password', 'length', 'max'=>255),
			array('role', 'length', 'max'=>50),

			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, last_name, email, telephone, city_id, sex, mailing, image, role, password, active, birth_date, region_id, register_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'city'=>array(self::BELONGS_TO, 'City', 'city_id'),
            'region'=>array(self::BELONGS_TO, 'Region', 'region_id'),
            'country'=>array(self::BELONGS_TO, 'Country', 'country_id'),
            'socials'=>array(self::HAS_MANY, 'UserSocials', 'user_id'),
            'competitionsCount'=>array(self::STAT, 'UserCompetitions', 'user_id'),
            'competitions'=>array(self::HAS_MANY, 'UserCompetitions', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'first_name' => Yii::t('main', 'First Name'),
			'last_name' => Yii::t('main', 'Last Name'),
			'birth_date' => Yii::t('main', 'Birth date'),
			'email' => Yii::t('main', 'Email'),
			'telephone' => Yii::t('main', 'Telephone'),
            'country_id' => Yii::t('main', 'Country'),
            'region_id' => Yii::t('main', 'Region'),
            'city_id' => Yii::t('main', 'City'),
			'sex' => Yii::t('main', 'Sex'),
			'mailing' => Yii::t('main', 'Mailing'),
			'image' => Yii::t('main', 'Image'),
			'role' => Yii::t('main', 'Role'),
			'password' => Yii::t('main', 'Password'),
			'active' => Yii::t('main', 'Active'),
			'register_date' => Yii::t('main', 'Register date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->order = 'id DESC';
		$criteria->compare('id',$this->id,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('birth_date',$this->birth_date,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('country_id',$this->country_id,true);
		$criteria->compare('region_id',$this->region_id,true);
		$criteria->compare('city_id',$this->city_id,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('mailing',$this->mailing);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('register_date',$this->register_date);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * DEPRECATED
     * @return array
     */
    public function getCountriesList()
    {
        $model = Country::model()->findAll();
        return CHtml::listData($model, 'country_id', 'name');
    }

    /**
     * DEPRECATED
     * @return array
     */
    public function getRegionsList()
    {
        $model = Region::model()->findAll('country_id = :country', array(':country'=>$this->country->country_id));
        return CHtml::listData($model, 'region_id', 'name');
    }

    public function getCitiesList()
    {
        $model = City::model()->findAll(array('order'=>'name ASC'));
        $resArray = array();
        foreach($model as $key => $value) {
            $resArray[$value->city_id] = $value->name.' ('.$value->region->name.')';
        }
        return $resArray;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function afterSave() {
        parent::afterSave();
    }

    public function saveSocialAccount($buffer)
    {
        $socialUser = new UserSocials();
        $socialUser->token = $buffer->token;
        $socialUser->social_id = $buffer->social_id;
        $socialUser->social_user_id = $buffer->social_user_id;
        $socialUser->user_id = $this->id;
        if($socialUser->save()){
            UsersBuffer::model()->deleteByPk($buffer->id);
            unset(Yii::app()->session['sending_message']);
            unset(Yii::app()->session['u_id']);

            $socialClass = null;
            switch($socialUser->social_id){
                case 1:
                    $socialClass = new Vk();
                    break;
                case 2:
                    $socialClass = new Fb();
                    break;
            }
            $socialClass->login($socialUser->social_user_id, $socialUser->social_id);
            if(empty($this->first_name)) {
                $this->saveSocialData($socialClass);
            }
            Yii::app()->user->setFlash('success', '<p>Спасибо!<br /> Ваша почта успешно подтверджена.</p>');
            if(isset(Yii::app()->session['url'])) {
                $url = Yii::app()->session['url'];
                unset(Yii::app()->session['url']);
                header("Location: $url");
            }
            Yii::app()->getController()->redirect(Yii::app()->homeUrl);
        }
    }

    /**
     * @param $model
     */
    private function saveSocialData($model)
    {
        $data = $model->getUser();
        $this->attributes = $data;
        $this->save();
    }

    public function getCityName()
    {
        $city = '';
        if(isset($this->city_id) && !empty($this->city_id)) {
            $city = $this->city->name;
        }
        return $city;
    }
    public function getRegionName()
    {
        $region = '';
        if(isset($this->region_id) && !empty($this->region_id)) {
            $region = $this->region->name;
        }
        return $region;
    }

    public function getHasVk()
    {
        return UserSocials::model()->exists('user_id = :user_id AND social_id = 1', array(':user_id'=>$this->id));
    }
    public function getHasFB()
    {
        return UserSocials::model()->exists('user_id = :user_id AND social_id = 2', array(':user_id'=>$this->id));
    }

    public static function isVkUser()
    {
        return UserSocials::model()->exists('user_id = :user_id AND social_id = 1', array(':user_id'=>Yii::app()->user->id));
    }

    public static function isFbUser()
    {
        return UserSocials::model()->exists('user_id = :user_id AND social_id = 2', array(':user_id'=>Yii::app()->user->id));
    }

    public function getSocialId($social_id)
    {
        foreach($this->socials as $social)
        {
            if($social->social_id == $social_id && $social_id == UserSocials::VK_SOCIAL) {
                return $social->social_user_id;
            }
            elseif($social->social_id == $social_id && $social_id == UserSocials::FB_SOCIAL) {
                return $social->social_user_id;
            }
        }
    }

    public static function userRegisterStatistic($period = 'day')
    {
        $currentHour = date('H', time());
        $currentDay = date('d', time());
        $currentWeek = date('w', time());
        $currentMonth = date('m', time());
        $currentYear = date('Y', time());

        $data = '';

        if($period == 'day'){
            $start = $currentYear.'-'.$currentMonth.'-'.$currentDay.' 00:00:00';
            $userStatistic = Users::model()->findAll('register_date >= :start AND register_date <= NOW()', array(':start'=>$start));
        }elseif($period == 'week'){
            $userStatistic = Users::model()->findAll('register_date > DATE_SUB(CURDATE(), INTERVAL (DAYOFWEEK(CURDATE()) -1) DAY) AND register_date < DATE_ADD(CURDATE(), INTERVAL (9 - DAYOFWEEK(CURDATE())) DAY)');
        }elseif($period == 'month'){
            $userStatistic = Users::model()->findAll('register_date > DATE_SUB(curdate(),INTERVAL DAYOFMONTH(curdate())-1 DAY) AND register_date < DATE_ADD(LAST_DAY(CURDATE()), INTERVAL 1 DAY)');
        }elseif($period == 'year'){
            $userStatistic = Users::model()->findAll();
        }

        foreach($userStatistic as $stat) {
            $start = $currentYear.'-'.$currentMonth.'-'.date('d', strtotime($stat->register_date));
            $end = $currentYear.'-'.$currentMonth.'-'.(date('d', strtotime($stat->register_date)) + 1).' 00:00:00';
            $data .= '{ year: "'.$start.'", value: '.Users::model()->count('register_date < :end', array(':end'=>$end)).' },';
            //$data[$start] = Users::model()->count('register_date < :end', array(':end'=>$end));
        }
        return $data;
    }
}
