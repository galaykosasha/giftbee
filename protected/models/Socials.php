<?php

/**
 * This is the model class for table "gb_socials".
 *
 * The followings are the available columns in table 'gb_socials':
 * @property string $id
 * @property string $name
 * @property string $params
 * @property string $client_id
 * @property string $client_secret
 * @property string $codeGetUrl
 * @property string $tokenGetUrl
 */
class Socials extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gb_socials';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name, params, client_id, client_secret, codeGetUrl, tokenGetUrl', 'required'),
			array('name', 'length', 'max'=>50),
			array('params, client_id, client_secret, codeGetUrl, tokenGetUrl', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, params, client_id, client_secret, codeGetUrl, tokenGetUrl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'name' => Yii::t('main', 'Name'),
			'params' => Yii::t('main', 'Params'),
			'client_id' => Yii::t('main', 'Client'),
			'client_secret' => Yii::t('main', 'Client Secret'),
			'codeGetUrl' => Yii::t('main', 'Code Get Url'),
			'tokenGetUrl' => Yii::t('main', 'Token Get Url'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('params',$this->params,true);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('client_secret',$this->client_secret,true);
		$criteria->compare('codeGetUrl',$this->codeGetUrl,true);
		$criteria->compare('tokenGetUrl',$this->tokenGetUrl,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Socials the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
