<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 04.08.2015
 * Time: 18:06
 */
class AddCompetition extends CFormModel
{
    public $facebook_url;
    public $name;
    public $vk_url;
    public $telephone;
    public $comments;
    public $email;

    public function rules()
    {
        return array(
            array('telephone, name, email', 'required'),
            array('facebook_url, vk_url, telephone, name, email', 'length', 'max'=>200),
            array('email', 'match', 'pattern'=>'/^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/', 'message'=>'Введите корректный {attribute}'),
            array('comments', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('facebook_url, vk_url, telephone, comments, email', 'safe', 'on'=>'search'),
        );
    }

    public function attributeLabels()
    {
        return array(

            'facebook_url' => Yii::t('main', 'Ссылка на страницу FB '),
            'vk_url' => Yii::t('main', 'Ссылка на страницу ВК'),
            'telephone' => Yii::t('main', 'Номер телефона'),
            'comments' => Yii::t('main', 'Ваши коментарии'),
            'name' => Yii::t('main', 'Имя'),
            'email' => Yii::t('main', 'E-mail'),
        );
    }
}