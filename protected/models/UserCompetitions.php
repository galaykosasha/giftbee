<?php

/**
 * This is the model class for table "gb_user_competitions".
 *
 * The followings are the available columns in table 'gb_user_competitions':
 * @property string $id
 * @property string $competition_id
 * @property string $user_social_id
 * @property string $social_id
 * @property string $user_id
 * @property string $participation_date
 * @property integer $number
 */
class UserCompetitions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gb_user_competitions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('competition_id, user_social_id, social_id, user_id', 'required'),
			array('number', 'numerical', 'integerOnly'=>true),
			array('competition_id, social_id, user_id', 'length', 'max'=>11),
			array('user_social_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, competition_id, user_social_id, social_id, user_id, number, participation_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'user'=>array(self::BELONGS_TO, 'Users', 'user_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'competition_id' => Yii::t('main', 'Competition'),
			'user_social_id' => Yii::t('main', 'User Social'),
			'social_id' => Yii::t('main', 'Social'),
			'user_id' => Yii::t('main', 'User'),
			'number' => Yii::t('main', 'Number'),
			'participation_date' => Yii::t('main', 'Participation Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('competition_id',$this->competition_id,true);
		$criteria->compare('user_social_id',$this->user_social_id,true);
		$criteria->compare('social_id',$this->social_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('number',$this->number);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserCompetitions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->number = $this->recursionCheck();
            return true;
        }
        else
            return false;
    }

    private function recursionCheck()
    {
        $count = $this->count('competition_id = :c_id', array(':c_id'=>$this->competition_id));
        $check = $this->find('competition_id = :c_id AND number = :numb', array(':c_id'=>$this->competition_id, ':numb'=>$count+1));
        if(!$check)
            return $count+1;
        else
            $this->recursionCheck();
    }

    public static function countMembers()
    {
        return self::model()->count();
    }
}
