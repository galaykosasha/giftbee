<?php

/**
 * This is the model class for table "gb_users_buffer".
 *
 * The followings are the available columns in table 'gb_users_buffer':
 * @property string $id
 * @property integer $social_id
 * @property string $social_user_id
 * @property string $token
 * @property string $email
 * @property string $telephone
 * @property string $sex
 * @property string $image
 * @property integer $city_id
 * @property string $date
 * @property string $approve_code
 * @property string $birth_date
 */
class UsersBuffer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gb_users_buffer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('social_id, social_user_id, token', 'required'),
            //array('telephone', 'match', 'pattern'=>'/^\+\d{2}\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/', 'message'=>'Введите корректный номер {attribute}'),
            array('email', 'match', 'pattern'=>'/^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/', 'message'=>'Введите корректный {attribute}'),
            //array('birth_date', 'match', 'pattern'=>'/^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d$/', 'message'=>'Введите корректную дату'),
			array('social_id, city_id', 'numerical', 'integerOnly'=>true),
			array('email', 'required', 'on'=>'addToBuffer'),
			array('social_user_id, token, email, telephone, sex, image', 'length', 'max'=>255),
			array('approve_code', 'length', 'max'=>100),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, social_id, social_user_id, token, email, telephone, sex, image, city_id, date, approve_code, birth_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'social_id' => Yii::t('main', 'Social'),
			'social_user_id' => Yii::t('main', 'Social User'),
			'token' => Yii::t('main', 'Token'),
			'email' => Yii::t('main', 'Email'),
			'telephone' => Yii::t('main', 'Telephone'),
			'sex' => Yii::t('main', 'Sex'),
			'image' => Yii::t('main', 'Image'),
			'city_id' => Yii::t('main', 'City'),
			'date' => Yii::t('main', 'Date'),
			'approve_code' => Yii::t('main', 'Approve Code'),
			'birth_date' => Yii::t('main', 'День рождения'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('social_id',$this->social_id);
		$criteria->compare('social_user_id',$this->social_user_id,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('approve_code',$this->approve_code,true);
		$criteria->compare('birth_date',$this->birth_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersBuffer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getCitiesList()
    {
        $model = City::model()->findAll(array('order'=>'name ASC'));
        $resArray = array();
        foreach($model as $key => $value) {
            $resArray[$value->city_id] = $value->name.' ('.$value->region->name.')';
        }
        return $resArray;
    }
}
