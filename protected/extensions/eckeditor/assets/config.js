/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.forcePasteAsPlainText = true;
    config.removeFormatTags = 'b,big,code,del,dfn,em,font,i,ins,kbd,div';
    config.protectedSource.push(/<(style)[^>]*>.*<\/style>/ig);
    config.protectedSource.push(/aria-labelledby\=\"[\w\W]+\"/ig);
    config.protectedSource.push(/<(script)[^>]*>.*<\/script>/ig);// разрешить теги <script>
    config.protectedSource.push(/<(i)[^>]*>.*<\/i>/ig);// разрешить теги <i>
    config.protectedSource.push(/<\?[\s\S]*?\?>/g);// разрешить php-код
    config.protectedSource.push(/<!--dev-->[\s\S]*<!--\/dev-->/g);
    config.allowedContent = true; /* all tags */
};
