<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Giftbee.ua - Конкурсы, розыгрыши в социальных сетях, подарки бесплатно!',

	// preloading 'log' component
    'language'=>'ru',
    'preload' => array(
        'debug',
        'log',
    ),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'profile',
		'control',
	),

	// application components
	'components'=>array(
        'debug' => array(
            'class' => 'ext.yii2-debug.Yii2Debug',
            'enabled' => YII_DEBUG,
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            'class'=>'WebUser',
            'loginUrl'=>null,
		),
		'authManager' => array(
            // Будем использовать свой менеджер авторизации
            'class' => 'PhpAuthManager',
            // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
            'defaultRoles' => array('guest'),
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '/'=>'site/index',
                'settings'=>'profile/default/settings',
                'ajax/<action:\w+>'=>'ajax/<action>',
                'mail/<action:\w+>'=>'mail/<action>',
                '<company:[0-9a-zA-Z\-]+>/<competition_url:[0-9a-zA-Z\-]+>'=>'site/competition',
                '<action:\w+>'=>'site/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
            ),
        ),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CProfileLogRoute',
                    'levels'=>'error, warning',
                ),
            ),
        ),
        'session' => array(
            'class' => 'CCacheHttpSession'
        ),
        'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),
        'clientScript' => array(
            'coreScriptPosition'=>CClientScript::POS_END,
            'packages'=>array(
                'jquery'=>array(
                    'js'=>array(YII_DEBUG ? 'jquery.js' : 'jquery.min.js'),
                ),
            )
        ),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
