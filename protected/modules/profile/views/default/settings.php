<?php
   /**
    * Created by PhpStorm.
    * User: Lee
    * Date: 26.06.2015
    * Time: 12:16
    * @var $this DefaultController
    * @var $user Users
    * @var $form CActiveForm
    */
$this->pageTitle = 'Giftbee: Личный профиль';
   ?>
<!-- BANNER -->
<div class="container-fluid profile pages-top-mrg">
   <div class="row">
   <!-- COMP LOGO -->
   <div class="col-md-12 top_bar-logo">
     <a href="/" ><img src="<?= Yii::app()->createUrl('/images'); ?>/logo-top_bar.png" alt=""></a>
   </div>
   <!-- END COMP LOGO -->
      <div class="col-md-8 col-md-offset-2 text-center setting-page-dsc p0">
         <h1>Личные данные</h1>
         <span>Мы храним Ваши личные данные исключительно для связи с Вами в случаи выигрыша.<br>Так же они позволяют подбирать для Вас самые интересные конкурсы.</span>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
         <div class="user-info-board clearfix">
         <div class="row">
            <?php $form = $this->beginWidget('CActiveForm', array(
               'id'=>'user-form',
               'enableAjaxValidation'=>true,
               'enableClientValidation' => true,
               'clientOptions' => array(
                   'validateOnSubmit'=>true,
               ),
               'htmlOptions'=>array('class'=>'form-inline form-1'),
               )); ?>
            <div class="col-md-12 col-lg-6">
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-user"></i></div>
                     <?= $form->textField($user,'first_name', array('class'=>'form-control first-name', 'placeholder'=>'Имя')); ?>
                     <?= $form->error($user,'first_name', array('class'=>'danger-message')); ?>
                     <div class="message">Укажите Имя</div>
                  </div>
               </div>
            </div>
            <div class="col-md-12 col-lg-6">
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-user"></i></div>
                     <?= $form->textField($user,'last_name', array('class'=>'form-control last-name', 'placeholder'=>'Фамилия')); ?>
                     <?= $form->error($user,'last_name', array('class'=>'danger-message')); ?>
                     <div class="message">Фамилия</div>
                  </div>
               </div>
            </div>
            <div class="col-md-12 col-lg-6">
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-location-arrow"></i></div>
                     <div id="result-city" class="for-checkbox-validate">
                        <?= $form->dropDownList($user, 'city_id', $user->citiesList, array('class'=>'selectpicker form-control', 'empty'=>'Укажите город', 'data-live-search'=>true)); ?>
                     </div>
                     <?= $form->error($user,'city_id', array('class'=>'danger-message')); ?>
                     <div class="message">Город</div>
                  </div>
               </div>
            </div>
            <div class="col-md-12 col-lg-6">
               <div class="form-group profile-form-date">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                     <?= $form->telField($user,'birth_date', array('class'=>'form-control date-field', 'placeholder'=>'__-__-____')); ?>
                     <?= $form->error($user,'birth_date', array('class'=>'danger-message')); ?>
                     <div class="message">Дата рождения</div>
                  </div>
               </div>
               <div class="form-group profile-form-gander">
                  <div class="input-group">
                     <label class="radio-inline">
                     <i class="fa fa-male"></i>
                     <?= $form->radioButton($user, 'sex', array('id'=>'optionsRadios1', 'value'=>1, 'uncheckValue'=>null)); ?>
                     <span>М</span>
                     <i class="fa fa-square-o"></i>
                     <i class="fa fa-check-square-o"></i>
                     </label>
                     <label class="radio-inline">
                     <i class="fa fa-female"></i>
                     <?= $form->radioButton($user, 'sex', array('id'=>'optionsRadios2', 'value'=>0, 'uncheckValue'=>null)); ?>
                     <span>Ж</span>
                     <i class="fa fa-square-o"></i>
                     <i class="fa fa-check-square-o"></i>
                     </label>
                     <div class="clearfix"></div>
                     <?= $form->error($user, 'sex', array('class'=>'danger-message')); ?>
                     <div class="message">Пол</div>
                  </div>
               </div>
            </div>
            <div class="col-md-12 col-lg-6">
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                     <?= $form->emailField($user,'email', array('class'=>'form-control', 'pattern' => "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$", 'placeholder'=>'E-mail')); ?>
                     <?= $form->error($user,'email', array('class'=>'danger-message')); ?>
                     <div class="message">Е-mail</div>
                  </div>
               </div>
            </div>
            <div class="col-md-12 col-lg-6">
               <div class="checkbox">
                  <label >
                  <?= $form->checkBox($user, 'mailing', array('class'=>'form-control pull-left')); ?>
                  <i class="fa fa-square-o pull-left"></i>
                  <i class="fa fa-check-square-o pull-left"></i>
                  <span class="checkbox-dsc pull-left">Я хочу получать письма с результатми розыгрышей от GiftBee и новыми конкурсами</span>
                  </label>
                  <?= $form->error($user,'mailing', array('class'=>'danger-message')); ?>
               </div>
            </div>
            </div>


            <!-- WIDGET -->
            <div class="row">
              <div class="col-md-12 p0 text-center">
              <div class="col-md-12">
              <h2>Подпишись на новости от Giftbee.ua</h2>
              <p>Узнавай первым о новых конкурсах и результатах розыгрышей.<br>
                  Это обязательное условия для участия в конкурсах!</p>
              </div>
            </div>


               <div class="col-sm-6 col-md-6">

                  <div class="add-account">
                     <div class="add-account-header add-account-header-vk">
                        <!-- <i class="fa fa-close"></i> -->
                     </div>
                     <?php if($user->hasVk): ?>
                     <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                     <!-- VK Widget -->
                     <div id="vk_groups"></div>
                     <script type="text/javascript">
                        VK.Widgets.Group("vk_groups", {mode: 0, height: "200",width: "auto", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 96481626);
                     </script>
                     <?php else: ?>
                     <div class="add-account-plus"><a href="<?= Yii::app()->createUrl('/site/loginVk'); ?>"><i class="fa fa-plus"></i></a></div>
                     <?php endif; ?>
                  </div>

               </div>


               <div class="col-sm-6 col-md-6">

                  <div class="add-account">
                     <div class="add-account-header">
                        <!-- <i class="fa fa-check"></i> -->
                     </div>
                     <div>
                        <?php if($user->hasFb): ?>
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                           var js, fjs = d.getElementsByTagName(s)[0];
                           if (d.getElementById(id)) return;
                           js = d.createElement(s); js.id = id;
                           js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4";
                           fjs.parentNode.insertBefore(js, fjs);
                           }(document, 'script', 'facebook-jssdk'));
                        </script>
                        <div class="fb-page" data-href="https://www.facebook.com/giftbeeua" data-width="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
                           <div class="fb-xfbml-parse-ignore">
                              <blockquote cite="https://www.facebook.com/giftbeeua"><a href="https://www.facebook.com/giftbeeua">giftbee.ua</a></blockquote>
                           </div>
                        </div>
                     </div>
                     <?php else: ?>
                     <div class="add-account-plus">
                        <a href="<?= Yii::app()->createUrl('/site/loginFb'); ?>"><i class="fa fa-plus"></i></a>
                     </div>
                     <?php endif; ?>
                  </div>
                  
               </div>

            </div>
            <!-- END WIDGET -->

              <div class="row">
                <div class="col-xs-12 col-md-12">
                  <?= CHtml::tag('button', array('class'=>'btn btn-green submit-btn'), 'Сохранить'); ?>
                </div>
              </div>

            <?php $this->endWidget(); ?>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
</div>
<!-- end full-width-banner -->
<!-- <div class="clearfix"></div> -->
<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?84"></script>

<?php Yii::app()->clientScript->registerScript('autoComplete', '
   jQuery( document ).ready(function() {
        jQuery(".submit-btn").on("click", function(){
            $("html,body").animate({scrollTop: $("#user-form").offset().top}, 1000);
        });


    // GROUPS IN/OUT
    VK.init({
      apiId: 4958908
    });


      VK.api("groups.isMember", { group_id:"96481626", user_id:"44223049" }, function(data) {
        if(data.response) {
          jQuery(".add-account-header-vk .fa").removeClass("fa-close").addClass("fa-check");
        } else {
          jQuery(".add-account-header-vk .fa").removeClass("fa-check").addClass("fa-close");
        }
      });
   



    // В группе
    VK.Observer.subscribe("widgets.groups.joined", function f()
    {
      VK.api("groups.isMember", { group_id:"96481626", user_id:"44223049" }, function(data) {
        jQuery(".add-account-header-vk .fa").removeClass("fa-close").addClass("fa-check");
        console.log(data.response);
      });
    });

    // Не в группе
    VK.Observer.subscribe("widgets.groups.leaved", function f()
    {
      VK.api("groups.isMember", { group_id:"96481626", user_id:"44223049" }, function hehe (data) {
        jQuery(".add-account-header-vk .fa").removeClass("fa-check").addClass("fa-close");
        console.log(data.response);
      });
    });






       /*jQuery("#Users_country_id").on("change", function(){
           jQuery.ajax({
               url: "'.Yii::app()->createAbsoluteUrl('/profile/default/regionSelector').'",
               method : "POST",
               data: {country_id: jQuery("#Users_country_id").val()},
               success: function(html){
                   jQuery("#result-region").html(html);
                   changeRegion();
               }
           })
       });
   
       function changeRegion()
       {
           jQuery("#Users_region_id").on("change", function(){
               jQuery.ajax({
                   url: "'.Yii::app()->createAbsoluteUrl('/profile/default/citySelector').'",
                   method : "POST",
                   data: {region_id: jQuery("#Users_region_id").val()},
                   success: function(html){
                       jQuery("#result-city").html(html);
                   }
               })
           });
       }
       changeRegion();*/
   });
   
   ', CClientScript::POS_END); ?>