<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionSettings()
    {
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $user->scenario = 'settings';

        if(isset($_POST['ajax'])) {
            if ($_POST['ajax']=='user-form') {
                echo CActiveForm::validate($user);
            }
            Yii::app()->end();
        }
        if(isset($_POST['Users'])){
            $user->attributes = $_POST['Users'];
            $user->first_name = CHtml::encode(strip_tags($_POST['Users']['first_name']));
            $user->last_name = CHtml::encode(strip_tags($_POST['Users']['last_name']));
            if($user->save()) {
                Yii::app()->user->setFlash('success', 'Данные успешно сохранены!<br>'.CHtml::link('Вернуться на главную страницу', array('/site/index')));
                $this->refresh();
            } else {
                Yii::app()->user->setFlash('error', Yii::t('main', 'Ошибка сохранения данных!'));
            }
        }

        $this->render('settings', array('user'=>$user));
    }

    public function actionRegionSelector()
    {
        $country = Yii::app()->getRequest()->getParam('country_id');

        if(Yii::app()->request->isAjaxRequest) {
            if(!$country) {
                echo CHtml::dropDownList('Users[region_id]', '', array(), array('empty'=>'Укажите область', 'disabled'=>true, 'class'=>'selectpicker form-control'));
            } else {
                $regions = Region::model()->findAll(array('condition'=>"country_id = :country", 'params'=>array(':country'=>$country)));
                echo CHtml::dropDownList('Users[region_id]', null, CHtml::listData($regions, 'region_id', 'name'), array('empty'=>'Укажите область', 'class'=>'selectpicker form-control'));
            }
            Yii::app()->end();
        }
    }

    public function actionCitySelector()
    {
        $region = Yii::app()->getRequest()->getParam('region_id');

        if(Yii::app()->request->isAjaxRequest) {
            if(!$region) {
                echo CHtml::dropDownList('Users[city_id]', '', array(), array('empty'=>'Укажите город', 'disabled'=>true, 'class'=>'selectpicker form-control'));
            } else {
                $cities = City::model()->findAll(array('condition'=>"region_id = :region", 'params'=>array(':region'=>$region)));
                echo CHtml::dropDownList('Users[city_id]', null, CHtml::listData($cities, 'city_id', 'name'), array('empty'=>'Укажите город', 'class'=>'selectpicker form-control'));
            }
            Yii::app()->end();
        }
    }
}