<?php

class CompetitionsController extends AdminController
{
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Competitions;
		if(isset($_POST['Competitions']))
		{
			$model->attributes=$_POST['Competitions'];
            if($model->save()) {
                if(isset($_POST['images']) && !empty($_POST['images']))
                {
                    foreach($_POST['images'] as $image)
                    {
                        $imagesModel = Images::model()->find('image = :image', array(':image'=>$image));
                        $imagesModel->competition_id = $model->id;
                        $imagesModel->save();
                    }
                }
                Yii::app()->user->setFlash('success', Yii::t('main', 'Данные успешно сохранены!'));
                $this->redirect(array('update','id'=>$model->id));
            } else {
                Yii::app()->user->setFlash('error', Yii::t('main', 'Ошибка сохранения данных!'));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		if(isset($_POST['Competitions']))
		{
			$model->attributes=$_POST['Competitions'];
            if($model->save()) {
                if(isset($_POST['images']) && !empty($_POST['images']))
                {
                    foreach($_POST['images'] as $image)
                    {
                        $imagesModel = Images::model()->find('image = :image', array(':image'=>$image));
                        $imagesModel->competition_id = $model->id;
                        $imagesModel->save();
                    }
                }
                Yii::app()->user->setFlash('success', Yii::t('main', 'Данные успешно сохранены!'));
                $this->refresh();
            } else {
                Yii::app()->user->setFlash('error', Yii::t('main', 'Ошибка сохранения данных!'));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new Competitions('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Competitions']))
            $model->attributes=$_GET['Competitions'];

        $this->render('index',array(
        'model'=>$model,
        ));
	}

    public function actionArchive()
    {
        $model=new Competitions('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Competitions']))
            $model->attributes=$_GET['Competitions'];

        $this->render('archive',array(
            'model'=>$model,
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Competitions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Competitions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Competitions $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='competitions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionRegionSelector()
    {
        $country = Yii::app()->getRequest()->getParam('country_id');

        if(Yii::app()->request->isAjaxRequest) {
            if(!$country) {
                echo CHtml::dropDownList('Competitions[region_id]', '', array(), array('empty'=>'Укажите область', 'disabled'=>true, 'class'=>'form-control'));
            } else {
                $regions = Region::model()->findAll(array('condition'=>"country_id = :country", 'params'=>array(':country'=>$country)));
                echo CHtml::dropDownList('Competitions[region_id]', null, CHtml::listData($regions, 'region_id', 'name'), array('empty'=>'Укажите область', 'class'=>'form-control'));
            }
            Yii::app()->end();
        }
    }

    public function actionCitySelector()
    {
        $region = Yii::app()->getRequest()->getParam('region_id');

        if(Yii::app()->request->isAjaxRequest) {
            if(!$region) {
                echo CHtml::dropDownList('Competitions[city_id]', '', array(), array('empty'=>'Укажите город', 'disabled'=>true, 'class'=>'form-control'));
            } else {
                $cities = City::model()->findAll(array('condition'=>"region_id = :region", 'params'=>array(':region'=>$region)));
                echo CHtml::dropDownList('Competitions[city_id]', null, CHtml::listData($cities, 'city_id', 'name'), array('empty'=>'Укажите город', 'class'=>'form-control'));
            }
            Yii::app()->end();
        }
    }

    public function actionUpload($size)
    {
        $uniq = uniqid('image_');
        if($size == 'xs') {
            $image = CUploadedFile::getInstanceByName('image');
            $image->saveAs(Yii::getPathOfAlias('webroot.uploads.competitions.xs-images').DIRECTORY_SEPARATOR.$uniq.$image->name);

            $model = new Competitions();
            echo CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/'.$size.'-images/'.$uniq.$image->name);
            echo CHtml::activeHiddenField($model, $size.'_image', array('value'=>$uniq.$image->name));
            Yii::app()->end();
        } elseif($size == 'md') {
            $image = CUploadedFile::getInstanceByName('image');
            $image->saveAs(Yii::getPathOfAlias('webroot.uploads.competitions.md-images').DIRECTORY_SEPARATOR.$uniq.$image->name);

            $model = new Competitions();
            echo CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/'.$size.'-images/'.$uniq.$image->name);
            echo CHtml::activeHiddenField($model, $size.'_image', array('value'=>$uniq.$image->name));
            Yii::app()->end();
        }
        $image = CUploadedFile::getInstanceByName('image');

        $image->saveAs(Yii::getPathOfAlias('webroot.uploads.competitions.temporary').DIRECTORY_SEPARATOR.$uniq.$image->name);
        list($resource['width'], $resource['height']) = getimagesize(Yii::getPathOfAlias('webroot.uploads.competitions.temporary').DIRECTORY_SEPARATOR.$uniq.$image->name);

        echo CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/temporary/'.$uniq.$image->name, '', array('id'=>$size.'-crop'));
        echo CHtml::hiddenField('nameFull', $uniq.$image->name);
        echo CHtml::hiddenField('src_w', $resource['width']);
        echo CHtml::hiddenField('src_h', $resource['height']);
        Yii::app()->end();
    }

    public function actionCrop($size)
    {
        $name = $_POST['nameFull'];
        $fullImagePath = Yii::getPathOfAlias('webroot.uploads.competitions.temporary').DIRECTORY_SEPARATOR.$name;
        $quality = 100;

        $fullImg = getimagesize($fullImagePath);
        switch(strtolower($fullImg['mime']))
        {
            case 'image/png':
                $source_image = imagecreatefrompng($fullImagePath);
                $type = 'png';
                $quality = 0;
                break;
            case 'image/jpeg':
                $source_image = imagecreatefromjpeg($fullImagePath);
                $type = 'jpeg';
                break;
            case 'image/gif':
                $source_image = imagecreatefromgif($fullImagePath);
                $type = 'gif';
                break;
            default: die('image type not supported');
        }
        $function = 'image'.$type;

        if($size == 'xs') {
            $resizeImage = imagecreatetruecolor(640, 960); //$_POST['h'], $_POST['h']
        } else {
            $resizeImage = imagecreatetruecolor(900, 300); //$_POST['h'], $_POST['h']
        }

        $src_x = $_POST['x'];
        $src_y = $_POST['y'];
        $dst_y = 0;
        $dst_x = 0;
        $dst_w = $_POST['w'];
        $dst_h = $_POST['h'];


        if($size == 'xs') {
            imagecopyresampled($resizeImage,$source_image, 0, 0, $src_x, $src_y,
                640, 960, $dst_w, $dst_h);
        } else {
            imagecopyresampled($resizeImage,$source_image, 0, 0, $src_x, $src_y,
                900, 300, $dst_w, $dst_h);
        }

        $function($resizeImage, Yii::getPathOfAlias('webroot.uploads.competitions').'/'.$size.'-images'.'/'.$name, $quality);
        $model = new Competitions();
        echo CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/'.$size.'-images/'.$name);
        echo CHtml::activeHiddenField($model, $size.'_image', array('value'=>$name));
        Yii::app()->end();
    }

    public function actionMassUpload()
    {
        $uniq = uniqid('image_');
        $image = CUploadedFile::getInstanceByName('image');
        $imagesModel = new Images();
        $image->saveAs(Yii::getPathOfAlias('webroot.uploads.competitions.extra').DIRECTORY_SEPARATOR.$uniq.$image->name);
        $imagesModel->image = $uniq.$image->name;
        if($imagesModel->save()) {
            echo CHtml::openTag('div', array('class'=>'min-image-container'));
            echo CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/extra/'.$imagesModel->image, '');
            echo CHtml::hiddenField('images['.$imagesModel->image.']', $imagesModel->image);
            echo CHtml::closeTag('div');
            Yii::app()->end();
        }
    }
}
