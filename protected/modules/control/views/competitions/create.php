<?php
/* @var $this CompetitionsController */
/* @var $model Competitions */
$this->actionHeader = Yii::t('main', 'Добавление').' '.'Competitions';
$this->breadcrumbs=array(
	'Competitions'=>array('index'),
	Yii::t('main', 'Добавление'),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>