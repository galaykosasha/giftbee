<?php
/* @var $this CompetitionsController */
/* @var $model Competitions */
$this->actionHeader = Yii::t('main', 'Управление').' '.'Competitions';
$this->breadcrumbs=array(
	'Competitions'=>array('index'),
	Yii::t('main', 'Управление'),
);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h5 class="box-title">
                    Competitions
                </h5>
                <div class="button_save">
                    <?= CHtml::link('<i class="fa fa-plus"></i>'.Yii::t('main', 'Добавить'), array('/control/competitions/create'), array('class'=>'pull-right btn btn-info btn-flat')); ?>
                </div>
            </div>
            <div class="box-body">
                <?php $this->widget('application.modules.control.components.widgets.overWrite.AdminGridView', array(
                'id'=>'competitions-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
                    'id',
                    'xs_image'=>array(
                        'type'=>'image',
                        'name'=>'xs_image',
                        'value'=>'Yii::app()->baseUrl."/uploads/competitions/xs-images/".$data->xs_image',
                    ),
                    'company_id'=>array(
                        'name'=>'company_id',
                        'value'=>'$data->company->name_ru',
                    ),
                    'title_ru',
                    /*
                    'short_description_ru',
                    'description_ru',
                    'date_start',
                    'date_end',
                    'max_users_count',
                    'presents_count',
                    'country_id',
                    'region_id',
                    'city_id',
                    */

                array(
                    'class'=>'CButtonColumn',
                    'htmlOptions' => array('style'=>'text-align: center; width: 80px;'),
                    'template'=>'{update}&nbsp;{delete}',
                    'buttons' => array(
                        'update' => array(
                            'imageUrl'=>$this->assetsPath.'/images/edit.png',
                        ),
                        'delete' => array(
                            'imageUrl'=>$this->assetsPath.'/images/delete.png',
                        ),
                    ),
                ),
                ),
                )); ?>
            </div>
        </div>
    </div>
</div>