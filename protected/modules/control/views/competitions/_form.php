<?php
/* @var $this CompetitionsController */
/* @var $model Competitions */
/* @var $form CActiveForm */
$newImageName = uniqid();
Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/plugins/jCrop/jquery.Jcrop.js',
    CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/plugins/tinymce/js/tinymce/tinymce.js',
    CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/plugins/jCrop/jquery.Jcrop.css');
Yii::app()->clientScript->registerCss('image','
#result-xs img {
    height: 100%;
    width: auto;
}
');
?>
<div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'competitions-form',
                'enableAjaxValidation'=>false,
            )); ?>
    <div class="col-xs-12">
        <!-- Flash message -->
         <?php $this->beginWidget('application.modules.control.components.widgets.FlashWidget',array(
            'params'=>array(
                'model' => $model,
                'form' => null,
            )));
        $this->endWidget(); ?>
        <!-- End Flash message -->
    </div>


    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Изображения'); ?>
                </h3>
            </div>
            <div class="box-body upload-images-container">
                <div id="result-xs" class="col-xs-5 col-md-5 upload-img_item" data-toggle="modal" data-target="#myModal-640">
                    <?= $model->xs_image ? CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/xs-images/'.$model->xs_image, '') : CHtml::image(Yii::app()->baseUrl.'/images/', ''); ?>
                </div>
                <div id="result-md" class="col-xs-7 col-md-7 upload-img_item" data-toggle="modal" data-target="#myModal-900">
                    <?= $model->xs_image ? CHtml::image(Yii::app()->baseUrl.'/uploads/competitions/md-images/'.$model->md_image, '') : CHtml::image(Yii::app()->baseUrl.'/images/', ''); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Основные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <?= $form->labelEx($model,'is_active'); ?>
                    <?= $form->dropDownList($model, 'is_active', array(0=>'Нет', 1=>'Да'), array('class'=>'form-control')); ?>
                    <?= $form->error($model,'is_active'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'type'); ?>
                    <?= $form->dropDownList($model, 'type', array(1=>'Конкурс', 2=>'Скидка'), array('class'=>'form-control')); ?>
                    <?= $form->error($model,'type'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'company_id'); ?>
                    <?= $form->dropDownList($model, 'company_id', CHtml::listData(Companies::model()->findAll(), 'id', 'name_ru'), array('class'=>'form-control', 'empty'=>'Выберите компанию')); ?>
                    <?= $form->error($model,'company_id'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'title_ru'); ?>
                    <?= $form->textField($model,'title_ru',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'title_ru'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'url'); ?>
                    <?= $form->textField($model,'url',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'url'); ?>
                </div>


                <!--<div class="form-group">
                    <?/*= $form->labelEx($model,'short_description_ru'); */?>
                    <?/*= $form->textField($model,'short_description_ru',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); */?>
                    <?/*= $form->error($model,'short_description_ru'); */?>
                </div>-->


                <div class="form-group">
                    <?= $form->labelEx($model,'description_ru'); ?>
                    <?php /* $this->widget('application.extensions.eckeditor.ECKEditor', array(
                        'model'=>$model,
                        'attribute'=>'description_ru',
                        'config'=>array(
                            'filebrowserUploadUrl' => $this->createUrl('/admin/upload/index/'),//Конфиг вставлять сюда
                        ),
                    ));
                    */?>
					<?= $form->textArea($model,'description_ru'); ?>
                    <?= $form->error($model,'description_ru'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'group_vk'); ?>
                    <span id="vk-group-name"></span>
                    <div class="input-group">
                        <?= $form->textField($model,'group_vk',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
                        <span class="input-group-addon">
                            <i class="fa fa-check" id="vk-check"></i>
                        </span>
                    </div>
                    <?= $form->error($model,'group_vk'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'post_url_vk'); ?>
                    <?= $form->textField($model,'post_url_vk',array('size'=>60, 'maxlength'=>150, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'post_url_vk'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'group_fb'); ?>
                    <span id="fb-group-name"></span>
                    <div class="input-group">
                        <?= $form->textField($model,'group_fb',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
                        <span class="input-group-addon">
                            <i class="fa fa-check" id="fb-check"></i>
                        </span>
                    </div>
                    <?= $form->error($model,'group_fb'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'post_url_fb'); ?>
                    <?= $form->textField($model,'post_url_fb',array('size'=>60, 'maxlength'=>150, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'post_url_fb'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'competition_url'); ?>
                    <?= $form->textField($model,'competition_url',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'competition_url'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'youtube_url'); ?>
                    <?= $form->textField($model,'youtube_url',array('class'=>'form-control')); ?>
                    <?= $form->error($model,'youtube_url'); ?>
                </div>
            </div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Добавить') : Yii::t('main', 'Сохранить'), array('class'=>'btn btn-primary')); ?>
                <?php if(!empty($model->winners)): ?>
                    <div class="btn btn-default" id="sendTestMailAfterCompetition">Отправить тестовые письма</div>
                    <div class="btn btn-default" id="sendMailAfterCompetition">Отправить письма</div>
                <?php endif; ?>
            </div>

        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Дополнительные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <?= $form->labelEx($model,'date_start'); ?>
                    <?php $this->widget('application.modules.control.components.widgets.timepicker.timepicker', array(
                        'model'=>$model,
                        'name'=>'date_start',
                        'options' => array(
                            'showOn'=>'focus',
                            'timeFormat'=>'hh:mm:ss',
                            'showSecond'=>true,
                            'hourMin' => 0,
                            'hourMax' => 24,
                            'minuteMax'=>60
                        ),
                    )); ?>
                    <?= $form->error($model,'date_start'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($model,'date_end'); ?>
                    <?php $this->widget('application.modules.control.components.widgets.timepicker.timepicker', array(
                        'model'=>$model,
                        'name'=>'date_end',
                        'options' => array(
                            'showOn'=>'focus',
                            'timeFormat'=>'hh:mm:ss',
                            'showSecond'=>true,
                            'hourMin' => 0,
                            'hourMax' => 24,
                            'minuteMax'=>60
                        ),
                    )); ?>
                    <?= $form->error($model,'date_end'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($model,'site_url'); ?>
                    <?= $form->textField($model, 'site_url', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'site_url'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'max_users_count'); ?>
                    <?= $form->textField($model, 'max_users_count', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'max_users_count'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'presents_count'); ?>
                    <?= $form->textField($model,'presents_count',array('size'=>11, 'maxlength'=>11, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'presents_count'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'winners'); ?>
                    <?= $form->textField($model,'winners',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'winners'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'video'); ?>
                    <?= $form->textField($model,'video',array('size'=>11, 'maxlength'=>11, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'video'); ?>
                </div>

                <?php if(!$model->isNewRecord): ?>
                    <div class="form-group">
                        <span>Ссылка на акцию</span>
                        <div><?= CHtml::link(Yii::app()->createAbsoluteUrl('/site/competition', array('company'=>$model->company->url, 'competition_url'=>$model->url)),array('/site/competition', 'company'=>$model->company->url, 'competition_url'=>$model->url), array('target'=>'_blank')); ?></div>
                    </div>
                <?php endif; ?>

                <!--<div class="form-group">
                    <?/*= $form->labelEx($model,'sale_type'); */?>
                    <?/*= $form->dropDownList($model,'sale_type', array(1=>'Одноразовая', 2=>'Постоянная'), array('class'=>'form-control', 'empty'=>'Тип скидки')); */?>
                    <?/*= $form->error($model,'sale_type'); */?>
                </div>

                <div class="form-group">
                    <?/*= $form->labelEx($model,'sale'); */?>
                    <?/*= $form->textField($model,'sale',array('size'=>11, 'class'=>'form-control')); */?>
                    <?/*= $form->error($model,'sale'); */?>
                </div>

                <div class="form-group">
                    <?/*= $form->labelEx($model,'sale_description'); */?>
                    <?/*= $form->textArea($model,'sale_description',array('class'=>'form-control')); */?>
                    <?/*= $form->error($model,'sale_description'); */?>
                </div>-->

                <div class="form-group">
                    <?= $form->labelEx($model,'winner_form_url'); ?>
                    <?= $form->textField($model,'winner_form_url',array('class'=>'form-control')); ?>
                    <?= $form->error($model,'winner_form_url'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'mail_text'); ?>
                    <?= $form->textArea($model,'mail_text',array('class'=>'form-control')); ?>
                    <?= $form->error($model,'mail_text'); ?>
                </div>



                <!--<div class="form-group">
                    <?/*= $form->labelEx($model,'country_id'); */?>
                    <?/*= $form->dropDownList($model, 'country_id',CHtml::listData(Country::model()->findAll(array('order'=>'name ASC')), 'country_id', 'name'), array('class'=>'form-control', 'empty'=>'Укажите страну')); */?>
                    <?/*= $form->error($model,'country_id'); */?>
                </div>


                <div class="form-group">
                    <?/*= $form->labelEx($model,'region_id'); */?>
                    <div id="result-region">
                        <?/*= $form->dropDownList($model, 'region_id', CHtml::listData(Region::model()->findAll('country_id = :c_id', array(':c_id'=>$model->country_id)), 'region_id', 'name'), array('class'=>'form-control')); */?>
                    </div>
                    <?/*= $form->error($model,'region_id'); */?>
                </div>


                <div class="form-group">
                    <?/*= $form->labelEx($model,'city_id'); */?>
                    <div id="result-city">
                        <?/*= $form->dropDownList($model, 'city_id', CHtml::listData(City::model()->findAll('region_id = :r_id', array(':r_id'=>$model->region_id)), 'city_id', 'name'), array('class'=>'form-control')); */?>
                    </div>
                    <?/*= $form->error($model,'city_id'); */?>
                </div>-->
            </div>
        </div>

        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Изображения'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div class="admin-images-product">
                    <div id="images-extra">
                        <?php if(isset($model->images)): ?>
                            <?php foreach($model->images as $image): ?>
                                <div class="min-image-container">
                                    <?= Chtml::image(Yii::app()->baseUrl.'/uploads/competitions/extra/'.$image->image, $image->image); ?>
                                    <?= Chtml::hiddenField('images['.$image->image.']', $image->image); ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="btn btn-info btn-block btn-flat" data-toggle="modal" data-target="#photoModal"><?= Yii::t('main', 'Дабавить фото'); ?></div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<!-- Modal -->
<div class="modal fade" id="photoModal" tabindex="-1" role="dialog" aria-labelledby="photoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="photoModalLabel">Добавление фото</h4>
            </div>
            <?php $form = $this->beginWidget('CActiveForm', array('id'=>'image-form', 'method'=>'POST', 'htmlOptions'=>array('enctype'=>'multipart/form-data'))); ?>
            <div class="modal-body">
                <?= Chtml::fileField('image'); ?>
            </div>
            <div class="modal-footer">
                <?= CHtml::submitButton('Загрузить', array('class'=>'btn btn-info btn-block btn-flat')); ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal-640" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Загрузка и обрезка фото</h4>
            </div>
            <div class="modal-body">

                <form id="xs-image-form">
                    <label for="xs">
                        <input type="file" id="xs" name="image"/>
                        <span class="img-size">640px</span>
                    </label>
                </form>

                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'xs-image-crop',
                    'method'=>'POST',
                    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                )); ?>

                <div id="xs-done"></div>
                <input type="hidden" id="xs-x" name="x" />
                <input type="hidden" id="xs-y" name="y" />
                <input type="hidden" id="xs-w" name="w" />
                <input type="hidden" id="xs-h" name="h" />

                <?= CHtml::submitButton('Обрезать', array('class'=>'btn btn-default')); ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

<?php Yii::app()->clientScript->registerScript('xs-upload', '
    jQuery("#xs").change(function(){
      jQuery("#xs-image-form").submit();
    });

    $( document ).ready(function() {
        $("#xs-image-crop").hide();
    });

    $("#xs-image-form").submit(function(event){
        event.preventDefault();
        var data = new FormData($("#xs-image-form")[0]);
        $.ajax({
            type: "POST",
            url: "'.Yii::app()->createUrl("/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/upload", array('size'=>'xs')).'",
            data: data,
            contentType: false,
            processData: false,
            success: function(html) {
                $("#result-xs").html(html);
            }
        })
    });
', CClientScript::POS_END); ?>


<!-- Modal -->
<div class="modal fade" id="myModal-900" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Загрузка и обрезка фото</h4>
            </div>
            <div class="modal-body">

                <form id="md-image-form">
                    <label for="md">
                        <input type="file" id="md" name="image"/>
                        <span class="img-size">900px</span>
                    </label>
                </form>

                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'md-image-crop',
                    'method'=>'POST',
                    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                )); ?>

                <div id="md-done"></div>
                <input type="hidden" id="md-x" name="x" />
                <input type="hidden" id="md-y" name="y" />
                <input type="hidden" id="md-w" name="w" />
                <input type="hidden" id="md-h" name="h" />

                <?= CHtml::submitButton('Обрезать', array('class'=>'btn btn-default')); ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

<?php Yii::app()->clientScript->registerScript('md-upload', '

jQuery("#md").change(function(){
  jQuery("#md-image-form").submit();
});

$( document ).ready(function() {
    $("#md-image-crop").hide();
});

$("#md-image-form").submit(function(event){
    event.preventDefault();
    var data = new FormData($("#md-image-form")[0]);
    $.ajax({
        type: "POST",
        url: "'.Yii::app()->createUrl("/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/upload", array('size'=>'md')).'",
        data: data,
        contentType: false,
        processData: false,
        success: function(html) {
            $("#result-md").html(html);
        }
    })
});


    /*jQuery("#md").change(function(){
      jQuery("#md-image-form").submit();
    });

    $(document).ready(function() {
        $("#md-image-crop").hide();
    });

    $("#md-image-form").submit(function(event){
        event.preventDefault();
        var data = new FormData($("#md-image-form")[0]);
        $.ajax({
            type: "POST",
            url: "'.Yii::app()->createUrl("/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/upload", array('size'=>'md')).'",
            data: data,
            contentType: false,
            processData: false,
            success: function(html) {
                $("#md-image-form").hide();
                $("#md-image-crop").show();
                $("#md-done").append(html);
                $("#md-crop").load(function(){
                    $(this).Jcrop({
                        aspectRatio: 3/1,
                        boxWidth: 530,
                        boxHeight: 600,
                        onSelect: updateCoords
                    });
                });
            }
        })
    });

    function updateCoords(c)
    {
        $("#md-x").val(c.x);
        $("#md-y").val(c.y);
        $("#md-w").val(c.w);
        $("#md-h").val(c.h);
    }

    function checkCoords()
    {
        if (parseInt($("#md-w").val())) return true;
        alert("Please select a crop region then press submit.");
        return false;
    }

    $("#md-image-crop").submit(function(event){
        event.preventDefault();
        var data = new FormData($("#md-image-crop")[0]);
        $.ajax({
            type: "POST",
            url: "'.Yii::app()->createUrl("/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/crop", array('size'=>'md')).'",
            data: data,
            contentType: false,
            processData: false,
            success: function(html) {
                $("#result-md").html(html);
                $("#md-image-crop").hide();
                $("#md-image-form").show();

            }
        });
    });*/
', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerScript('autoComplete', '
jQuery( document ).ready(function() {
    jQuery("#Competitions_country_id").on("change", function(){
        jQuery.ajax({
            url: "'.Yii::app()->createAbsoluteUrl('/control/competitions/regionSelector').'",
            method : "POST",
            data: {country_id: jQuery("#Competitions_country_id").val()},
            success: function(html){
                jQuery("#result-region").html(html);
                changeRegion();
            }
        })
    });

    function changeRegion()
    {
        jQuery("#Competitions_region_id").on("change", function(){
            jQuery.ajax({
                url: "'.Yii::app()->createAbsoluteUrl('/control/competitions/citySelector').'",
                method : "POST",
                data: {region_id: jQuery("#Competitions_region_id").val()},
                success: function(html){
                    jQuery("#result-city").html(html);
                }
            })
        });
    }
    changeRegion();
});
', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerScriptFile('http://vkontakte.ru/js/api/openapi.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile('http://connect.facebook.net/en_US/all.js', CClientScript::POS_HEAD); ?>

<?php Yii::app()->clientScript->registerScript('vkApi', '
VK.init({
    apiId: 4958908
});
function replaceGroupVk(group){
    VK.Api.call("groups.getById", {group_id: group, fields: ""}, function(r) {
        jQuery("#Competitions_group_vk").val(r.response[0].gid);
        jQuery("#vk-group-name").html(r.response[0].name);
    });
}
jQuery("#vk-check").on("click", function(){
    replaceGroupVk(jQuery("#Competitions_group_vk").val());
});
', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerScript('fb', '
FB.init(
{
    appId : "1609866009290719"
});
function replaceGroupFb(group){
    FB.api(
        "/v2.4/"+group,
        function (response) {
            if (response && !response.error) {
                jQuery("#Competitions_group_fb").val(response.id);
                jQuery("#fb-group-name").html(response.name);
            }
        }
    );
}
jQuery("#fb-check").on("click", function(){
    replaceGroupFb(jQuery("#Competitions_group_fb").val());
});
', CClientScript::POS_END); ?>

<div id="fb-root"></div> <!-- Required by Facebook -->

<?php Yii::app()->clientScript->registerScript('mass-download', '
    $("#image-form").submit(function(event){
        event.preventDefault();
        var data = new FormData($("#image-form")[0]);
        $.ajax({
            type: "POST",
            url: "'.Yii::app()->createUrl('/'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/massUpload').'",
            data: data,
            contentType: false,
            processData: false,
            success: function(html) {
                jQuery("#images-extra").append(html);
                jQuery("#image-form")[0].reset();
            }
        }).done(function(){

        });
    });
', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScript('tinymce', '
    tinymce.init({
      selector: "textarea",
      height: 500,
      plugins: [
        "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
      ],

      valid_elements : "div[*],span[*],a[*],p[*],strong[*],em[*],i[*],h1[*],h2[*],h3[*],h4[*],ul[*],li[*]",

      toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
      toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
      toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",

      menubar: false,
      toolbar_items_size: "small",

      style_formats: [{
        title: "Bold text",
        inline: "b"
      }, {
        title: "Red text",
        inline: "span",
        styles: {
          color: "#ff0000"
        }
      }, {
        title: "Red header",
        block: "h1",
        styles: {
          color: "#ff0000"
        }
      }, {
        title: "Example 1",
        inline: "span",
        classes: "example1"
      }, {
        title: "Example 2",
        inline: "span",
        classes: "example2"
      }, {
        title: "Table styles"
      }, {
        title: "Table row 1",
        selector: "tr",
        classes: "tablerow1"
      }],

      templates: [{
        title: "Test template 1",
        content: "Test 1"
      }, {
        title: "Test template 2",
        content: "Test 2"
      }],
      content_css: "//beta.tinymce.com/css/codepen.min.css"
    });
', CClientScript::POS_END); ?>
<?php if(isset($model->id)): ?>
<?php Yii::app()->clientScript->registerScript('sendMailAfterCompetition', '
    jQuery("#sendMailAfterCompetition").on("click", function(){
    var areYouShure = confirm("Проверьте лимит почтовой рассылки на сервере перед отправкой");
    if(areYouShure) {
            $.ajax({
             url: "'.Yii::app()->createUrl('/mail/sendMailsAfterCompetition', array('id' => $model->id)).'",
             global: false,
             type: "POST",
             data: ({id : '.$model->id.'}),
             success: function (html) {
                 console.log(html);
             }
         });
    }

    });
', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerScript('sendTestMailAfterCompetition', '
    jQuery("#sendTestMailAfterCompetition").on("click", function(){

        $.ajax({
             url: "'.Yii::app()->createUrl('/mail/sendTestAfterCompetitionMails', array('id' => $model->id)).'",
             global: false,
             type: "POST",
             data: ({id : '.$model->id.'}),
             success: function (html) {
                 console.log(html);
             }
         });
    });
', CClientScript::POS_END); ?>
<?php endif; ?>
