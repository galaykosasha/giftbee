<?php
/* @var $this CompetitionsController */
/* @var $model Competitions */
$this->actionHeader = Yii::t('main', 'Редактирование').' '.'Competitions'.' '.$model->id;
$this->breadcrumbs=array(
	'Competitions'=>array('index'),
	Yii::t('main', 'Редактирование'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>