<?php
/* @var $this CompaniesController */
/* @var $model Companies */
$this->actionHeader = Yii::t('main', 'Добавление').' '.'Companies';
$this->breadcrumbs=array(
	'Companies'=>array('index'),
	Yii::t('main', 'Добавление'),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>