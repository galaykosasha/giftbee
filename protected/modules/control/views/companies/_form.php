<?php
/* @var $this CompaniesController */
/* @var $model Companies */
/* @var $form CActiveForm */
?>
<div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'companies-form',
                'enableAjaxValidation'=>false,
            )); ?>
    <div class="col-xs-12">
        <!---- Flash message ---->
         <?php $this->beginWidget('application.modules.control.components.widgets.FlashWidget',array(
            'params'=>array(
                'model' => $model,
                'form' => null,
            )));
        $this->endWidget(); ?>
        <!---- End Flash message ---->
    </div>

    <div class="col-md-6">
        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Основные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <?= $form->labelEx($model,'name_ru'); ?>
                    <?= $form->textField($model,'name_ru',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'name_ru'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'url'); ?>
                    <?= $form->textField($model,'url',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'url'); ?>
                </div>
            
                <div class="form-group">
                    <?= $form->hiddenField($model,'image'); ?>
                    <?= $form->error($model,'image'); ?>
                </div>
            </div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Добавить') : Yii::t('main', 'Сохранить'), array('class'=>'btn btn-primary')); ?>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Дополнительные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                <form id="logo-form">
                    <label for="logo-input">
                        <img src="<?= Yii::app()->baseUrl.$model->image ? '/uploads/companies/logos/'.$model->image : '/images/logo-input.jpg'; ?>" alt=""/>
                        <input type="file" id="logo-input" name="image" style="display: none"/>
                    </label>
                </form>
            </div>
        </div>
    </div>
</div>
<?php Yii::app()->clientScript->registerScript('logo-upload', '
    jQuery("#logo-input").change(function(){
      jQuery("#logo-form").submit();
    });
    $("#logo-form").submit(function(event){
        event.preventDefault();
        var data = new FormData($("#logo-form")[0]);
        $.ajax({
            type: "POST",
            url: "'.Yii::app()->createUrl("/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/upload").'",
            data: data,
            contentType: false,
            processData: false,
            success: function(json) {
                var result = jQuery.parseJSON(json);
                jQuery("#logo-form img").attr("src", result.folder + result.name);
                jQuery("#Companies_image").val(result.name);
            }
        })
    });
', CClientScript::POS_END); ?>