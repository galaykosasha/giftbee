<?php
/* @var $this CompaniesController */
/* @var $model Companies */
$this->actionHeader = Yii::t('main', 'Редактирование').' '.'Companies'.' '.$model->id;
$this->breadcrumbs=array(
	'Companies'=>array('index'),
	Yii::t('main', 'Редактирование'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>