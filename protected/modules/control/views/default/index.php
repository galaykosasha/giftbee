<?php
/* @var $this DefaultController */
Yii::app()->clientScript->registerCssFile($this->assetsPath.'/plugins/morris/morris.css');
Yii::app()->clientScript->registerScriptFile($this->assetsPath.'/plugins/morris/Raphael.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($this->assetsPath.'/plugins/morris/morris.min.js',CClientScript::POS_END);
$this->breadcrumbs=array(
	$this->module->id,
);
?>
<?php
$usersCount = Users::model()->count();
$usersBufferCount = UsersBuffer::model()->count();
$usersBufferWithMailCount = UsersBuffer::model()->count('email <> ""');
$usersBufferWithoutMailCount = UsersBuffer::model()->count('email is NULL');
$usersBufferWithoutMailCount = UsersBuffer::model()->count('email is NULL');
$usersBufferFromVKCount = UsersBuffer::model()->count('social_id = 1');
$usersBufferFromFBCount = UsersBuffer::model()->count('social_id = 2');
$sql = 'id NOT IN(SELECT DISTINCT id FROM gb_user_competitions)';
$usersNotHasCompetition = Users::model()->count('id NOT IN(SELECT DISTINCT user_id FROM gb_user_competitions)');

$activeCriteria = new CDbCriteria();
$activeCriteria->condition = 'date_start < NOW() AND date_end > NOW()';
$activeCriteria->compare('is_overflow', 0);
$activeCriteria->compare('is_active', 1);
$activeCompetitionsCount = Competitions::model()->count($activeCriteria);

$notActiveCriteria = new CDbCriteria();
$notActiveCriteria->condition = 'date_end < NOW()';
$notActiveCriteria->compare('is_overflow', 1, false, 'OR');
$notActiveCriteria->compare('is_active', 1);
$notActiveCompetitionsCount = Competitions::model()->count($notActiveCriteria);


?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Пользователи'); ?>
                </h3>
            </div>
            <div class="box-body">
                <h4>Зарегистрированных пользователей - <b><?= $usersCount; ?></b></h4>

                <h4>Пользователей в буфере - <b><?= $usersBufferCount; ?></b></h4>
                <h4>Пользователей в буфере с почтой - <b><?= $usersBufferWithMailCount; ?></b></h4>
                <h4>Пользователей в буфере без почты - <b><?= $usersBufferWithoutMailCount; ?></b></h4>

                <h4>Пользователей в буфере с ВК - <b><?= $usersBufferFromVKCount; ?></b></h4>
                <h4>Пользователей в буфере с ФБ - <b><?= $usersBufferFromFBCount; ?></b></h4>

                <h4>Пользователей которые не участвовали - <b><?= $usersNotHasCompetition; ?></b></h4>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Конкурсы'); ?>
                </h3>
            </div>
            <div class="box-body">
                <h4>Активные конкурсы - <b><?= $activeCompetitionsCount; ?></b></h4>
                <h4>Архивные конкурсы - <b><?= $notActiveCompetitionsCount; ?></b></h4>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Конкурсы'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div id="myfirstchart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
</div>
<?php Yii::app()->clientScript->registerScript('chart', '
new Morris.Line({
    element: "myfirstchart",
    data: ['.Users::userRegisterStatistic('month').'],
    xkey: "year",
    ykeys: ["value"],
    labels: ["Value"]
});
', CClientScript::POS_END); ?>