<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'users-form',
                'enableAjaxValidation'=>false,
            )); ?>
    <div class="col-xs-12">
        <!---- Flash message ---->
         <?php $this->beginWidget('application.modules.control.components.widgets.FlashWidget',array(
            'params'=>array(
                'model' => $model,
                'form' => null,
            )));
        $this->endWidget(); ?>
        <!---- End Flash message ---->
    </div>

    <div class="col-md-6">
        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Основные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                	            
                <div class="form-group">
                    <?= $form->labelEx($model,'first_name'); ?>
                    <?= $form->textField($model,'first_name',array('size'=>60, 'maxlength'=>100, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'first_name'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'last_name'); ?>
                    <?= $form->textField($model,'last_name',array('size'=>60, 'maxlength'=>100, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'last_name'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'email'); ?>
                    <?= $form->textField($model,'email',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'email'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'telephone'); ?>
                    <?= $form->textField($model,'telephone',array('size'=>50, 'maxlength'=>50, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'telephone'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'country_id'); ?>
                    <?= $form->textField($model, 'country_id', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'country_id'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'region_id'); ?>
                    <?= $form->textField($model, 'region_id', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'region_id'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'city_id'); ?>
                    <?= $form->textField($model, 'city_id', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'city_id'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'sex'); ?>
                    <?= $form->textField($model, 'sex', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'sex'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'image'); ?>
                    <?= $form->textField($model,'image',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'image'); ?>
                </div>
            
                <div class="form-group">
                    <?= $form->labelEx($model,'password'); ?>
                    <?= $form->passwordField($model,'password',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'password'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'birth_date'); ?>
                    <?= $form->textField($model,'birth_date',array('size'=>60, 'maxlength'=>100, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'birth_date'); ?>
                </div>

                            
            </div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Добавить') : Yii::t('main', 'Сохранить'), array('class'=>'btn btn-primary')); ?>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Дополнительные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <?= $form->labelEx($model,'mailing'); ?>
                    <?= $form->textField($model, 'mailing', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'mailing'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'role'); ?>
                    <?= $form->textField($model,'role',array('size'=>50, 'maxlength'=>50, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'role'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'active'); ?>
                    <?= $form->textField($model, 'active', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'active'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>