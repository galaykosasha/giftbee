<?php
/* @var $this UsersController */
/* @var $model Users */
$this->actionHeader = Yii::t('main', 'Добавление').' '.'Users';
$this->breadcrumbs=array(
	'Users'=>array('index'),
	Yii::t('main', 'Добавление'),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>