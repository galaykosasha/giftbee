<?php
/* @var $this UsersController */
/* @var $model Users */

$this->actionHeader = Yii::t('main', 'Управление').' '.'Users';
$this->breadcrumbs=array(
	'Users'=>array('index'),
	Yii::t('main', 'Управление'),
);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h5 class="box-title">
                    Users
                </h5>
                <div class="button_save">
                    <?= CHtml::link('<i class="fa fa-plus"></i>'.Yii::t('main', 'Добавить'), array('/control/users/create'), array('class'=>'pull-right btn btn-info btn-flat')); ?>
                </div>
            </div>
            <div class="box-body">
                <?php $this->widget('application.modules.control.components.widgets.overWrite.AdminGridView', array(
                'id'=>'users-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
                    'id',
                    array(
                        'name'=>'image',
                        'value'=>'$data->image',
                        'type'=>'image'
                    ),
                    'first_name',
                    'last_name',
                    'email',
                    array(
                        'header'=>'Колличество номеров',
                        'value'=>'$data->competitionsCount',
                        'sortable'=>true,
                    ),
                    array(
                        'header'=>'Дата регистрации',
                        'value'=>'$data->register_date',
                    ),
                    array(
                        'header'=>'Вконтакте',
                        'value'=>'$data->hasVk ? CHtml::link("ВК", "https://vk.com/id".$data->getSocialId(UserSocials::VK_SOCIAL)) : ""',
                        'sortable'=>true,
                        'type'=>'html',
                    ),
                    array(
                        'header'=>'Facebook',
                        'value'=>'$data->hasFb ? CHtml::link("FB", "https://fb.com/".$data->getSocialId(UserSocials::FB_SOCIAL)) : ""',
                        'sortable'=>true,
                        'type'=>'html',
                    ),
                    /*
                    'region_id',
                    'city_id',
                    'sex',
                    'mailing',
                    'image',
                    'role',
                    'password',
                    'active',
                    'birth_date',
                    */
                    array(
                        'class'=>'CButtonColumn',
                        'htmlOptions' => array('style'=>'text-align: center; width: 80px;'),
                        'template'=>'{update}&nbsp;{delete}',
                        'buttons' => array(
                            'update' => array(
                                'imageUrl'=>$this->assetsPath.'/images/edit.png',
                            ),
                            'delete' => array(
                                'imageUrl'=>$this->assetsPath.'/images/delete.png',
                            ),
                        ),
                    ),
                ),
                )); ?>
            </div>
        </div>
    </div>
</div>