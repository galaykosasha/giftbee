<?php
/* @var $this UsersController */
/* @var $model Users */
$this->actionHeader = Yii::t('main', 'Редактирование').' '.'Users'.' '.$model->id;
$this->breadcrumbs=array(
	'Users'=>array('index'),
	Yii::t('main', 'Редактирование'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>