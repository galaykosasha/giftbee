<?php
class UserIdentity extends CUserIdentity {
    // Будем хранить id.
    protected $_id;
    protected $_social_id;
    protected $_social_user_id;

    public function __construct($social_user_id, $social_id)
    {
        $this->_social_id = $social_id;
        $this->_social_user_id = $social_user_id;
    }

    public function authenticate() {
        $user = UserSocials::model()->find(
            'social_id = :social_id AND social_user_id = :social_user_id',
            array(':social_id'=>$this->_social_id, 'social_user_id'=>$this->_social_user_id)
        );
        if(($user===null)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $this->_id = $user->user_id;
            $this->setState('social_user_id', $user->social_user_id);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }
}