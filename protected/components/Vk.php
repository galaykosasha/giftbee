<?php
class Vk extends BaseApi
{
    public $social_id = 1;
    public $client_id = '4958908';
    public $client_secret = 'b9Odt764EwX4hvZbCtGI';

    protected $codeGetUrl = 'https://oauth.vk.com/authorize?';
    protected $tokenGetUrl = 'https://oauth.vk.com/access_token?';

    public function getAccessCode()
    {
        $queryString = array(
            'client_id'=>$this->client_id,
            'scope'=>'offline,wall',
            'redirect_uri'=> Yii::app()->createAbsoluteUrl('/site/LoginVk'),
            'response_type'=>'code',
            'v'=>'5.34',
            'state'=>'hello'
        );
        Yii::app()->controller->redirect($this->codeGetUrl.http_build_query($queryString));
    }

    public function getAccessToken($code)
    {
        $QueryString = array(
            'client_id'=>$this->client_id,
            'client_secret'=>$this->client_secret,
            'code'=>$code,
            'redirect_uri'=> Yii::app()->createAbsoluteUrl('/site/LoginVk'),
        );
        $getAccessUrl = $this->tokenGetUrl.http_build_query($QueryString);
        $tokenInfo = json_decode(file_get_contents($getAccessUrl));

        if(isset($tokenInfo->access_token)) {
            if(!Yii::app()->user->isGuest) {
                $this->addAccount($tokenInfo->access_token, $tokenInfo->user_id, $this->social_id);
            } else {
                $this->checkUser($tokenInfo->access_token, $tokenInfo->user_id, $this->social_id);
            }
        }
    }

    public function getUser()
    {
        $currentUser = UserSocials::model()->find('social_id = :s_id AND user_id = :u_id', array('s_id'=>$this->social_id, ':u_id'=>Yii::app()->user->id));
        $data = json_decode(file_get_contents(
            'https://api.vk.com/method/users.get?'.
            'user_ids='.$currentUser->social_user_id.
            '&fields=first_name,last_name,photo_100,sex'.
            '&version=5.28'
        ));

        //Проверка на коректность принятых данных
        if(isset($data->response) && $data = $data->response) {
            return array(
                'id'=>$data[0]->uid,
                'first_name'=>$data[0]->first_name,
                'last_name'=>$data[0]->last_name,
                'image'=>$data[0]->photo_100,
                'sex'=>$data[0]->sex == 2 ? '1' : '0',
            );
        } else {
            throw new Exception(Yii::t('main', 'Ошибка получения данных от API').'<br>');
        }
    }
}