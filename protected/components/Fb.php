<?php
class Fb extends BaseApi
{
    public $social_id = 2;
    public $client_id = '1609866009290719';
    public $client_secret = 'c1c10d1a88fbcbbca67f7998178db144';

    protected $codeGetUrl = 'https://www.facebook.com/dialog/oauth?';
    protected $tokenGetUrl = 'https://graph.facebook.com/oauth/access_token?';

    public function getAccessCode()
    {
        $queryString = array(
            'client_id'=>$this->client_id,
            'redirect_uri'=> Yii::app()->createAbsoluteUrl('/site/loginFb'),
            'response_type'=>'code',
        );
        Yii::app()->controller->redirect($this->codeGetUrl.http_build_query($queryString));
    }

    public function getAccessToken($code)
    {
        $QueryString = array(
            'client_id'=>$this->client_id,
            'redirect_uri'=> Yii::app()->createAbsoluteUrl('/site/loginFb'),
            'client_secret'=>$this->client_secret,
            'code'=>$code,
        );
        $tokenInfo = null;
        parse_str(file_get_contents($this->tokenGetUrl. http_build_query($QueryString)), $tokenInfo);

        if(isset($tokenInfo['access_token'])) {
            $result = json_decode(file_get_contents('https://graph.facebook.com/me?fields=id&access_token=' . $tokenInfo['access_token']));
            if(!Yii::app()->user->isGuest) {
                $this->addAccount($tokenInfo['access_token'], $result->id, $this->social_id);
            } else {
                $this->checkUser($tokenInfo['access_token'], $result->id, $this->social_id);
            }
        }

    }

    public function getUser()
    {
        $currentUser = UserSocials::model()->find('social_user_id = :id', array(':id'=>Yii::app()->user->social_user_id));
        $token = $currentUser->token;
        $data = json_decode(file_get_contents(
            'https://graph.facebook.com/me?fields=id,first_name,last_name,gender,picture.width(100).height(100)&access_token='.$token
        ));

        if(isset($data->id) && !empty($data->id)) {
            return array(
                'id'=>$data->id,
                'first_name'=>$data->first_name,
                'last_name'=>$data->last_name,
                'image'=>$data->picture->data->url,
                'sex'=>$data->gender == 'male' ? '1' : '0',
            );
        } else {
            throw new Exception(Yii::t('main', 'Ошибка получения данных от API').'<br>');
        }
    }
}