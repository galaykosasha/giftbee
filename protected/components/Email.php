<?php
class Email
{
    public function buildEmailText($link)
    {
        return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="height: 100%;" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>A Simple Responsive HTML Email</title>
        <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;}
        .content {width: 100%; max-width: 600px;}
        </style>
    </head>
    <body style="height: 100%;" yahoo bgcolor="#f6f8f1">
        <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; height: 100%;">
            <tr style="height: 100%;">
                <td  style="vertical-align: middle; height: 100%;">
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <img style="width: 150px; margin: 17px 0;" src="http://giftbee.com.ua/images/logo-1.png" alt="" />
                            </td>
                        </tr>
                    </table>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="box-shadow: 0 0 16px 4px rgba(0,0,0,.1); background-color: #fff; padding: 50px;">
                        <tr>
                            <td>
                                <h2 style="font-weight: 500; margin: 0;">Подтвердите регистрацию на Giftbee.ua!</h2><br>
                                <p class="lead" style="margin: 0;">Осталось совсем немного на пути к подаркам!</p>
                                <p class="lead" style="margin: 0 0 10px 0;">Пожалуйста перейдите по этой ссылке чтобы закончить регистрацию</p>
                                <a style="font-size: 18px; padding: 0 0 20px 0; display: block;" href="'.$link.'">'.$link.'</a>
                                <p style="font-size: 12px; margin: 0; color: #888;">Нужна помощь в пользовании сайтом? Посмотрите раздел <a href="http://giftbee.com.ua/instruction">инструкции</a></p>
                                    <p style="font-size: 12px; margin: 10px 0 0 0; color: #888;">
                                    Пожалуйста не отвечайте на это письмо, оно отправлено автоматически. Если Вы заметили ошибку в работе сайта сообщите нам на почту <a href="mailto:bug@giftbee.com.ua?subject=Ошибка на сайте">bug@giftbee.com.ua</a>
                                    </p>

                            </td>
                        </tr>
                    </table>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <h3 style="font-weight: 500; margin: 30px 0 5px;">Giftbee.com.ua</h3>
                                <h4 style="font-weight: 500; margin: 0;">Здесь живут подарки!</h4>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>';
    }

    public function afterRegisterMessage()
    {
        return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html style="height: 100%;" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>A Simple Responsive HTML Email</title>
            <style type="text/css">
            body {margin: 0; padding: 0; min-width: 100%!important;}
            .content {width: 100%; max-width: 600px;}
            </style>
        </head>
        <body style="height: 100%;" yahoo bgcolor="#f6f8f1">
            <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; height: 100%;">
                <tr style="height: 100%;">
                    <td  style="vertical-align: middle; height: 100%;">
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <img style="width: 150px; margin: 17px 0;" src="http://giftbee.com.ua/images/logo-1.png" alt="" />
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="box-shadow: 0 0 16px 4px rgba(0,0,0,.1); background-color: #fff; padding: 50px;">
                            <tr>
                                <td>
                                    <h2 style="font-weight: 500; margin: 0;">Спасибо за регистрацию на Giftbee.ua!</h2><br>
                                    <p class="lead" style="margin: 0;">Подпишитесь на наши страницы:</p>
                                    <p>
                                    <a style="font-size: 18px; padding: 0 5px 10px 0; display:inline-block;" href="https://www.facebook.com/giftbeeua">Facebook</a>
                                    <a style="font-size: 18px; padding: 0 5px 10px 5px; display:inline-block;" href="https://vk.com/giftbeeua">Вконтакте</a>
                                    </p>
                                    <p style="font-size: 18px; margin: 0 0 10px 0; color: #888;">Теперь Вы можете участвовать в конкурсах и выигрывать. Желаем удачи!</p>
                                    <p style="font-size: 12px; margin: 0; color: #888;">Нужна помощь в пользовании сайтом? Посмотрите раздел <a href="http://giftbee.com.ua/instruction">инструкции</a></p>
                                    <p style="font-size: 12px; margin: 10px 0 0 0; color: #888;">
                                    Пожалуйста не отвечайте на это письмо, оно отправлено автоматически. Если Вы заметили ошибку в работе сайта сообщите нам на почту <a href="mailto:bug@giftbee.com.ua?subject=Ошибка на сайте">bug@giftbee.com.ua</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <h3 style="font-weight: 500; margin: 30px 0 5px;">Giftbee.com.ua</h3>
                                <h4 style="font-weight: 500; margin: 0;">Здесь живут подарки!</h4>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
        </body>
    </html>';
    }

    public function forNullCompetitionsUsers($user_name)
    {
        return '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html style="height: 100%;" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>A Simple Responsive HTML Email</title>
            <style type="text/css">
            body {margin: 0; padding: 0; min-width: 100%!important;}
            .content {width: 100%; max-width: 600px;}
            </style>
        </head>
        <body style="height: 100%;" yahoo bgcolor="#f6f8f1">
            <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; height: 100%;">
                <tr style="height: 100%;">
                    <td  style="vertical-align: middle; height: 100%;">
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <img style="width: 150px; margin: 17px 0;" src="http://giftbee.com.ua/images/logo-1.png" alt="" />
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="box-shadow: 0 0 16px 4px rgba(0,0,0,.1); background-color: #fff; padding: 50px;">
                            <tr>
                                <td>
                                    <h2 style="font-weight: 500; margin: 0;">Здравствуйте '.$user_name.'</h2><br>
                                    <p class="lead" style="margin: 0;">Вы прошли авторизацию на сайте giftbee.com.ua, но не поучавствовали ни в одном конкурсе. <br>
                                        Возможно Вы в чем то не разобрались?  Или Вам не подошли призы от наших партнеров?<br>
                                        В любом случае за это время у нас появилось много интересных конкурсов будем рады видеть Вас!
                                    </p>
                                    <p class="lead" style="margin: 0;">Подписывайтесь на наши групы фб, вк. Желаем удачи.</p>
                                    <p>
                                    <a style="font-size: 18px; padding: 0 5px 10px 0; display:inline-block;" href="https://www.facebook.com/giftbeeua">Facebook</a>
                                    <a style="font-size: 18px; padding: 0 5px 10px 5px; display:inline-block;" href="https://vk.com/giftbeeua">Вконтакте</a>
                                    </p>


                                    <p style="font-size: 18px; margin: 0 0 10px 0; color: #888;">Спасибо!</p>
                                    <p style="font-size: 12px; margin: 0; color: #888;"> </p>
                                    <p style="font-size: 12px; margin: 10px 0 0 0; color: #888;">
                                    Пожалуйста не отвечайте на это письмо, оно отправлено автоматически. Если Вы заметили ошибку в работе сайта сообщите нам на почту <a href="mailto:bug@giftbee.com.ua?subject=Ошибка на сайте">bug@giftbee.com.ua</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <h3 style="font-weight: 500; margin: 30px 0 5px;"><a href="https://www.giftbee.com.ua">giftbee.com.ua</a></h3>
                                <h4 style="font-weight: 500; margin: 0;">Здесь живут подарки!</h4>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
        </body>
    </html>
        ';
    }

    public function afterParticipation($user_name, $competition_title)
    {
        return '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html style="height: 100%;" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>A Simple Responsive HTML Email</title>
            <style type="text/css">
            body {margin: 0; padding: 0; min-width: 100%!important;}
            .content {width: 100%; max-width: 600px;}
            </style>
        </head>
        <body style="height: 100%;" yahoo bgcolor="#f6f8f1">
            <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; height: 100%;">
                <tr style="height: 100%;">
                    <td  style="vertical-align: middle; height: 100%;">
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <img style="width: 150px; margin: 17px 0;" src="http://giftbee.com.ua/images/logo-1.png" alt="" />
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="box-shadow: 0 0 16px 4px rgba(0,0,0,.1); background-color: #fff; padding: 50px;">
                            <tr>
                                <td>
                                    <h2 style="font-weight: 500; margin: 0;">Здравствуйте '.$user_name.'</h2><br>
                                    <p class="lead" style="margin: 0;">
                                    Спасибо за участие в конкрусе '.$competition_title.' <br><br>Хотим сообщить что у нас еще много других <a href="http://giftbee.com.ua/competitions">конкурсов</a>.
                                     <br>Немедленно заходите, учавствуйте и побеждайте. <br>
                                    Желаем удачи.<br>
<br>Если Вам нужна помощь посетите раздел <a href="http://giftbee.com.ua/instruction">инструкции</a> или задайте интересующий Вас вопрос в наших группах:</p>
<p>
                                    <a style="font-size: 18px; padding: 0 5px 10px 0; display:inline-block;" href="https://www.facebook.com/giftbeeua">Facebook</a>
                                    <a style="font-size: 18px; padding: 0 5px 10px 5px; display:inline-block;" href="https://vk.com/giftbeeua">Вконтакте</a>
                                    </p>


                                    <p >Спасибо!</p>
                                    <p style="font-size: 12px; margin: 0; color: #888;"> </p>
                                    <p style="font-size: 12px; margin: 10px 0 0 0; color: #888;">
                                    Пожалуйста не отвечайте на это письмо, оно отправлено автоматически. Если Вы заметили ошибку в работе сайта сообщите нам на почту <a href="mailto:bug@giftbee.com.ua?subject=Ошибка на сайте">bug@giftbee.com.ua</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <h3 style="font-weight: 500; margin: 30px 0 5px;"><a href="https://www.giftbee.com.ua">giftbee.com.ua</a></h3>
                                <h4 style="font-weight: 500; margin: 0;">Здесь живут подарки!</h4>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
        </body>
    </html>
        ';
    }

    public function forOneCompetitionUsers($user_name, $competition_title){
        return '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html style="height: 100%;" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>A Simple Responsive HTML Email</title>
            <style type="text/css">
            body {margin: 0; padding: 0; min-width: 100%!important;}
            .content {width: 100%; max-width: 600px;}
            </style>
        </head>
        <body style="height: 100%;" yahoo bgcolor="#f6f8f1">
            <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; height: 100%;">
                <tr style="height: 100%;">
                    <td  style="vertical-align: middle; height: 100%;">
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <img style="width: 150px; margin: 17px 0;" src="http://giftbee.com.ua/images/logo-1.png" alt="" />
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="box-shadow: 0 0 16px 4px rgba(0,0,0,.1); background-color: #fff; padding: 50px;">
                            <tr>
                                <td>
                                    <h2 style="font-weight: 500; margin: 0;">Здравствуйте '.$user_name.'</h2><br>
                                    <p class="lead" style="margin: 0;">
                                    Спасибо за участие в конкрусе '.$competition_title.' <br><br>Хотим сообщить что у нас еще много других <a href="http://giftbee.com.ua/competitions">конкурсов</a>.
                                     <br>Немедленно заходите, учавствуйте и побеждайте. <br>
                                    Желаем удачи.<br>
<br>Если Вам нужна помощь посетите раздел <a href="http://giftbee.com.ua/instruction">инструкции</a> или задайте интересующий Вас вопрос в наших группах:</p>
<p>
                                    <a style="font-size: 18px; padding: 0 5px 10px 0; display:inline-block;" href="https://www.facebook.com/giftbeeua">Facebook</a>
                                    <a style="font-size: 18px; padding: 0 5px 10px 5px; display:inline-block;" href="https://vk.com/giftbeeua">Вконтакте</a>
                                    </p>


                                    <p >Спасибо!</p>
                                    <p style="font-size: 12px; margin: 0; color: #888;"> </p>
                                    <p style="font-size: 12px; margin: 10px 0 0 0; color: #888;">
                                    Пожалуйста не отвечайте на это письмо, оно отправлено автоматически. Если Вы заметили ошибку в работе сайта сообщите нам на почту <a href="mailto:bug@giftbee.com.ua?subject=Ошибка на сайте">bug@giftbee.com.ua</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <h3 style="font-weight: 500; margin: 30px 0 5px;"><a href="https://www.giftbee.com.ua">giftbee.com.ua</a></h3>
                                <h4 style="font-weight: 500; margin: 0;">Здесь живут подарки!</h4>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
        </body>
    </html>
        ';
    }

    public function forCompetition()
    {
        return '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html style="height: 100%;" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>A Simple Responsive HTML Email</title>
            <style type="text/css">
            body {margin: 0; padding: 0 0 20px 0; min-width: 100%!important;}
            .content {width: 100%; max-width: 600px;}
            </style>
        </head>
        <body style="height: 100%;" yahoo bgcolor="#f6f8f1">
            <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; height: 100%;">
                <tr style="height: 100%;">
                    <td  style="vertical-align: middle; height: 100%;">
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <img style="width: 150px; margin: 17px 0;" src="http://giftbee.com.ua/images/logo-1.png" alt="" />
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="box-shadow: 0 0 16px 4px rgba(0,0,0,.1); background-color: #fff; padding: 50px;">
                            <tr>
                                <td>
                                    <h2 style="font-weight: 500; margin: 0;">У нас для Вас новый конкурс!</h2><br>
                                    <img style="max-width: 100%" class="comp-banner" alt="Sound Sound" src="http://giftbee.com.ua/uploads/competitions/md-images/ximage_55dc665a8d7cf900x300.jpg.pagespeed.ic.gjF8m-Etis.jpg">
                                    <p class="lead" style="margin: 0;"><br/><b>Привет, друзья!</b><br/>Помните, мы обещали конкурсы с подарками?<br/>
                                    Вот и пришло время порадовать Вас! 9 октября в 20:00 мы идем вдохновляться на джазово-поэтический вечер посвященный началу и середине ХХ века!
                                    Письма и дневники Хемингуэя, Ремарка, Фицджеральда, Камю, Маркеса, Уєлса, Дали, Виана, стихи Киплинга, Бернса и многих других в исполнении
                                    Вани Якимова, под аккомпанемент легкого джазового рояля Алексея Боголюбова.<br/>
                                    По результатам конкурса мы определим 3 победителя...</p>
                                    <a href="'.Yii::app()->createAbsoluteUrl('/site/countEmailReferral').'" style="height:50px; display: inline-block; background: #faaa18; text-decoration: none; color: #fff; line-height: 50px; padding: 0 15px; margin: 10px 0 0;">Перейти к конкурсу</a>


                                    <p >Спасибо!</p>
                                    <p style="font-size: 12px; margin: 0; color: #888;"> </p>
                                    <p style="font-size: 12px; margin: 10px 0 0 0; color: #888;">
                                    Пожалуйста не отвечайте на это письмо, оно отправлено автоматически. Если Вы заметили ошибку в работе сайта сообщите нам на почту <a href="mailto:bug@giftbee.com.ua?subject=Ошибка на сайте">bug@giftbee.com.ua</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <h3 style="font-weight: 500; margin: 30px 0 5px;"><a href="https://www.giftbee.com.ua">giftbee.com.ua</a></h3>
                                    <h4 style="font-weight: 500; margin: 0;">Здесь живут подарки!</h4>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
        </body>
    </html>
        ';
    }

    public function newMails($name)
    {
        return '
            <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
  <style type="text/css">
body {
  margin: 0;
  padding: 0;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

table {
  border-spacing: 0;
}

table td {
  border-collapse: collapse;
}

.ExternalClass {
  width: 100%;
}

.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
  line-height: 100%;
}

.ReadMsgBody {
  width: 100%;
  background-color: #ebebeb;
}

table {
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}

img {
  -ms-interpolation-mode: bicubic;
  max-width: 100%;
  padding-bottom: 5px;
}

.yshortcuts a {
  border-bottom: none !important;
}

@media screen and (max-width: 599px) {
    .force-row,
  .container {
        width: 100% !important;
        max-width: 100% !important;
  }
}
@media screen and (max-width: 400px) {
    .container-padding {
        padding-left: 12px !important;
    padding-right: 12px !important;
  }
}
.ios-footer a {
    color: #aaaaaa !important;
    text-decoration: underline;
}
</style>
</head>

<body style="font-family:Helvetica, Arial, sans-serif; margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#FFF">
  <tr>
    <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #FFF;">

      <br>

      <!-- 600px container (white background) -->
      <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
        <tr>
          <td class="container-padding header" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#DF4726;padding-left:24px;padding-right:24px">
            <img src="http://giftbee.com.ua/images/xlogo-top_bar.png.pagespeed.ic.4KsuXpGnwZ.png" alt="">
          </td>
        </tr>
        <tr>
          <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#f5f5f5">
            <br>
<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Приветствуем Вас '.$name.'</div>
<br>

<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
    Заходите быстрее на Giftbee.Ведь мы подготовили много интерестных конкурсов и ценных подарков. Вот некоторые из них:
  <br><br>
</div>



<!-- example: two columns (simple) -->
    <table style="margin-bottom: 10px;" width="264" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row" bgcolor="#fff">
      <tr>
        <td class="col" valign="top" style="margin-bottom; 10px; border: 1px solid #ebebeb; border-bottom: 5px solid #e7e6e6; font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
          <img style="max-width: 100%;" src="http://giftbee.com.ua/uploads/competitions/xs-images/ximage_55f81ea0673fa450x200-min.jpg.pagespeed.ic.llMJZeWAZl.jpg" alt="">
          <strong style="display: block; margin: 10px 15px;">Подарочный сертификат номиналом в 500 гривен на любой товар в интернет магазине: www.belmoca.com.ua в подарок</strong>
          <p style="display: block; margin: 10px 15px;"><span style="color: #7e7e7e;">Belmoca</span><a href="http://giftbee.com.ua/belmoka/500grn" style="background: #faab1b; height: 31px;width: 100px;color: #fff;display: inline-block;line-height: 31px;text-decoration: none;text-align: center;float: right;margin-bottom: 10px;border-radius: 4px;  letter-spacing: .02em;  box-shadow: 0 3px 0 0 #f1a44a;
">Подробнее</a></p>
        </td>
      </tr>
    </table>

    <table style="margin-bottom: 10px;" width="264" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row" bgcolor="#fff">
      <tr>
        <td class="col" valign="top" style="margin-bottom; 10px; border: 1px solid #ebebeb; border-bottom: 5px solid #e7e6e6;font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
        <img style="max-width: 100%;" src="http://giftbee.com.ua/uploads/competitions/xs-images/ximage_55f81bf9ee2c7450x200.jpg.pagespeed.ic.aP5gqG_6S_.jpg" alt="">
        <strong style="display: block; margin: 10px 15px;">Прекрасный кофейный набор Moka Flor для настоящих гурманов в подарок</strong>
        <p style="display: block; margin: 10px 15px;"><span style="color: #7e7e7e;">100% Arabica</span><a href="http://giftbee.com.ua/100Arabica/mokaflor" style="background: #faab1b; height: 31px;width: 100px;color: #fff;display: inline-block;line-height: 31px;text-decoration: none;text-align: center;float: right;margin-bottom: 10px;border-radius: 4px;  letter-spacing: .02em;  box-shadow: 0 3px 0 0 #f1a44a;
">Подробнее</a></p>
        </td>
      </tr>
    </table>

    <table width="528"><tr><td></td></tr></table>

    <table style="margin-bottom: 10px;" width="264" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row" bgcolor="#fff">
      <tr>
        <td class="col" valign="top" style="margin-bottom; 10px; border: 1px solid #ebebeb; border-bottom: 5px solid #e7e6e6; font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
          <img style="max-width: 100%;" src="http://giftbee.com.ua/uploads/competitions/xs-images/ximage_55f81731b24a0450x200-min.jpg.pagespeed.ic.ywLWnGakW3.jpg" alt="">
          <strong style="display: block; margin: 10px 15px;">Разыграем подарочное издание «Азбуки Елизаветы Бём»</strong>
          <p style="display: block; margin: 10px 15px;"><span style="color: #7e7e7e;">«Стольный Град»</span><a href="http://giftbee.com.ua/izdatelstvo-stolnuy-grad/azbuka-bem-podarochnaya" style="background: #faab1b; height: 31px;width: 100px;color: #fff;display: inline-block;line-height: 31px;text-decoration: none;text-align: center;float: right;margin-bottom: 10px;border-radius: 4px;  letter-spacing: .02em;  box-shadow: 0 3px 0 0 #f1a44a;
">Подробнее</a></p>
        </td>
      </tr>
    </table>

    <table style="margin-bottom: 10px;" width="264" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row" bgcolor="#fff">
      <tr>
        <td class="col" valign="top" style="margin-bottom; 10px; border: 1px solid #ebebeb; border-bottom: 5px solid #e7e6e6;font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
        <img style="max-width: 100%;" src="http://giftbee.com.ua/uploads/competitions/xs-images/ximage_55f955c21488f450x200_3.jpg.pagespeed.ic.Ghc-gEZuOb.jpg" alt="">
        <strong style="display: block; margin: 10px 15px;">Разыграем 5 наборов цветущих чаев!</strong>
        <p style="display: block; margin: 10px 15px;"><span style="color: #7e7e7e;">Элитный чай</span><a href="http://giftbee.com.ua/elitnuy-chay/5tea" style="background: #faab1b; height: 31px;width: 100px;color: #fff;display: inline-block;line-height: 31px;text-decoration: none;text-align: center;float: right;margin-bottom: 10px;border-radius: 4px;  letter-spacing: .02em;  box-shadow: 0 3px 0 0 #f1a44a;
">Подробнее</a></p>
        </td>
      </tr>
    </table>
    <table width="528"><tr><td></td></tr></table>



<!--/ end example -->


      <p style="text-align: center;"><a href="http://giftbee.com.ua/competitions" style="width: 200px; height: 40px;line-height: 40px;  background: #8dbd4c;  box-shadow: 0 3px 0 0 #7ba63f;  border: 0;  margin-top: 10px;  float: none; color: #fff; display: inline-block; border-radius: 4px; text-decoration: none;">Все конкурсы</a></p>
      <p style="text-align: center; clear: both;">В этот раз повезет именно Вам!<b> Желаем удачи</b></p>
      <p>Если Вам нужна помощь посетите раздел <a href="http://giftbee.com.ua/instruction">инструкции</a> или задайте интересующий Вас вопрос в наших группах:</p>
      <p style="text-align: center;">
        <a style="font-size: 18px; padding: 0 5px 10px 0; display:inline-block;" href="https://www.facebook.com/giftbeeua">Facebook</a>
        <a style="font-size: 18px; padding: 0 5px 10px 5px; display:inline-block;" href="https://vk.com/giftbeeua">Вконтакте</a>
      </p>
      <p style="font-size: 12px; margin: 10px 0 0 0; color: #888;">
    Пожалуйста не отвечайте на это письмо, оно отправлено автоматически. Если Вы заметили ошибку в работе сайта сообщите нам на почту <a href="mailto:bug@giftbee.com.ua?subject=Ошибка на сайте">bug@giftbee.com.ua</a>
      </p>
      <h3 style="text-align: center; font-weight: 500; margin: 30px 0 5px;"><a href="http://www.giftbee.com.ua">giftbee.com.ua</a></h3>
      <h4 style="text-align: center; font-weight: 500; margin: 0;">Здесь живут подарки!</h4>
      </table>

    </td>
  </tr>
</table>
<!--/100% background wrapper-->

</body>
</html>
        ';
    }

    public function forUsersInBuffer($link)
    {
        return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="height: 100%;" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>A Simple Responsive HTML Email</title>
        <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;}
        .content {width: 100%; max-width: 600px;}
        </style>
    </head>
    <body style="height: 100%;" yahoo bgcolor="#f6f8f1">
        <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; height: 100%;">
            <tr style="height: 100%;">
                <td  style="vertical-align: middle; height: 100%;">
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <img style="width: 150px; margin: 17px 0;" src="http://giftbee.com.ua/images/logo-1.png" alt="" />
                            </td>
                        </tr>
                    </table>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="box-shadow: 0 0 16px 4px rgba(0,0,0,.1); background-color: #fff; padding: 50px;">
                        <tr>
                            <td>
                                <h2 style="font-weight: 500; margin: 0;">Здравствуйте</h2><br>
                                <p class="lead" style="margin: 0;">Вы пытались зарегистрироваться на giftbee.com.ua но что-то пошло не так.</p>

                                <p class="lead" style="margin: 0 0 10px 0;">Вам осталось всего лишь подтвердить почту. Для этого перейдите по ссылке:</p>
                                <a style="font-size: 18px; padding: 0 0 20px 0; display: block;" href="'.$link.'">'.$link.'</a>
                                <p style="font-size: 12px; margin: 0; color: #888;">Нужна помощь в пользовании сайтом? Посмотрите раздел <a href="http://giftbee.com.ua/instruction">инструкции</a></p>
                                    <p style="font-size: 12px; margin: 10px 0 0 0; color: #888;">
                                    Пожалуйста не отвечайте на это письмо, оно отправлено автоматически. Если Вы заметили ошибку в работе сайта сообщите нам на почту <a href="mailto:bug@giftbee.com.ua?subject=Ошибка на сайте">bug@giftbee.com.ua</a>
                                    </p>

                            </td>
                        </tr>
                    </table>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <h3 style="font-weight: 500; margin: 30px 0 5px;">Giftbee.com.ua</h3>
                                <h4 style="font-weight: 500; margin: 0;">Здесь живут подарки!</h4>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>';
    }
}
