<?php
abstract class BaseApi {
    public $social_id;
    abstract public function getAccessCode();
    abstract public function getAccessToken($code);

    protected function checkUser($access_token, $social_user_id, $social_id)
    {
        $user = UserSocials::model()->find(
            'social_id = :social_id AND social_user_id = :social_user_id',
            array(':social_id'=>$social_id, 'social_user_id'=>$social_user_id)
        );

        if(isset($user)) {
            $this->login($user->social_user_id, $social_id); // Если существует юзер, залогинем его
            if(isset(Yii::app()->session['url'])) {
                $url = Yii::app()->session['url'];
                unset(Yii::app()->session['url']);
                header("Location: $url"); exit;
            }
        } else {
            $this->checkBuffer($access_token, $social_user_id, $social_id); // Если юзер не существует, проверим, есть ли он в буфере
        }
    }

    public function addAccount($access_token, $social_user_id, $social_id)
    {
        $user = UserSocials::model()->find(
            'social_id = :social_id AND social_user_id = :social_user_id',
            array(':social_id'=>$social_id, 'social_user_id'=>$social_user_id)
        );

        if(!isset($user)) {
            $user = new UserSocials();
            $user->social_id = $this->social_id;
            $user->user_id = Yii::app()->user->id;
            $user->social_user_id = $social_user_id;
            $user->token = $access_token;
            if($user->save()) {
                if(isset(Yii::app()->session['url'])) {
                    $url = Yii::app()->session['url'];
                    unset(Yii::app()->session['url']);
                    if($this->social_id == 1) {
                        Yii::app()->user->setFlash('vkontakte', '<p>Теперь Вы можете<br> выполнять условия для</p> <b>Вконтакте</b>');
                    } elseif($this->social_id == 2){
                        Yii::app()->user->setFlash('facebook', '<p>Теперь Вы можете<br> выполнять условия для</p> <b>facebook</b>');
                    }
                    header("Location: $url"); exit;
                }
                Yii::app()->getController()->redirect('/profile/default/settings');
            }
        } else {
            if(!Yii::app()->user->isGuest) {
                if($social_id == 1) {
                    Yii::app()->user->setFlash('vkontakte', '<p>Этот профиль Вконтакте уже используется</p>');
                }
                elseif($social_id == 2) {
                    Yii::app()->user->setFlash('vkontakte', '<p>Этот профиль Facebook уже используется</p>');
                }
            }
        }
    }

    private function checkBuffer($access_token, $social_user_id, $social_id)
    {
        $user = UsersBuffer::model()->find('social_user_id = :id', array(':id'=>$social_user_id));
        if($user) {
            if(isset($user->approve_code) && !empty($user->approve_code)) {
                Yii::app()->session['sending_message'] = true;
            }
            Yii::app()->session['u_id'] = $user->id;
        } else {
            $this->saveToBuffer($access_token, $social_user_id, $social_id);
        }
        Yii::app()->getController()->redirect(Yii::app()->homeUrl);
    }

    private function saveToBuffer($access_token, $social_user_id, $social_id)
    {
        $buffer = new UsersBuffer();
        $buffer->token = $access_token;
        $buffer->social_user_id = $social_user_id;
        $buffer->social_id = $social_id;
        if($buffer->save()) {
            Yii::app()->session['u_id'] = $buffer->id;
        }
    }

    public function login($social_user_id, $social_id)
    {
        $identity = new UserIdentity($social_user_id, $social_id);
        $identity->authenticate();

        if($identity->errorCode === UserIdentity::ERROR_NONE)
        {
            Yii::app()->user->login($identity, 3600*24*30); //30 days

            $user = Users::model()->findByPk(Yii::app()->user->id);
            Yii::app()->user->setFlash('info', '<p>Добро пожаловать,</p> <b>'. $user->first_name.'</b>');
        }
    }
}